package com.akkipedia.skeleton.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akash on 16/02/17.
 */

public class FormValidator {
    private List<FormElement> elements;
    private boolean shouldSetErrors;

    private FormValidator(){}

    public void validate(ValidationCallback callback){
        boolean isValid = true;
        List<String> errors = new ArrayList<>();
        for(FormElement element : elements){
            element.setErrors(shouldSetErrors);
            isValid &= element.isValid();
            errors.add(element.getError());
        }
        callback.onCompleted(isValid, errors);
    }


    public static class Builder{
        private List<FormElement> elements;
        private boolean shouldSetErrors;

        public Builder(){
            elements = new ArrayList<>();
        }

        public Builder addElement(FormElement element){
            elements.add(element);
            return this;
        }

        public Builder setErrors(boolean shouldSetErrors){
            this.shouldSetErrors = shouldSetErrors;
            return this;
        }

        public FormValidator build(){
            FormValidator formValidator = new FormValidator();
            formValidator.elements = elements;
            formValidator.shouldSetErrors = shouldSetErrors;
            return formValidator;
        }
    }

    public interface ValidationCallback{
        void onCompleted(boolean isValid, List<String> errors);
    }
}
