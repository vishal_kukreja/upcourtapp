package com.akkipedia.skeleton.forms;

import android.widget.EditText;

/**
 * Created by Akash on 16/02/17.
 */

public class EditTextFormElement extends FormElement {
    private ElementValidator validator;
    private String error;

    public EditTextFormElement(EditText editText, ElementValidator validator) {
        this.elementView = editText;
        this.validator = validator;
    }

    public boolean isValid(){
        error = validator.isValid(((EditText)elementView).getText().toString());
        if(shouldSetErrors)
            ((EditText) elementView).setError(error);
        return error == null;
    }

    @Override
    public String getError() {
        return error;
    }
}
