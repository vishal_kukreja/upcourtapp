package com.akkipedia.skeleton.forms;

/**
 * Created by Akash on 16/02/17.
 */

public class StringLengthValidator implements ElementValidator {
    private int length;

    public StringLengthValidator(int length) {
        this.length = length;
    }

    @Override
    public String isValid(Object element) {
        String error = null;
        boolean isValid = (element instanceof String);
        if(isValid){
            isValid &= ((String) element).length() >= length;
        } else {
            error = "Invalid object type";
        }
        if(!isValid){
            error = length == 1
                    ?"Value cannot be empty"
                    :"Should be at least "+length+" characters";
        }
        return error;
    }
}
