package com.akkipedia.skeleton.forms;

import android.widget.EditText;

/**
 * Created by Akash on 16/02/17.
 */

public class ConfirmPasswordValidator implements ElementValidator {
    private EditText passwordEditText;

    public ConfirmPasswordValidator(EditText passwordEditText) {
        this.passwordEditText = passwordEditText;
    }

    @Override
    public String isValid(Object element) {
        String error = null;
        boolean isValid = (element instanceof String);
        if(isValid){
            isValid &= ((String) element).equals(passwordEditText.getText().toString());
        } else {
            error = "Invalid object type";
        }
        if(!isValid){
            error = "Password mismatch";
        }
        return error;
    }
}
