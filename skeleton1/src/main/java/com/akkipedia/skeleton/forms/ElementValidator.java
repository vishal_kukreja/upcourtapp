package com.akkipedia.skeleton.forms;

/**
 * Created by Akash on 16/02/17.
 */

public interface ElementValidator {
    String isValid(Object element);
}
