package com.akkipedia.skeleton.forms;

import android.view.View;

/**
 * Created by Akash on 16/02/17.
 */

abstract class FormElement {
    View elementView;
    boolean shouldSetErrors;
    public abstract boolean isValid();
    public abstract String getError();
    public void setErrors(boolean shouldSetErrors){
        this.shouldSetErrors = shouldSetErrors;
    }
}
