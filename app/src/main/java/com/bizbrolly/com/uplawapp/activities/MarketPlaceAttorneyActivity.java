package com.bizbrolly.com.uplawapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.SearchAttorneyList;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.adapter.MarketPlaceAdapter;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketPlaceAttorneyActivity extends AppCompatActivity {
    List<SearchAttorneyList.GetAttorneyListResultBean.DataBean> list;
    List<SearchAttorneyList.GetAttorneyListResultBean.DataBean> list_copy;
    int CCount = 0;
    MarketPlaceAdapter marketPlaceAdapter;
    SearchView searchView;
    RecyclerView market_place_attorney;
    Toolbar toolbar;
    TextView no_search;
    private ImageView logo;

    public static Object deepCopy(Object input) {
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Object output = null;
        try {
            // deep copy
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            // serialize and pass the object
            oos.writeObject(input);
            oos.flush();
            ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            // return the new object
            output = ois.readObject();

            // verify it is the same
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_place_attorney);
        findViews();
        logo.setVisibility(View.GONE);
        no_search.setVisibility(View.GONE);
        setActionTool();
        searchAttorneyRetrofit();

    }

    private void setActionTool() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Search Attorney");
    }

    private void findViews() {
        logo = (ImageView) findViewById(R.id.logo);

        no_search = (TextView) findViewById(R.id.NoSEAR_TEXtVIEW);
        toolbar = (Toolbar) findViewById(R.id.toolbarc);
        market_place_attorney = (RecyclerView) findViewById(R.id.market_place_attorney);
        market_place_attorney.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                if (list_copy != null)
//                    list = list_copy;
//                else
//                    list_copy = (List<SearchAttorneyList.GetAttorneyListResultBean.DataBean>) deepCopy(list);
//                for (int i = list.size() - 1; i >= 0; i--) {
//                    if (list.get(i).getName().toLowerCase().contains(newText.toLowerCase())) {
//                    } else {
//                        list.remove(i);
//                    }
//                    if (list.size() == 0) {
//
//                        no_search.setVisibility(View.VISIBLE);
//
//                    }
//                }
//
//                MarketPlaceAdapter adapter = new MarketPlaceAdapter(MarketPlaceAttorneyActivity.this, list);
//                market_place_attorney.setLayoutManager(new LinearLayoutManager(MarketPlaceAttorneyActivity.this));
//                market_place_attorney.setAdapter(adapter);
                list_copy = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getName() != null) {
                        if (list.get(i).getName().toLowerCase().contains(newText.toLowerCase())) {
                            list_copy.add(list.get(i));
                        }
                    }

                }

                if (list_copy.size() == 0) {
                    no_search.setVisibility(View.VISIBLE);
                    logo.setVisibility(View.VISIBLE);

                } else {
                    logo.setVisibility(View.GONE);

                    no_search.setVisibility(View.GONE);
                }

                MarketPlaceAdapter adapter = new MarketPlaceAdapter(MarketPlaceAttorneyActivity.this, list_copy);
                market_place_attorney.setLayoutManager(new LinearLayoutManager(MarketPlaceAttorneyActivity.this));
                market_place_attorney.setAdapter(adapter);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchAttorneyRetrofit() {
        final ProgressDialog p = new ProgressDialog(this);
        p.setMessage("Please wait...");
        p.setCancelable(false);
        p.show();
        String Username = Preferences.getInstance(this).getUserName();
        String authToken = Preferences.getInstance(this).getAuthToken();
        WebServiceRequests.getInstance().getAttorney(
                this,
                Username,
                authToken,
                new Callback<SearchAttorneyList>() {
                    @Override
                    public void onResponse(Call<SearchAttorneyList> call, Response<SearchAttorneyList> response) {

                        if (response.isSuccessful() && response.body() != null && response.body().getGetAttorneyListResult().isResult()) {
                            p.hide();
                            list = response.body().getGetAttorneyListResult().getData();
                            list_copy = (List<SearchAttorneyList.GetAttorneyListResultBean.DataBean>) deepCopy(list);
                            marketPlaceAdapter = new MarketPlaceAdapter(MarketPlaceAttorneyActivity.this, response.body().getGetAttorneyListResult().getData());
                            market_place_attorney.setAdapter(marketPlaceAdapter);
                        } else {
                            Toast.makeText(MarketPlaceAttorneyActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchAttorneyList> call, Throwable t) {
                        p.hide();
                        Toast.makeText(MarketPlaceAttorneyActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_CANCELED) {

                marketPlaceAdapter.onActivityResult(requestCode, resultCode, data);

                //Write your code if there's no result
            } else {

            }
        }
    }

}
