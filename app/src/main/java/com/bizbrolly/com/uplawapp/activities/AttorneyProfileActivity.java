package com.bizbrolly.com.uplawapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.Profile_lasted;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.ContactResponse;
import com.bizbrolly.com.uplawapp.WebServices.SearchAttorneyList;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttorneyProfileActivity extends AppCompatActivity {
    TextView vcard_tv_name, vcard_tv_specl, vcard_tv_phone, vcard_tv_mail;
    Toolbar toolbar;
    Button back_button, contact_button;
    TextView Details_TextView, name1, descriptionTextView, special, locat, bushrs, yearTextView, textView_Desclaimer;
    List<SearchAttorneyList.GetAttorneyListResultBean.DataBean> list;
    Bundle extras;
    ImageView imgID, vcard_image;
    String yr, name, specliztionString, locationString, businesshrs, descriptionString, detailString, image_String;
    Button contact_bton;
    private TextView headerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_atorney_profile);
        //List<String> myList = (List<String>) getIntent().getSerializableExtra("mylist");
        findViews();
        if (getIntent().getExtras() != null) {
            extras = getIntent().getExtras();
            setData();
        }


        if (Preferences.getInstance(this).getRole().equals("Attorney")) {
            headerText.setText("My Profile");

            back_button.setVisibility(View.GONE);
            contact_button.setVisibility(View.GONE);
        }

        setActionTool();

        setListner();


//        Type listType = new TypeToken<ArrayList<SearchAttorneyList.GetAttorneyListResultBean.DataBean>>() {}.getType();
//        List<SearchAttorneyList.GetAttorneyListResultBean.DataBean> dataBeen = new Gson().fromJson(getIntent().getExtras().getString("list"), listType);
        //list=new com.google.gson.Gson().fromJson(getIntent().getStringExtra("myList"),SearchAttorneyList.GetAttorneyListResultBean.DataBean.class);


    }

    private void setData() {
        yr = extras.getString("year");
        yearTextView.setText(yr);

        name = extras.getString("name");
        name1.setText(name);

        businesshrs = extras.getString("Business_Hours");
        bushrs.setText(businesshrs);

        descriptionString = extras.getString("Attorney_Bio");
        descriptionTextView.setText(descriptionString);

        detailString = extras.getString("Email");
        Details_TextView.setText(detailString);


        specliztionString = extras.getString("Specialization");
        special.setText(specliztionString);

        locationString = extras.getString("Location");
        locat.setText(locationString);

        image_String = extras.getString("Image");
        if (!image_String.trim().equals("")) {
            Picasso.with(this).load(image_String).into(imgID);
        }
    }

    private void setListner() {
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        contact_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Dialog d = new Dialog(AttorneyProfileActivity.this);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.content_vcard);
                //View v=LayoutInflater.from(AttorneyProfileActivity.this).inflate(R.layout.content_vcard,null);
                contact_bton = (Button) d.findViewById(R.id.contact_bton);
                vcard_image = (ImageView) d.findViewById(R.id.vcard_image);
                vcard_tv_name = (TextView) d.findViewById(R.id.vcard_tv_name);
                vcard_tv_phone = (TextView) d.findViewById(R.id.vcard_tv_phone);
                vcard_tv_mail = (TextView) d.findViewById(R.id.vcard_tv_mail);
                textView_Desclaimer = (TextView) d.findViewById(R.id.textView4);
                textView_Desclaimer.setText(Html.fromHtml("<b>Disclaimer:</b>" + " if you contact the attorney, and the attorney does not respond within 24 hours, please contact our support team for further assistance"));

                Picasso.with(AttorneyProfileActivity.this).load(image_String).into(vcard_image);
                vcard_tv_name.setText(name1.getText().toString());

                vcard_tv_mail.setText(extras.getString("Email"));
                vcard_tv_phone.setText(extras.getString("phone"));
                //d.setContentView(v);
                vcard_tv_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + vcard_tv_phone.getText().toString()));

                        startActivity(i);
                    }
                });
                vcard_tv_mail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{vcard_tv_mail.getText().toString()});
                        i.setType("message/rfc822");
                        startActivity(Intent.createChooser(i, "Choose an Email client :"));
//                        i.setData(Uri.parse("mailto:"));
//                        i.setType("text/plain");
//
//                        i.putExtra(Intent.EXTRA_EMAIL,vcard_tv_mail.getText().toString());
//                        startActivity(i);
                    }
                });
                contact_bton.setOnClickListener(new View.OnClickListener() {
                    //                  final ProgressDialog p =new ProgressDialog(AttorneyProfileActivity.this);
//                     p.setMessage("Please wait...");
//                     p.show();
                    @Override
                    public void onClick(View v) {
                        final ProgressDialog p = new ProgressDialog(AttorneyProfileActivity.this);
                        p.setMessage("please wait...");
                        p.setCancelable(false);
                        p.show();
                        WebServiceRequests.getInstance().getContact(extras.getInt("AttorneyId"), AttorneyProfileActivity.this, new Callback<ContactResponse>() {
                            @Override
                            public void onResponse(Call<ContactResponse> call, Response<ContactResponse> response) {
                                response.code();

                                if (response.isSuccessful() && response.body().getContactToAttorneyResult().isResult()) {
                                    p.hide();


                                    Toast.makeText(AttorneyProfileActivity.this, "Your request is submitted successfully.", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(Intent.ACTION_SEND);

//                                    i.setData(Uri.parse("mailto:"));
//                                    i.setType("text/plain");

                                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{vcard_tv_mail.getText().toString()});
                                    i.setType("message/rfc822");
                                    startActivity(Intent.createChooser(i, "Choose an Email client :"));
                                } else {
                                    p.hide();
                                    Toast.makeText(AttorneyProfileActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ContactResponse> call, Throwable t) {
                                p.hide();
                                Toast.makeText(AttorneyProfileActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                });
                d.show();


                //   Intent i = new Intent(AttorneyProfileActivity.this, AskAttorneyActivity.class);
//                i.putExtra("KEY", ConstantUtill.ATTORNEYS_PROFILE);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        });
    }

    private void findViews() {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (Button) findViewById(R.id.back_button);
        contact_button = (Button) findViewById(R.id.contact_button);
        yearTextView = (TextView) findViewById(R.id.yer);
        name1 = (TextView) findViewById(R.id.name);
        descriptionTextView = (TextView) findViewById(R.id.description);
        Details_TextView = (TextView) findViewById(R.id.deltails_TextView);
        special = (TextView) findViewById(R.id.Speciallll);
        locat = (TextView) findViewById(R.id.locationID);
        bushrs = (TextView) findViewById(R.id.BusinesshrsID);
        imgID = (ImageView) findViewById(R.id.imafe_ID);
        headerText = (TextView) findViewById(R.id.header_text);

    }

    private void setActionTool() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
    }
MenuItem editItem;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Preferences.getInstance(this).getRole().equals("Attorney")) {
            getMenuInflater().inflate(R.menu.profile_menu, menu);
            editItem = menu.getItem(0);

        }
else {

        }


        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }else if(item.getItemId()==R.id.edit){
            startActivity(new Intent(this, Profile_lasted.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
