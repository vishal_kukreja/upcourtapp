package com.bizbrolly.com.uplawapp.activities;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akkipedia.skeleton.activities.BaseSkeletonActivity;
import com.bizbrolly.com.uplawapp.R;

public class Signup_Attorney extends BaseSkeletonActivity {

    Dialog dialog;


    public String FullName;
    String email;
    String password;
    String phone;


    private RelativeLayout relativeLayout;

    private LinearLayout contentView1;
    private EditText AttorneysBio;
    private EditText Specialization;
    private EditText Location;
    private EditText YearExperience;
    private EditText BusinessHours;
    private LinearLayout agreeCheckbox1;
    private CheckBox radioButtonId1;
    private Button signUp1;
    private TextView login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup__attorney);
        findViewById(R.id.relativeLayout1).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });
        initView();
        Bundle b1 = getIntent().getExtras();
        if (b1 != null) {
            String[] s = b1.getStringArray("s");

            Log.e("vis", s[0] + s[1] + s[2] + s[3] + "");
            FullName = s[0];
            email = s[1];
            password = s[2];
            phone = s[3];
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Signup_Attorney.this, WalkThroughActivity.class);
                //it will clear the top
                //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                //it will also clear the top
                finishAffinity();

            }
        });

        signUp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    signUp();

                }

            }
        });
    }

    private void signUp() {
        String s[] = {FullName, email, password, phone,
                AttorneysBio.getText().toString(),
                Specialization.getText().toString(),
                Location.getText().toString(),
                YearExperience.getText().toString(),
                BusinessHours.getText().toString()};
        Bundle b = new Bundle();
        b.putStringArray("s1", s);
        Intent i = new Intent(this, SignupAttorney_last.class);
        i.putExtras(b);
        startActivity(i);
    }
 /*   public void showThankYouMessage() {

        dialog.show();
    }*/

   /* private void createDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_thank_you,null);
        TextView textView_to_alter= (TextView) view.findViewById(R.id.textview_to_alter_user);
        textView_to_alter.setText("Your request has been submitted and we will respond to you as early as possible.");
        Button okButton = (Button) view.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        //dialog.setCancelable(false);
        dialog.setContentView(view);

    }
*/

    private boolean validate() {
        boolean valid = true;
        if (AttorneysBio.getText().toString().trim().length() == 0) {
            AttorneysBio.requestFocus();
            AttorneysBio.setError(Html.fromHtml("<font size='10sp'>Attorney Biography cannot be empty</font>"));
            valid = false;
        } else if (Specialization.getText().toString().trim().length() == 0) {
            Specialization.requestFocus();
            Specialization.setError(Html.fromHtml("<font size='10sp'>Specialization cannot be empty</font>"));
            valid = false;
        } else if (Location.getText().toString().trim().length() == 0) {
            Location.requestFocus();
            Location.setError(Html.fromHtml("<font size='10sp'>Location cannot be empty</font>"));
            valid = false;

        } else if (YearExperience.getText().toString().length() == 0) {
            YearExperience.requestFocus();
            YearExperience.setError(Html.fromHtml("<font size='10sp'>Experience cannot be empty</font>"));


            valid = false;
        } else if (BusinessHours.getText().toString().length() == 0) {
            BusinessHours.requestFocus();

            BusinessHours.setError(Html.fromHtml("<font size='10sp'>Business Hours cannot be empty</font>"));
            BusinessHours.setFocusable(true);

            valid = false;
        }
        return valid;
    }

    private void initView() {
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        contentView1 = (LinearLayout) findViewById(R.id.content_view1);
        AttorneysBio = (EditText) findViewById(R.id.AttorneysBio);
        Specialization = (EditText) findViewById(R.id.Specialization);
        Location = (EditText) findViewById(R.id.Location);
        YearExperience = (EditText) findViewById(R.id.YearExperience);
        BusinessHours = (EditText) findViewById(R.id.BusinessHours);
//        agreeCheckbox1 = (LinearLayout) findViewById(R.id.agree_checkbox1);
//        radioButtonId1 = (CheckBox) findViewById(R.id.radioButton_id1);
        signUp1 = (Button) findViewById(R.id.sign_up1);
        login = (TextView) findViewById(R.id.login1);
    }
}