package com.bizbrolly.com.uplawapp.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.AskResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hp on 9/12/2017.
 */

public class AskAttorney extends Fragment {
    Button submit;
    EditText questionEditText;
    Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ask_atorney, container, false);
        ((MainActivity) getActivity()).setHeadingText("Ask Attorney");
        findViews(v);
        setListner();
        createDialog();
        return v;
    }

    private void setListner() {

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (questionEditText.getText().toString().trim().length() > 0) {
                    submitResponse();
                } else {
                    questionEditText.setError("Field should not be empty");
                }

            }
        });
    }

    private void submitResponse() {
        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setMessage("Response submitting...");
        p.show();


        String question_String;
        question_String = questionEditText.getText().toString();
        WebServiceRequests.getInstance().submitIt(Preferences.getInstance(getActivity()).getUserName(),
                Preferences.getInstance(getActivity()).getAuthToken(),
                question_String,
                new Callback<AskResponse>() {
                    @Override
                    public void onResponse(Call<AskResponse> call, Response<AskResponse> response) {

                        if (response.isSuccessful() && response.body().getAskQuestionResult().isResult()) {
                            p.hide();
                            showThankYouMessage();
                            //   Preferences.getInstance(getActivity()).setData(response.body().getAskQuestionResult().getData());
                        } else {
                            Toast.makeText(getActivity(), response.body().getAskQuestionResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AskResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }


    private void findViews(View v) {
        submit = (Button) v.findViewById(R.id.submit);
        questionEditText = (EditText) v.findViewById(R.id.questionID);
    }

    private void createDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_thank_you,null);
        TextView textView_to_alter= (TextView) view.findViewById(R.id.textview_to_alter_user);
        textView_to_alter.setText("Your question has been received. Questions are typically answered within 12 hours.");
        Button okButton = (Button) view.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        //dialog.setCancelable(false);
        dialog.setContentView(view);

    }

    public void showThankYouMessage() {
        questionEditText.setText("");
        dialog.show();
    }


}
