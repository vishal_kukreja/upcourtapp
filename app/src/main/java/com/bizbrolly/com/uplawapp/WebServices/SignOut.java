package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by VishalK on 9/19/2017.
 */

public class SignOut {
    /**
     * SignOutResult : {"Data":"You've been logged out successfully","ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private SignOutResultBean SignOutResult;

    public SignOutResultBean getSignOutResult() {
        return SignOutResult;
    }

    public void setSignOutResult(SignOutResultBean SignOutResult) {
        this.SignOutResult = SignOutResult;
    }

    public static class SignOutResultBean {
        /**
         * Data : You've been logged out successfully
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private String Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public String getData() {
            return Data;
        }

        public void setData(String Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
