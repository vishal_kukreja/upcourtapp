package com.bizbrolly.com.uplawapp.WebServices;

import java.io.Serializable;
import java.util.List;

/**
 * Created by VishalK on 10/11/2017.
 */

public class feedResponse implements Serializable {
    /**
     * GetFeedResult : {"Data":[{"__type":"clsFeeds:#UpCourt.Model","Description":"Ruth Davidson criticised Tories who want the PM to resign, saying they were not led by anyone \"serious\".\r\nIt comes after ex-party chairman Grant Shapps said about 30 Tory MPs backed his call for a leadership contest.\r\nMeanwhile, Foreign Secretary Boris Johnson has messaged colleagues warning \"people are fed up with this malarkey\"","FeedId":101,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":true,"NumberOfLike":1,"Title":"Tory rebels should put up, or shut upRuth Davidson","UserId":0},{"__type":"clsFeeds:#UpCourt.Model","Description":"The rule allows employers and insurers to decline to provide birth control if doing so violates their \"religious beliefs\" or \"moral convictions\".\r\nFifty-five million women benefited from the Obama-era rule, which made companies provide free birth control.\r\nBefore taking office, Mr Trump had pledged to eliminate that requirement.","FeedId":102,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":false,"NumberOfLike":0,"Title":"Trump rolls back access to free birth control","UserId":0},{"__type":"clsFeeds:#UpCourt.Model","Description":"The rule allows employers and insurers to decline to provide birth control if doing so violates their \"religious beliefs\" or \"moral convictions\".\r\nFifty-five million women benefited from the Obama-era rule, which made companies provide free birth control.\r\nBefore taking office, Mr Trump had pledged to eliminate that requirement.","FeedId":103,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":true,"NumberOfLike":1,"Title":"Trump rolls back access to free birth control","UserId":0}],"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private GetFeedResultBean GetFeedResult;

    public GetFeedResultBean getGetFeedResult() {
        return GetFeedResult;
    }

    public void setGetFeedResult(GetFeedResultBean GetFeedResult) {
        this.GetFeedResult = GetFeedResult;
    }

    public static class GetFeedResultBean implements Serializable{
        /**
         * Data : [{"__type":"clsFeeds:#UpCourt.Model","Description":"Ruth Davidson criticised Tories who want the PM to resign, saying they were not led by anyone \"serious\".\r\nIt comes after ex-party chairman Grant Shapps said about 30 Tory MPs backed his call for a leadership contest.\r\nMeanwhile, Foreign Secretary Boris Johnson has messaged colleagues warning \"people are fed up with this malarkey\"","FeedId":101,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":true,"NumberOfLike":1,"Title":"Tory rebels should put up, or shut upRuth Davidson","UserId":0},{"__type":"clsFeeds:#UpCourt.Model","Description":"The rule allows employers and insurers to decline to provide birth control if doing so violates their \"religious beliefs\" or \"moral convictions\".\r\nFifty-five million women benefited from the Obama-era rule, which made companies provide free birth control.\r\nBefore taking office, Mr Trump had pledged to eliminate that requirement.","FeedId":102,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":false,"NumberOfLike":0,"Title":"Trump rolls back access to free birth control","UserId":0},{"__type":"clsFeeds:#UpCourt.Model","Description":"The rule allows employers and insurers to decline to provide birth control if doing so violates their \"religious beliefs\" or \"moral convictions\".\r\nFifty-five million women benefited from the Obama-era rule, which made companies provide free birth control.\r\nBefore taking office, Mr Trump had pledged to eliminate that requirement.","FeedId":103,"ImageURL":"http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg","IsLike":true,"NumberOfLike":1,"Title":"Trump rolls back access to free birth control","UserId":0}]
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private ErrorDetailBean ErrorDetail;
        private boolean Result;
        private List<DataBean> Data;

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public List<DataBean> getData() {
            return Data;
        }

        public void setData(List<DataBean> Data) {
            this.Data = Data;
        }

        public static class ErrorDetailBean implements Serializable{
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }

        public static class DataBean implements Serializable{
            /**
             * __type : clsFeeds:#UpCourt.Model
             * Description : Ruth Davidson criticised Tories who want the PM to resign, saying they were not led by anyone "serious".
             It comes after ex-party chairman Grant Shapps said about 30 Tory MPs backed his call for a leadership contest.
             Meanwhile, Foreign Secretary Boris Johnson has messaged colleagues warning "people are fed up with this malarkey"
             * FeedId : 101
             * ImageURL : http://farm4.static.flickr.com/3114/2524849923_1c191ef42e.jpg
             * IsLike : true
             * NumberOfLike : 1
             * Title : Tory rebels should put up, or shut upRuth Davidson
             * UserId : 0
             */

            private String __type;
            private String Description;
            private int FeedId;
            private String ImageURL;
            private boolean IsLike;
            private int NumberOfLike;
            private String Title;
            private int UserId;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String Description) {
                this.Description = Description;
            }

            public int getFeedId() {
                return FeedId;
            }

            public void setFeedId(int FeedId) {
                this.FeedId = FeedId;
            }

            public String getImageURL() {
                return ImageURL;
            }

            public void setImageURL(String ImageURL) {
                this.ImageURL = ImageURL;
            }

            public boolean isIsLike() {
                return IsLike;
            }

            public void setIsLike(boolean IsLike) {
                this.IsLike = IsLike;
            }

            public int getNumberOfLike() {
                return NumberOfLike;
            }

            public void setNumberOfLike(int NumberOfLike) {
                this.NumberOfLike = NumberOfLike;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String Title) {
                this.Title = Title;
            }

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int UserId) {
                this.UserId = UserId;
            }
        }
    }
}
