package com.bizbrolly.com.uplawapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.WebServices.LikeResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.WebServices.feedResponse;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedActivity extends AppCompatActivity {

    private Toolbar toolbarr;
    private TextView titleFeedTextView;
    private TextView messageFeedTextView;
    private ImageView imageFeed;
    private TextView noOfLikesTextview;
    private TextView likeTextview;
    private LinearLayout likeLayputdeta;
    private ImageView imageLikee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        initView();
        feedResponse.GetFeedResultBean.DataBean data = (feedResponse.GetFeedResultBean.DataBean) getIntent().getExtras().getSerializable("databundle");
        // feedResponse.GetFeedResultBean.DataBean data = (feedResponse.GetFeedResultBean.DataBean) bundle.getSerializable("databundle");
        setData(data);
        setActionTool(this);
        setLikes(data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

    }


    private void setLikes(final feedResponse.GetFeedResultBean.DataBean data) {


        if (data.isIsLike() == true) {
            imageLikee.setImageResource(R.drawable.ic_like_pink_24dp);
        } else {
            imageLikee.setImageResource(R.drawable.ic_favorite_black_24dp);

        }
        likeLayputdeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog p = new ProgressDialog(FeedActivity.this);
                p.setMessage("Please wait..");
                p.setCancelable(false);
                p.show();
                if (data.isIsLike() == false) {

                    WebServiceRequests.getInstance().getlike(FeedActivity.this, data.getFeedId(), 1, new Callback<LikeResponse>() {
                        @Override
                        public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {
                            if (response.isSuccessful() && response.body().getSaveLikeDislikeResult().isResult()) {
                                p.hide();
                                data.setIsLike(true);
                                imageLikee.setImageResource(R.drawable.ic_like_pink_24dp);
                                data.setNumberOfLike(data.getNumberOfLike()+1);
                                noOfLikesTextview.setText(""+(data.getNumberOfLike()));
                            }
                        }

                        @Override
                        public void onFailure(Call<LikeResponse> call, Throwable t) {
                            Toast.makeText(FeedActivity.this, "Failed to connect network, please try again", Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {

                    WebServiceRequests.getInstance().getlike(FeedActivity.this, data.getFeedId(), 0, new Callback<LikeResponse>() {
                        @Override
                        public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {
                            if (response.isSuccessful() && response.body().getSaveLikeDislikeResult().isResult()) {
                                p.hide();
                                data.setIsLike(false);
                                imageLikee.setImageResource(R.drawable.ic_favorite_black_24dp);
                                data.setNumberOfLike(data.getNumberOfLike()-1);
                                noOfLikesTextview.setText(""+(data.getNumberOfLike()));
                            }
                        }

                        @Override
                        public void onFailure(Call<LikeResponse> call, Throwable t) {
                            Toast.makeText(FeedActivity.this, "Failed to connect network, please try again", Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });
    }


    private void setData(feedResponse.GetFeedResultBean.DataBean data) {
        titleFeedTextView.setText(data.getTitle());
        Picasso.with(FeedActivity.this).load(data.getImageURL()).placeholder(R.drawable.placeholder).into(imageFeed);
        noOfLikesTextview.setText(""+data.getNumberOfLike());


                messageFeedTextView.setText(data.getDescription());
    }

    private void setActionTool(FeedActivity activity) {
        activity.setSupportActionBar(toolbarr);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity.getSupportActionBar().setTitle("Feed");

    }

    private void initView() {
        toolbarr = (Toolbar) findViewById(R.id.toolbarr);
        titleFeedTextView = (TextView) findViewById(R.id.title_feed_textView);
        messageFeedTextView = (TextView) findViewById(R.id.message_feed_textView);
        imageFeed = (ImageView) findViewById(R.id.image_feedd);
        noOfLikesTextview = (TextView) findViewById(R.id.no_of_likes_textview);
        likeTextview = (TextView) findViewById(R.id.like_textview);
        likeLayputdeta = (LinearLayout) findViewById(R.id.like_layputdeta);
        imageLikee = (ImageView) findViewById(R.id.image_likee);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}



