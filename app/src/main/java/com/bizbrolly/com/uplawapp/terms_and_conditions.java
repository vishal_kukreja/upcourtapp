package com.bizbrolly.com.uplawapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bizbrolly.com.uplawapp.activities.MainActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class terms_and_conditions extends Fragment {


    public terms_and_conditions() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setHeadingText("Terms and conditions");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
    }

}
