package com.bizbrolly.com.uplawapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkipedia.skeleton.activities.BaseSkeletonActivity;
import com.akkipedia.skeleton.utils.ScreenUtils;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.ForgetPassword;
import com.bizbrolly.com.uplawapp.WebServices.SignUpResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.urbanairship.UAirship;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseSkeletonActivity {
    boolean chk = false;
    private String imageBase64;
    public String FullName;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    TextView forgetPassword_TextView;
    Button sign_up, change_passord_button;
    public TextView login;
    public EditText firstName, lastName, email, password, phone, email_forgot_password_editText;
    CheckBox isAgree_Button;
    RadioGroup isAgree;
    Boolean agreed = false;
    Button next;
    LinearLayout check_layout;
    RelativeLayout touch_hide_relative_layout;
    CircleImageView profileImage1;
    String userChoosenTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);


        findViewById(R.id.relativeLayout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });
        findViews();
        String s = getIntent().getStringExtra("role");
        if (s.trim().equals("user")) {
            next.setVisibility(View.GONE);
        } else {
//            if (findViewById(R.id.profileImage1) == null) {
//                next.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        findViews();
//                        profileImage1.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                selectImage();
//                            }
//                        });
//                        Toast.makeText(SignUpActivity.this, "True", Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//            }
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validate()) {
                        FullName = firstName.getText().toString() + " " + lastName.getText().toString();
                        String s[] = {FullName,
                                email.getText().toString(),
                                password.getText().toString(),
                                phone.getText().toString()};
                        Intent i = new Intent(SignUpActivity.this, Signup_Attorney.class);
                        Bundle b = new Bundle();
                        b.putStringArray("s", s);
                        i.putExtras(b);
                        startActivity(i);
                    }
                }
            });

            check_layout.setVisibility(View.GONE);
            sign_up.setVisibility(View.GONE);
        }


        isAgree_Button = (CheckBox) findViewById(R.id.radioButton_id);

        isAgree_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {

                isAgree_Button.setChecked(false);
                final Dialog dialog = new Dialog(SignUpActivity.this);
                View v = getLayoutInflater().inflate(R.layout.isagreee, null);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(v);
                dialog.show();
                Button c = (Button) v.findViewById(R.id.checkedISAGREE);
                c.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isAgree_Button.setChecked(true);
                        chk = true;
                        agreed = true;
                        dialog.dismiss();

                    }
                });
//                c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        if (isChecked) {
//                            isAgree_Button.setChecked(true);
//                            chk = true;
//                            agreed=true;
//                            dialog.dismiss();
//                        }
//                        else {
//                            isAgree_Button.setChecked(false);
//                        }
//                    }
//                });

            }
        });

//        if (!isAgree_Button.isChecked()) {
//            isAgree_Button.setEnabled(true);
//
//        } else {
//            isAgree_Button.setEnabled(false);
//        }

        setListner();
    }


//    private boolean validate_Text() {
//        boolean flag=false;
//        if(password.getText().toString().length()==0)
//        {
//            password.setError("Field Cannot be empty");
//            password.setFocusable(true);
//        }else if(password.getText().toString().length()<6)
//        {
//            password.setError("Password is too short");
//            password.setFocusable(true);
//        }
//        if (password.getText().toString().length()>=6){
//            if(!password.getText().toString().matches("[A-Za-z0-9]+")){
//                password.setError("Enter alphaNumeric password");
//                password.setFocusable(true);
//            }
//        }
//        if (password.getText().toString().length()>=6){
//            if(password.getText().toString().matches("[A-Za-z0-9]+")){
//                password.setFocusable(true);
//                flag=true;
//            }
//        }
//        return flag;
//    }

    private boolean validate() {
        boolean valid = true;
        if (firstName.getText().toString().trim().length() == 0) {
            firstName.requestFocus();
            firstName.setError(Html.fromHtml("<font size='10sp'>First Name cannot be empty</font>"));
            valid = false;
        } else if (lastName.getText().toString().trim().length() == 0) {
            lastName.requestFocus();
            lastName.setError(Html.fromHtml("<font size='10sp'>Last Name cannot be empty</font>"));

            valid = false;
        } else if (email.getText().toString().trim().length() == 0) {
            email.requestFocus();
            email.setError(Html.fromHtml("<font size='10sp'>Enter Email Address</font>"));
            valid = false;

        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {

            email.setError(Html.fromHtml("<font size='10sp'>Invalid Email Address</font>"));
            email.setFocusable(true);
            valid = false;
        } else if (password.getText().toString().length() == 0) {

            password.setError(Html.fromHtml("<font size='10sp'>Field Cannot be empty</font>"));
            password.setFocusable(true);

            valid = false;
        } else if (password.getText().toString().length() < 6) {

            password.setError(Html.fromHtml("<font size='10sp'>Password must be atleast 6 characters in length</font>"));
            password.setFocusable(true);

            valid = false;
        } /*else if (password.getText().toString().length() >= 6) {
            if (password.getText().toString().matches("[A-Za-z0-9]+")) {

                password.setError(Html.fromHtml("<font size='10sp'>password should contain alphabet,integer and special character</font>"));
                password.setFocusable(true);

                valid = false;
            }else {

            }
        }*/ else if (phone.getText().toString().trim().length() == 0) {

            phone.setError(Html.fromHtml("<font size='10sp'>Phone cannot be empty</font>"));
            phone.setFocusable(true);
            valid = false;
        } else if (phone.getText().toString().trim().length() < 10) {

            phone.setError(Html.fromHtml("<font size='10sp'>Phone no. should atleast 10 digit  no</font>"));
            phone.setFocusable(true);
            valid = false;
        }
        /*if (phone.getText().toString().trim().length() < 10) {

            phone.setError(Html.fromHtml("<font size='10sp'>Phone no. should at least 10 digit  no</font>"));
            phone.setFocusable(true);
            valid = false;
        }*/

        return valid;
    }

    private void setListner() {


        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    if (agreed == true) {


                        SignUp();

                    } else {
                        Toast.makeText(SignUpActivity.this, "Please accept the terms and conditions", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignUpActivity.this, WalkThroughActivity.class);
                //it will clear the top
                //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                //it will also clear the top
                finishAffinity();
            }
        });
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[i].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Gallery";

                    galleryIntent();
                } else if (items[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri uri = data.getData();
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        profileImage1.setImageBitmap(thumbnail);
        imageBase64 = encode_Image(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri uri = data.getData();
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        profileImage1.setImageBitmap(bm);
        imageBase64 = encode_Image(bm);
    }

    public static String encode_Image(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        bitmap = Bitmap.createScaledBitmap(
                bitmap,
                bitmap.getWidth() >= 200 ? 200 : bitmap.getWidth(),
                bitmap.getHeight() >= 200 ? 200 : bitmap.getHeight(),
                true
        );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }


    private void findViews() {
        profileImage1 = (CircleImageView) findViewById(R.id.profileImage1);


        check_layout = (LinearLayout) findViewById(R.id.check_layout);
        next = (Button) findViewById(R.id.next);
        email_forgot_password_editText = (EditText) findViewById(R.id.Email_ID_Forgot_PasswordID);
        change_passord_button = (Button) findViewById(R.id.Button_Forgot_PasswordID);
        //forgetPassword_TextView = (TextView) findViewById(R.id.ForgetPassword_TextView);
        sign_up = (Button) findViewById(R.id.sign_up);
        login = (TextView) findViewById(R.id.login);
        firstName = (EditText) findViewById(R.id.firstName_id);
        lastName = (EditText) findViewById(R.id.lastName_id);
        email = (EditText) findViewById(R.id.Email_id);
        password = (EditText) findViewById(R.id.password_id);
        phone = (EditText) findViewById(R.id.Phone_id);
        //isAgree = (RadioGroup) findViewById(R.id.radioGroup_Id);
        touch_hide_relative_layout = (RelativeLayout) findViewById(R.id.relativeLayout);

    }

    private void SignUp() {
//        validate_Text();
        // validate();
        FullName = firstName.getText().toString() + " " + lastName.getText().toString();

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        progressDialog.show();


        WebServiceRequests.getInstance().signUp(
                SignUpActivity.this,
                FullName,
                email.getText().toString(),
                password.getText().toString(),
                phone.getText().toString(),
                new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        call.request();
                        progressDialog.hide();
                        if (response.isSuccessful() && response.body().getSignUpResult().isResult()) {
                            if (response.body().getSignUpResult().getData().getUserDetails().getRoleDetails()
                                    .getRoleId() == 102) {


                                UAirship.shared().getPushManager().editTags()
                                        .addTag(String.valueOf(response.body().
                                                getSignUpResult().getData().getUserDetails().getUserId()))
                                        .apply();
                                Preferences.getInstance(SignUpActivity.this).setLoginType("102");

                                Preferences.getInstance(SignUpActivity.this).setUserId(response.body().
                                        getSignUpResult().getData().getUserDetails().getUserId());

                                Preferences.getInstance(SignUpActivity.this).setAuthToken(response.body().
                                        getSignUpResult().getData().getAuthToken());

                                Preferences.getInstance(SignUpActivity.this).setDOB((String) response
                                        .body().getSignUpResult().getData().getUserDetails().getDOB());

                                Preferences.getInstance(SignUpActivity.this).setUserName(response.body().
                                        getSignUpResult().getData().getUsername());
                                //                                Preferences.getInstance(LoginActivity.this).setDOB(response.body().getSignInResult()
                                //                                .getData().dob());
                                Preferences.getInstance(SignUpActivity.this).setName(response.body().
                                        getSignUpResult().getData().getUserDetails().getFirstName());
                                Preferences.getInstance(SignUpActivity.this).setLoggedIn(true);
                                Preferences.getInstance(SignUpActivity.this).setPhone((String) response.body().
                                        getSignUpResult().getData().getUserDetails().getPhone());
                                Preferences.getInstance(SignUpActivity.this).setfbLogin(true);
//                                                    Intent i = new Intent(getActivity(), DoctorDashboardActivity.class);
//                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(i);
//                                                    finish();
                            }
//                                                      else if (response.body().getSignInResult().getData().getUserDetails().
//                                                        getRoleDetails().getRoleId() == 101) {
//                                                    Preferences.getInstance(LoginActivity.this).setLoginType("101");
//
//                                                    Preferences.getInstance(LoginActivity.this).setUserId(response.body().
//                                                            getSignInResult().getData().getUserDetails().getUserId());
//                                                    Preferences.getInstance(LoginActivity.this).setAuthToken(response.body().
//                                                            getSignInResult().getData().getAuthToken());
//                                                    Preferences.getInstance(LoginActivity.this).setUserName(response.body().
//                                                            getSignInResult().getData().getUsername());
//                                                    Preferences.getInstance(LoginActivity.this).setDOB(response
//                                                            .body().getSignInResult().getData().getUserDetails().getDOB());
//                                                    Preferences.getInstance(LoginActivity.this).setName(response.body().
//                                                            getSignInResult().getData().getUserDetails().getFirstName());
//                                                    Preferences.getInstance(LoginActivity.this).setLoggedIn(true);
//                                                    Preferences.getInstance(LoginActivity.this).setPhone(response.body().
//                                                            getSignInResult().getData().getUserDetails().getPhone());
//                                                    Preferences.getInstance(LoginActivity.this).setfbLogin(true);
//                                                    Intent i = new Intent(LoginActivity.this, UserDashBoardActivity.class);
//                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(i);
//                                                    //Toast.makeText(SignInActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                                                    finish();
                            //               }
                            Intent i = new Intent(SignUpActivity.this, MainActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finishAffinity();

                        } else {
                            Toast.makeText(SignUpActivity.this, response.body().getSignUpResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        progressDialog.hide();
                        Toast.makeText(SignUpActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void forgetPassword() {
        change_passord_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebServiceRequests.getInstance().forgetPassword(email_forgot_password_editText.getText().toString(), new Callback<ForgetPassword>() {
                    @Override
                    public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {

                    }

                    @Override
                    public void onFailure(Call<ForgetPassword> call, Throwable t) {
                        Toast.makeText(SignUpActivity.this, "Network Failure", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    AlertDialog alertDialog;

    private void forgotPasswordDialog() {
        LinearLayout linearLayout = new LinearLayout(this);
        final EditText emailEditText = new EditText(this);
        linearLayout.addView(emailEditText);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) emailEditText.getLayoutParams();
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.setMargins(ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10));
        emailEditText.setHint("Enter Email Address");
        emailEditText.setLayoutParams(layoutParams);
        alertDialog = new AlertDialog.Builder(this).setView(linearLayout)
                .setTitle("Forgot Password?")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Reset", null).create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (emailEditText.getText().toString().trim().isEmpty()) {
                            emailEditText.setError("Field should not be empty");
                        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                            emailEditText.setError("Enter Valid Email ID");
                        } else {
                            continue_func(emailEditText.getText().toString().trim());
                        }
                    }
                });
            }
        });
        alertDialog.show();
    }

    private void continue_func(final String email_EditView) {
        showProgressDialog();
        WebServiceRequests.getInstance().forgetPassword(email_EditView, new Callback<ForgetPassword>() {
            @Override
            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                Preferences.getInstance(SignUpActivity.this).setEmailForgot(email_EditView);
                Toast.makeText(SignUpActivity.this, "A mail has been sent to you", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
                alertDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(SignUpActivity.this, "Network Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
