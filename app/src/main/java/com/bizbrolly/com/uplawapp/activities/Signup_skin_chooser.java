package com.bizbrolly.com.uplawapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.bizbrolly.com.uplawapp.R;

public class Signup_skin_chooser extends AppCompatActivity {

    private TextView userSignupTextView;
    private TextView AttorneySignupTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_skin_chooser);
        initView();
        userSignupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Signup_skin_chooser.this,SignUpActivity.class);
                i.putExtra("role","user");
                startActivity(i);

            }
        });
        AttorneySignupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Signup_skin_chooser.this,SignUpActivity.class);
                i.putExtra("role","Attorney");
                startActivity(i);
            }
        });
    }

    private void initView() {
        userSignupTextView = (TextView) findViewById(R.id.userSignup_textView);
        AttorneySignupTextView = (TextView) findViewById(R.id.AttorneySignup_textView);
    }
}
