package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by vishal on 24-09-2017.
 */

public class AskResponse {


    /**
     * AskQuestionResult : {"Data":"Your question has been received and we will response to your email shortly.","ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private AskQuestionResultBean AskQuestionResult;

    public AskQuestionResultBean getAskQuestionResult() {
        return AskQuestionResult;
    }

    public void setAskQuestionResult(AskQuestionResultBean AskQuestionResult) {
        this.AskQuestionResult = AskQuestionResult;
    }

    public static class AskQuestionResultBean {
        /**
         * Data : Your question has been received and we will response to your email shortly.
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private String Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public String getData() {
            return Data;
        }

        public void setData(String Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
