package com.bizbrolly.com.uplawapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.AnswerQResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnswerQ_Activity extends AppCompatActivity {

    private RelativeLayout homeToolbarId;
    private Toolbar toolbar;
    private TextView headerText;
    private TextView questionTextView;
    private EditText answerQuestion;
    private Button submit;
    int Questionid;
    long userId;
    String Answer, answered, time, timeAttorney;
    String profile_image;
    String username;
    boolean NotAnswered;
    Dialog dialog;
    private CircleImageView profileImageId;
    private TextView textViewItemRecyclerView;
    private TextView textViewTime;
    private LinearLayout lv;
    private TextView answeredQues;
    private TextView answeee;
    private TextView answeredddd;
    private LinearLayout click;
    private CircleImageView profileImageIdAttorney;
    private TextView textViewItemRecyclerViewAttorney;
    private TextView textViewTimeAttorney;
    private TextView answereddddAttorney;
String AttorneysBio_s,BusinessHours_s,EmailId_s,Name_s,Phone_s,Specialization_s,UserImage_s,YearExperience_s,Location_s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_q_);
        initView();
        Bundle b = getIntent().getExtras();
        Questionid = b.getInt("questionid");
        Answer = b.getString("question");
        username = b.getString("username");
        profile_image = b.getString("profile_image");
        answered = b.getString("ans");
        time = b.getString("time");
        timeAttorney = b.getString("timeAttorney");
        userId = b.getLong("userId");
        NotAnswered = b.getBoolean("NotAnswered");


        AttorneysBio_s = b.getString("AttorneysBio");
        BusinessHours_s = b.getString("BusinessHours");
        EmailId_s = b.getString("EmailId");
        Name_s = b.getString("Name");
        Phone_s = b.getString("Phone");
        Specialization_s = b.getString("Specialization");

        UserImage_s = b.getString("UserImage");
        YearExperience_s = b.getString("YearExperience");
        Location_s=b.getString("Location");





        textViewItemRecyclerView.setText(username);
        if (!NotAnswered) {
            answereddddAttorney.setVisibility(View.GONE);
            click.setVisibility(View.GONE);
            answeredQues.setVisibility(View.GONE);
            answeee.setText("Unanswered");
            answeredddd.setText("New");
        } else {

            if (!UserImage_s.isEmpty()) {
                Picasso.with(this).load(UserImage_s).into(profileImageIdAttorney);
            }
            answereddddAttorney.setVisibility(View.VISIBLE);
            textViewItemRecyclerViewAttorney.setText(Name_s);
            click.setVisibility(View.VISIBLE);
            textViewTimeAttorney.setVisibility(View.VISIBLE);
            textViewTimeAttorney.setText(time);

            click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent i = new Intent(AnswerQ_Activity.this, AttorneyProfileActivity.class);
                    i.putExtra("year",YearExperience_s);
                    i.putExtra("name",Name_s);
                    i.putExtra("Specialization",Specialization_s);
                    i.putExtra("Location", Location_s);
                    i.putExtra("Business_Hours",BusinessHours_s);
                    i.putExtra("Attorney_Bio", AttorneysBio_s);
                    i.putExtra("Email",EmailId_s);
                    i.putExtra("Image", UserImage_s);
                    i.putExtra("phone",Phone_s );
//                i.putExtra("AttorneyId", (int)Preferences.getInstance(MainActivity.this).getAttorneyId());
                    startActivity(i);
                }
            });
        }


        if (!profile_image.isEmpty()) {
            Picasso.with(this).load(profile_image).into(profileImageId);
        }
        questionTextView.setText(Answer.toString().trim());

        if (!Preferences.getInstance(this).getRole().equals("Attorney")) {

            answerQuestion.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            if (!answered.isEmpty()) {
                answeredQues.setText(answered.trim());
            }
            textViewTime.setText(timeAttorney);

        } else {
//            questionTextView.setFocusable(true);
//            questionTextView.requestFocus();
            textViewTime.setText(timeAttorney);
            lv.setVisibility(View.GONE);
        }

        setToolbar();
        setListner();

    }

    private void setToolbar() {
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void initView() {
        homeToolbarId = (RelativeLayout) findViewById(R.id.home_toolbar_id);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        headerText = (TextView) findViewById(R.id.header_text);
        questionTextView = (TextView) findViewById(R.id.questionText_view);
        answerQuestion = (EditText) findViewById(R.id.questionID);
        submit = (Button) findViewById(R.id.submit);
        profileImageId = (CircleImageView) findViewById(R.id.profile_image_id);
        textViewItemRecyclerView = (TextView) findViewById(R.id.textView_ItemRecyclerView);
        textViewTime = (TextView) findViewById(R.id.textView_time);
        lv = (LinearLayout) findViewById(R.id.lv);
        answeredQues = (TextView) findViewById(R.id.answeredQues);
        answeee = (TextView) findViewById(R.id.answeee);
        answeredddd = (TextView) findViewById(R.id.answeredddd);
        click = (LinearLayout) findViewById(R.id.click);
        profileImageIdAttorney = (CircleImageView) findViewById(R.id.profile_image_idAttorney);
        textViewItemRecyclerViewAttorney = (TextView) findViewById(R.id.textView_ItemRecyclerViewAttorney);
        textViewTimeAttorney = (TextView) findViewById(R.id.textView_timeAttorney);
        answereddddAttorney = (TextView) findViewById(R.id.answereddddAttorney);
    }

    private void setListner() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answerQuestion.getText().toString().trim().length() > 0) {
                    submitResponse();
                } else {
                    answerQuestion.setError("Field should not be empty");
                }
            }
        });
    }

    private void submitResponse() {
        final ProgressDialog p = new ProgressDialog(AnswerQ_Activity.this);
        p.setMessage("Response submitting...");
        p.show();

        String question_String;
        question_String = answerQuestion.getText().toString();
        WebServiceRequests.getInstance().answerQuestion(Preferences.getInstance(AnswerQ_Activity.this).getUserName(),
                Questionid,
                AnswerQ_Activity.this,
                Preferences.getInstance(this).getAuthToken(),
                answerQuestion.getText().toString().trim(), userId,
                new Callback<AnswerQResponse>() {
                    @Override
                    public void onResponse(Call<AnswerQResponse> call, Response<AnswerQResponse> response) {
                        if (response.isSuccessful() && response.body().getAnswerResult().isResult()) {

                            p.hide();


                            createDialog();
                            showThankYouMessage();

                        } else {
                            p.hide();
                            Toast.makeText(AnswerQ_Activity.this, response.body().getAnswerResult().getErrorDetail().getErrorDetails(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<AnswerQResponse> call, Throwable t) {
                        Toast.makeText(AnswerQ_Activity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                }
        );
      /*  WebServiceRequests.getInstance().submitIt(Preferences.getInstance(AskAttorneyActivity.this).getUserName(),
                Preferences.getInstance(AskAttorneyActivity.this).getAuthToken(),
                question_String,
                new Callback<AskResponse>() {
                    @Override
                    public void onResponse(Call<AskResponse> call, Response<AskResponse> response) {

                        if (response.isSuccessful() && response.body().getAskQuestionResult().isResult()) {
                            p.hide();
                            createDialog();
                            showThankYouMessage();
                            //   Preferences.getInstance(AskAttorneyActivity.this).setData(response.body().getAskQuestionResult().getData());
                        } else {
                            Toast.makeText(AskAttorneyActivity.this, response.body().getAskQuestionResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AskResponse> call, Throwable t) {
                        Toast.makeText(AskAttorneyActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                }
        );
*/
    }

    private void createDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_thank_you, null);
        TextView textView_to_alter = (TextView) view.findViewById(R.id.textview_to_alter_user);
        textView_to_alter.setText("Your answer has been submitted.");

        Button okButton = (Button) view.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        //dialog.setCancelable(false);
        dialog.setContentView(view);

    }

    public void showThankYouMessage() {
        answerQuestion.setText("");
        dialog.show();
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        View_questions v=new View_questions();
       v.View_questions(1);
    }*/
}
