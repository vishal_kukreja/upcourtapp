package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by bizbrolly on 2/13/17.
 */

public class Constants {

    public static final String BASE_URL = "http://BIZBROLLYDEV.CLOUDAPP.NET/UpCourt/UpCourt.svc/";
    public static final String Add_Url = "http://bizbrollydev.cloudapp.net/RadiusDevServices/Radius.svc/";
    public static final String API_KEY = "EV-OCT-EVENT-BIZBR-2017OLLY";


    // public static final String MedicalProfession = "MedicalProfession";


    public static final class PARTIALS {
        public static final String Adds = "GetAdvertisment/";
        public static final String SIGN_IN = "SignIn/";
        public static final String SIGN_OUT = "SignOut/";
        public static final String SIGN_UP = "SignUp/";
        public static final String GET_ATTORNEY_LIST = "GetAttorneyList/";
        public static final String GET_QUESTIONS_ATTORNEY = "GetQuestion/";
        public static final String AskAttorney = "AskQuestion/";
        //        public static final String GET_USER_DETAILS = "GetUserDetails/";
//        public static final String GET_MASTER_DATA = "GetMasterData/";
//        public static final String GET_DOCTOR_BY_SPECIALIZATION_DATA = "GetDoctorBySpecialization/";
//        public static final String SAVE_APPOINTMENT = "SaveAppointMent/";
//        public static final String GET_APPOINTMENT = "GetAppointMents/";
        public static final String CHANGE_PASSWORD = "ChangePassword/";
        public static final String FORGET_PASSWORD = "ForgetPassword/";
//        public static final String UPDATE_PROFILE = "UpdateProfile/";
public static final String UPDATE_PROFILE = "UpdateUserAndAttorneyDetails/";
        public static final String FEED = "GetFeed/";
        public static final String LIKE = "SaveLikeDislike/";
        public static final String CONTACT_BTN = "ContactToAttorney/";
        public static final String Answer = "Answer/";
    }

    public static final class KEYS {
        public static final String Answer = "Answer";
        public static final String Newname = "Name";
        public static final String Newphone = "Phone";
        public static final String Username = "Username";
        public static final String FacebookId = "FacebookId";
        public static final String AuthToken = "AuthToken";
        public static final String APIKey = "APIKey";
        public static final String Password = "Password";
        public static final String ClientIPAddress = "ClientIPAddress";
        public static final String ClientMachineName = "ClientMachineName";

        public static final String Questions = "Question";
        public static final String QuestionId = "QuestionId";
        public static final String MyQuestion = "MyQuestion";


        public static final String Old_password = "OldPassword";
        public static final String NEw_password = "NewPassword";

        public static final String AttorneysBio = "AttorneysBio";
        public static final String Specialization = "Specialization";
        public static final String Location = "Location";
        public static final String YearExperience = "YearExperience";
        public static final String BusinessHours = "BusinessHours";
        public static final String imageBase64 = "Imagebase64Content";

//        public static final String Phone = "Phone";
//        public static final String Role = "Role";
//        public static final String DOB = "DOB";
//        public static final String DoctorFee = "DoctorFee";
//        public static final String DoctorBio = "DoctorBio";
//        public static final String Imagebase64Content = "Imagebase64Content";
//
//        public static final String MasterDataType = "MasterDataType";
//        public static final String MasterDataId = "MasterDataId";
//
//        public static final String SpecializationId = "SpecializationId";
//
//        public static final String DoctorId = "DoctorId";
//        public static final String AppointmentDate = "AppointmentDate";
//        public static final String AppointmentCode = "AppointmentCode";
//
//        public static final String Appointmentof = "Appointmentof";
//        public static final String PatientId = "PatientId";
//        public static final String TextId = "TextId";
//        public static final String TextType = "TextType";

        public static final String FirstName = "FirstName";
        //"Username":"new111@gmail.com",
        //    "Password":"new1234",
        public static final String Phone = "Phone";
        public static final String Email = "Email";
        public static final String UserImage = "UserImage";
        public static final String Imagebase64Content = "Imagebase64Content";
        //                "ClientIPAddress":"101.102.103.178111",
//                "ClientMachineName":"BizBrolly",
//                "APIKey":"EV-OCT-EVENT-BIZBR-2017OLLY",
        public static final String Role = "Role";
        public static final String IsAgree = "IsAgree";

    }
}
