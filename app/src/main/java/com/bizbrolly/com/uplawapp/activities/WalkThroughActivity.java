package com.bizbrolly.com.uplawapp.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.akkipedia.skeleton.activities.BaseSkeletonActivity;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.adapter.WalkThroughAdapter;
import com.bizbrolly.com.uplawapp.utills.Preferences;

public class WalkThroughActivity extends BaseSkeletonActivity {

    ViewPager view_pager;
    WalkThroughAdapter walkThroughAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        findviews();
        if (Preferences.getInstance(this).isLoggedIn()) {
            Intent i = new Intent(WalkThroughActivity.this, MainActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(i);
            finish();
        }
        setData();
    }

    private void setData() {
        walkThroughAdapter = new WalkThroughAdapter(getSupportFragmentManager());
        view_pager.setAdapter(walkThroughAdapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    walkThroughAdapter.learnVideoFragment.pauseVideo();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setPagerState(int position) {
        view_pager.setCurrentItem(position, true);
    }

    private void findviews() {
        view_pager = (ViewPager) findViewById(R.id.view_pager);
    }

}
