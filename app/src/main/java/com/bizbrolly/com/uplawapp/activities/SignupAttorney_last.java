package com.bizbrolly.com.uplawapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.SignUpResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.urbanairship.UAirship;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupAttorney_last extends AppCompatActivity {


    public String FullName;
    String email;
    String password;
    String phone;
    String AttorneysBio;
    String Specialization;
    String Location;
    String YearExperience;
    String BusinessHours;


    private CircleImageView profileImage1;
    String userChoosenTask;
    boolean chk = false;
    private int REQUEST_CAMERA = 1;
    private String imageBase64;
    private int SELECT_FILE = 2;
    private Button signUp1;
    private RelativeLayout relativeLayout1;
    private ImageView logo;
    private RelativeLayout contentView2;
    private LinearLayout agreeCheckbox1;
    private CheckBox radioButtonId1;
    private TextView login1;

    CheckBox isAgree_Button;
    Boolean agreed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_attorney_last);
        initView();
        listener();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            String s[] = b.getStringArray("s1");
            FullName = s[0];
            email = s[1];
            password = s[2];
            phone = s[3];

            AttorneysBio = s[4];
            Specialization = s[5];
            YearExperience = s[6];
            BusinessHours = s[7];
            Location = s[8];

        }
    }

    private void listener() {
        isAgree_Button = (CheckBox) findViewById(R.id.radioButton_id1);

        isAgree_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {

                isAgree_Button.setChecked(false);
                final Dialog dialog = new Dialog(SignupAttorney_last.this);
                View v = getLayoutInflater().inflate(R.layout.isagreee, null);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(v);
                dialog.show();
                Button c = (Button) v.findViewById(R.id.checkedISAGREE);
                c.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isAgree_Button.setChecked(true);
                        chk = true;
                        agreed = true;
                        dialog.dismiss();
                    }
                });
            }
        });

        signUp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                signUp();
                if (agreed == true) {
                    signUp();
                } else {
                    Toast.makeText(SignupAttorney_last.this, "Please accept the terms and conditions", Toast.LENGTH_SHORT).show();
                }
               /* if (validate()) {
                    if (agreed == true) {

                    } else {
                        Toast.makeText(Signup_Attorney.this, "Please accept the terms and conditions", Toast.LENGTH_SHORT).show();
                    }

                }*/
            }
        });
        profileImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }

    private void signUp() {

        final ProgressDialog progressDialog = new ProgressDialog(SignupAttorney_last.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        progressDialog.show();
        WebServiceRequests.getInstance().signUpAttorney(SignupAttorney_last.this, FullName,
                email,
                password,
                phone,
                AttorneysBio,
                Specialization,
                Location,
                YearExperience,
                BusinessHours,
                imageBase64,
                new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                        if (response.isSuccessful() && response.body().getSignUpResult().isResult()) {

                            progressDialog.hide();

                            UAirship.shared().getPushManager().editTags()
                                    .addTag(String.valueOf(response.body().
                                            getSignUpResult().getData().getUserDetails().getUserId()))
                                    .apply();
                            UAirship.shared().getPushManager().editTags()
                                    .addTag("Attorney")
                                    .apply();
                            Preferences.getInstance(SignupAttorney_last.this).setApproved(response.body().getSignUpResult().getData().isIsApproved());

                            Preferences.getInstance(SignupAttorney_last.this).setAuthToken(response.body().
                                    getSignUpResult().getData().getAuthToken());

                            Preferences.getInstance(SignupAttorney_last.this).setUserName((String) response.body().
                                   getSignUpResult().getData().getUsername());

                            Preferences.getInstance(SignupAttorney_last.this).setName((String) response.body().
                                    getSignUpResult().getData().getUserDetails().getFirstName());

                            Preferences.getInstance(SignupAttorney_last.this).setPhone((String) response.body().
                                    getSignUpResult().getData().getUserDetails().getPhone());

                            Preferences.getInstance(SignupAttorney_last.this).setImage((String) response.body().getSignUpResult().getData().getUserDetails().getUserImage());

                            Preferences.getInstance(SignupAttorney_last.this).setLoggedIn(true);

                            Preferences.getInstance(SignupAttorney_last.this).setRole(
                                    response.body().getSignUpResult().getData().getUserDetails().getRoleDetails().getRole().toString());
                            Intent i = new Intent(SignupAttorney_last.this, MainActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finishAffinity();


                        } else {
                            progressDialog.hide();
                            Toast.makeText(SignupAttorney_last.this, response.body().getSignUpResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        progressDialog.hide();
                        Toast.makeText(SignupAttorney_last.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[i].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Gallery";

                    galleryIntent();
                } else if (items[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri uri = data.getData();
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        profileImage1.setImageBitmap(thumbnail);
        imageBase64 = encode_Image(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri uri = data.getData();
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        profileImage1.setImageBitmap(bm);
        imageBase64 = encode_Image(bm);
    }

    public static String encode_Image(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        bitmap = Bitmap.createScaledBitmap(
                bitmap,
                bitmap.getWidth() >= 200 ? 200 : bitmap.getWidth(),
                bitmap.getHeight() >= 200 ? 200 : bitmap.getHeight(),
                true
        );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private void initView() {
        profileImage1 = (CircleImageView) findViewById(R.id.profileImage1);
        signUp1 = (Button) findViewById(R.id.sign_up1);
        relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        logo = (ImageView) findViewById(R.id.logo);
        contentView2 = (RelativeLayout) findViewById(R.id.content_view2);
        agreeCheckbox1 = (LinearLayout) findViewById(R.id.agree_checkbox1);
        radioButtonId1 = (CheckBox) findViewById(R.id.radioButton_id1);
        login1 = (TextView) findViewById(R.id.login1);
    }
}
