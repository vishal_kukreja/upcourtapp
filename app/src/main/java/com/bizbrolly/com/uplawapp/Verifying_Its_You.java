package com.bizbrolly.com.uplawapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class Verifying_Its_You extends Fragment {

Button Continue_buttonView;
    public Verifying_Its_You() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View v=  inflater.inflate(R.layout.fragment_verifying__its__you, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        findViews(v);
        setlistener();
        // Inflate the layout for this fragment
        return v;
    }

    private void setlistener() {
        Continue_buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container_forgetPassword,new ChangePassword()).addToBackStack(null).commit();
            }
        });
    }

    private void findViews(View v) {
        Continue_buttonView = (Button) v.findViewById(R.id.Continue_ButtonView);

    }

}
