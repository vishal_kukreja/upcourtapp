package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by VishalK on 10/12/2017.
 */

public class AdvertisementResponse {
    /**
     * GetAdvertismentResult : {"Data":{"__type":"clsAdvertisment:#UpCourt.Model","AdvertismentId":101,"AttorneyId":0,"ImagePath":"https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2017/imagefirebal.jpg","WebsiteURL":"www.google.com"},"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private GetAdvertismentResultBean GetAdvertismentResult;

    public GetAdvertismentResultBean getGetAdvertismentResult() {
        return GetAdvertismentResult;
    }

    public void setGetAdvertismentResult(GetAdvertismentResultBean GetAdvertismentResult) {
        this.GetAdvertismentResult = GetAdvertismentResult;
    }

    public static class GetAdvertismentResultBean {
        /**
         * Data : {"__type":"clsAdvertisment:#UpCourt.Model","AdvertismentId":101,"AttorneyId":0,"ImagePath":"https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2017/imagefirebal.jpg","WebsiteURL":"www.google.com"}
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private DataBean Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public DataBean getData() {
            return Data;
        }

        public void setData(DataBean Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class DataBean {
            /**
             * __type : clsAdvertisment:#UpCourt.Model
             * AdvertismentId : 101
             * AttorneyId : 0
             * ImagePath : https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2017/imagefirebal.jpg
             * WebsiteURL : www.google.com
             */

            private String __type;
            private int AdvertismentId;
            private int AttorneyId;
            private String ImagePath;
            private String WebsiteURL;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public int getAdvertismentId() {
                return AdvertismentId;
            }

            public void setAdvertismentId(int AdvertismentId) {
                this.AdvertismentId = AdvertismentId;
            }

            public int getAttorneyId() {
                return AttorneyId;
            }

            public void setAttorneyId(int AttorneyId) {
                this.AttorneyId = AttorneyId;
            }

            public String getImagePath() {
                return ImagePath;
            }

            public void setImagePath(String ImagePath) {
                this.ImagePath = ImagePath;
            }

            public String getWebsiteURL() {
                return WebsiteURL;
            }

            public void setWebsiteURL(String WebsiteURL) {
                this.WebsiteURL = WebsiteURL;
            }
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
