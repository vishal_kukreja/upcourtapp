package com.bizbrolly.com.uplawapp.utills;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class Preferences {
    private static Preferences sharedInstance;
    private SharedPreferences sharedPreferences;

    Boolean loggedIn = false;

    private class Keys {

        private static final String AUTH_TOKEN = "AUTH_TOKEN";
        private static final String USER_NAME = "USER_NAME";
        private static final String LOGGED_IN = "LOGGED_IN";
        private static final String NAME = "NAME";
        private static final String DOB = "DOB";
        private static final String PHONE = "PHONE";
        private static final String FB_LOGIN = "FB_LOGIN";
        public static final String USER_TYPE = "USER_TYPE";
        public static final String USER_ID = "USER_ID";
        public static final String MARKET_ID = "MARKET_ID";
        public static final String QUESTION = "QUESTION";
        public static final String DATA = "data";
        public static final String ROLE = "role";
        public static final String EMAIL_FORGOT_PASSWORD = "EMAIL_FORGOT";
        public static final String IMAGE = "IMAGE_PROFILE";
        public static final String LIKE = "Like";
        public static final String APPROVED = "approved";

        public static final String Attorneys_Biography_ID="Attorneys_Biography_ID";
        public static final String SpecializationID="SpecializationID";
        public static final String LocationID="LocationID";
        public static final String YearExperienceId="YearExperienceId";
        public static final String BusinessHoursDD="BusinessHoursDD";
        public static final String UserId="UserId";
    }

    public Preferences(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static Preferences getInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new Preferences(context);
        }
        return sharedInstance;
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(Keys.LOGGED_IN, loggedIn);
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
        sharedPreferences.edit().putBoolean(Keys.LOGGED_IN, loggedIn).apply();
    }

    public boolean isfbLogin() {
        return sharedPreferences.getBoolean(Keys.FB_LOGIN, false);
    }

    public void setfbLogin(boolean loggedIn) {
        sharedPreferences.edit().putBoolean(Keys.FB_LOGIN, loggedIn).apply();
    }

    public String getAuthToken() {
        return sharedPreferences.getString(Keys.AUTH_TOKEN, "");
    }

    public void setAuthToken(String authToken) {
        sharedPreferences.edit().putString(Keys.AUTH_TOKEN, authToken).apply();
    }


    //Attorney..............................................................................................
    public String getBusinessHoursDD() {
        return sharedPreferences.getString(Keys.BusinessHoursDD, "");
    }

    public void setBusinessHoursDD(String  BusinessHoursDD) {
        sharedPreferences.edit().putString(Keys.BusinessHoursDD,BusinessHoursDD).apply();
    }

    /*public String getUserId() {
        return sharedPreferences.getString(Keys.UserId, "");
    }

    public void setUserId(String  UserId) {
        sharedPreferences.edit().putString(Keys.BusinessHoursDD,UserId).apply();
    }*/

    public String getYearExperienceId() {
        return sharedPreferences.getString(Keys.YearExperienceId, "");
    }

    public void setYearExperienceId(String  YearExperienceId) {
        sharedPreferences.edit().putString(Keys.YearExperienceId,YearExperienceId).apply();
    }

    public String getLocationID() {
        return sharedPreferences.getString(Keys.LocationID, "");
    }

    public void setLocationID(String  LocationID) {
        sharedPreferences.edit().putString(Keys.LocationID,LocationID).apply();
    }

    public String getSpecializationID() {
        return sharedPreferences.getString(Keys.SpecializationID, "");
    }

    public void setSpecializationID(String  SpecializationID) {
        sharedPreferences.edit().putString(Keys.SpecializationID,SpecializationID).apply();
    }


    public String getAttorneys_Biography_ID() {
        return sharedPreferences.getString(Keys. Attorneys_Biography_ID, "");
    }

    public void setAttorneys_Biography_ID(String  Attorneys_Biography_ID) {
        sharedPreferences.edit().putString(Keys. Attorneys_Biography_ID,  Attorneys_Biography_ID).apply();
    }







    public String getUserName() {
        return sharedPreferences.getString(Keys.NAME, "");
    }

    public void setUserName(String userName) {
        sharedPreferences.edit().putString(Keys.NAME, userName).apply();
    }

    public String getDOB() {
        return sharedPreferences.getString(Keys.DOB, "");
    }

    public void setDOB(String dob) {
        sharedPreferences.edit().putString(Keys.DOB, dob).apply();
    }

    public String getLoginType() {
        return sharedPreferences.getString(Keys.USER_TYPE, "");
    }

    public void setLoginType(String userTypeId) {
        sharedPreferences.edit().putString(Keys.USER_TYPE, userTypeId).apply();
    }


    public String getName() {
        return sharedPreferences.getString(Keys.USER_NAME, "");
    }

    public void setName(String name) {
        sharedPreferences.edit().putString(Keys.USER_NAME, name).apply();
    }


    public String getPhone() {
        return sharedPreferences.getString(Keys.PHONE, "");
    }

    public void setPhone(String phone) {
        sharedPreferences.edit().putString(Keys.PHONE, phone).apply();
    }


    public int getMarketId() {
        return sharedPreferences.getInt(Keys.MARKET_ID, 0);
    }

    public void setMarketId(int marketId) {
        sharedPreferences.edit().putInt(Keys.MARKET_ID, marketId).apply();
    }

    public int getLike() {
        return sharedPreferences.getInt(Keys.LIKE, 0);
    }

    public void setLike(int like) {
        sharedPreferences.edit().putInt(Keys.LIKE, like).apply();

    }

    public int getUserId() {
        return sharedPreferences.getInt(Keys.USER_ID, 0);
    }

    public void setUserId(int userId) {
        sharedPreferences.edit().putInt(Keys.USER_ID, userId).apply();

    }

    public String getQuestion() {
        return sharedPreferences.getString(Keys.QUESTION, "");
    }

    public void setQuestion(String question) {
        sharedPreferences.edit().putString(Keys.QUESTION, question).apply();

    }

    public String getEmailForgot() {
        return sharedPreferences.getString(Keys.EMAIL_FORGOT_PASSWORD, "");
    }

    public void setEmailForgot(String emailForgott) {
        sharedPreferences.edit().putString(Keys.EMAIL_FORGOT_PASSWORD, emailForgott).apply();

    }

    public String getImage() {
        return sharedPreferences.getString(Keys.IMAGE, "");
    }

    public void setImage(String image) {
        sharedPreferences.edit().putString(Keys.IMAGE, image).apply();

    }

    public void setData(String data) {
        sharedPreferences.edit().putString(Keys.DATA, data).apply();
    }

    public String getData() {
        return sharedPreferences.getString(Keys.DATA, "");
    }

    public void setRole(String role) {
        sharedPreferences.edit().putString(Keys.ROLE,role).apply();
    }

    public String getRole() {
        return sharedPreferences.getString(Keys.ROLE, "");
    }

    public boolean getApproved() {
        return sharedPreferences.getBoolean(Keys.APPROVED, false);
    }

    public void setApproved(boolean approved) {
        sharedPreferences.edit().putBoolean(Keys.APPROVED, approved).apply();
    }

    public void destroy_SharedPreference() {
        sharedPreferences.edit().putString(Keys.ROLE,"").apply();
        sharedPreferences.edit().putBoolean(Keys.LOGGED_IN, false).apply();
        sharedPreferences.edit().putBoolean(Keys.FB_LOGIN, false).apply();
        sharedPreferences.edit().putString(Keys.AUTH_TOKEN, "").apply();
        sharedPreferences.edit().putString(Keys.NAME, "").apply();
        sharedPreferences.edit().putString(Keys.DOB, "").apply();

        sharedPreferences.edit().putString(Keys.USER_TYPE, "").apply();
        sharedPreferences.edit().putString(Keys.USER_NAME, "").apply();
        sharedPreferences.edit().putString(Keys.PHONE, "").apply();
        sharedPreferences.edit().putInt(Keys.MARKET_ID, 0).apply();
        sharedPreferences.edit().putInt(Keys.USER_ID, 0).apply();


    }

}
