package com.bizbrolly.com.uplawapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Forget_Password extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget__password);

        getSupportFragmentManager().beginTransaction().replace(R.id.container_forgetPassword,new Forgot_Email_fragment()).commit();

    }
}
