package com.bizbrolly.com.uplawapp;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.WebServices.ForgetPassword;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.WalkThroughActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Forgot_Email_fragment extends Fragment {

    Button continue_buttonvView;
    EditText email_EditView;
    Dialog dialog;
    public Forgot_Email_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View v=inflater.inflate(R.layout.fragment_forgot__email_fragment, container, false);
        findViews(v);
        //((AppCompatActivity)getActivity()).setHeadingText("Forget Password");
        // ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        createDialog();
        setlistener();
        // Inflate the layout for this fragment
        return v;
    }

    private void setlistener() {
        continue_buttonvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email_EditView.getText().toString().isEmpty()){
                    email_EditView.setError("Field should not be empty");
                }
                else
               if (!Patterns.EMAIL_ADDRESS.matcher(email_EditView.getText().toString()).matches())
                {       email_EditView.setError("Enter Valid Email ID");
                    }else {
                   continue_func();
                }

            }
        });
    }
    private void createDialog(){
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.emailforgetbox);

    }
    private void continue_func() {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.show();
        WebServiceRequests.getInstance().forgetPassword(email_EditView.getText().toString(), new Callback<ForgetPassword>() {
            @Override
            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {

                Preferences.getInstance(getActivity()).setEmailForgot(email_EditView.getText().toString());
          //      Preferences.getInstance(getActivity()).setAuthToken(response.body().get);
                //progressDialog.hide();
                showThankYouMessage();
            }

            @Override
            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                Toast.makeText(getActivity(), "Network Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }




    public void showThankYouMessage(){

        Handler hand = new Handler();

        Runnable run = new Runnable() {
            @Override public void run() {
                dialog.cancel();
               // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container_forgetPassword,new Verifying_Its_You()).addToBackStack(null).commit();
                startActivity(new Intent(getActivity(), WalkThroughActivity.class));
               // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLogin,new LoginFragment()).commit();
            } };
        hand.postDelayed(run, 2000);

        dialog.show();
    }

    private void findViews(View v) {
        continue_buttonvView= (Button) v.findViewById(R.id.Continue_ButtonView);
            email_EditView= (EditText) v.findViewById(R.id.Email_idID);
    }

}
