package com.bizbrolly.com.uplawapp;

import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;

import com.urbanairship.UAirship;
import com.urbanairship.push.notifications.NotificationFactory;

/**
 * Created by vishal on 31-10-2017.
 */

public class BaseApplication extends MultiDexApplication {

    private static final String App_ID = "5a172c0d-684f-4d83-8158-cc439f5d57ef";

    @Override
    public void onCreate() {
        super.onCreate();
        //initialize
       /* FreshchatConfig freshchatConfig = new FreshchatConfig("dbfda6a1-04ba-4fc5-90a6-699ac57cc23f", "cb645f16-1688-4e63-9f6c-8ead98113e75");
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);*/
        NotificationFactory factory = UAirship.shared().getPushManager().getNotificationFactory();
        UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
// Customize the factory
//        factory.setTitleId(Integer.parseInt("ygukbjnlmlk"));
        factory.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
/*        factory.setLargeIcon(R.drawable.ic_notification);
      */  factory.setSmallIconId(R.drawable.notification);


    }


}
