package com.bizbrolly.com.uplawapp.adapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.FeedActivity;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.LikeResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.WebServices.feedResponse;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VishalK on 10/12/2017.
 */

public class feed_recycler_adapter extends RecyclerView.Adapter<feed_recycler_adapter.Item> {
    MainActivity homeFragment_context;

    List<feedResponse.GetFeedResultBean.DataBean> data;


    public feed_recycler_adapter(MainActivity activity, List<feedResponse.GetFeedResultBean.DataBean> data) {
        homeFragment_context = activity;
        this.data = data;
    }

    @Override
    public Item onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_feed, parent, false);
        Item i = new Item(view);
        return i;
    }

    @Override
    public void onBindViewHolder(Item holder, int position) {
        holder.setData();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class Item extends RecyclerView.ViewHolder {
        private TextView titleFeedTextView;
        private TextView messageFeedTextView;
        private ImageView imageFeed;
        private TextView noOfLikesTextview;
        private TextView likeTextview;
        private LinearLayout likeLayput;
        private ImageView imageLike;


        public Item(View itemView) {
            super(itemView);
            imageLike = (ImageView) itemView.findViewById(R.id.image_like);

            likeLayput = (LinearLayout) itemView.findViewById(R.id.like_layput);
            titleFeedTextView = (TextView) itemView.findViewById(R.id.title_feed_textView);
            messageFeedTextView = (TextView) itemView.findViewById(R.id.message_feed_textView);
            imageFeed = (ImageView) itemView.findViewById(R.id.image_feed);
            noOfLikesTextview = (TextView) itemView.findViewById(R.id.no_of_likes_textview);
            likeTextview = (TextView) itemView.findViewById(R.id.like_textview);
        }


        public void setData() {
            titleFeedTextView.setText(data.get(getAdapterPosition()).getTitle());
            messageFeedTextView.setText(data.get(getAdapterPosition()).getDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("databundle", (Serializable) data.get(getAdapterPosition()));
                    Intent i = new Intent(homeFragment_context, FeedActivity.class);
                    i.putExtra("databundle", data.get(getAdapterPosition()));

                    homeFragment_context.startActivity(i);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("databundle", (Serializable) data.get(getAdapterPosition()));
//                    feed_fragment fm = new feed_fragment();
//                    fm.setArguments(bundle);
//                    homeFragment_context.getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    homeFragment_context.getSupportFragmentManager().beginTransaction().replace(R.id.container, fm).commit();
                }
            });
            Picasso.with(homeFragment_context).load(data.get(getAdapterPosition()).getImageURL()).into(imageFeed);

            noOfLikesTextview.setText(""+data.get(getAdapterPosition()).getNumberOfLike());

            boolean b = false;
            if (data.get(getAdapterPosition()).isIsLike() == true) {
                b = true;
                imageLike.setImageResource(R.drawable.ic_like_pink_24dp);
            } else {
                b = false;
                imageLike.setImageResource(R.drawable.ic_favorite_black_24dp);

            }


            likeLayput.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    boolean b=false;
//                    if (data.get(getAdapterPosition()).isIsLike() == false) {
//                        b=true;
//                        imageLike.setImageResource(R.drawable.ic_like_pink_24dp);
//                    }else {
//                        b=false;
//                        imageLike.setImageResource(R.drawable.ic_favorite_black_24dp);
//
//                    }

                    final ProgressDialog p = new ProgressDialog(homeFragment_context);
                    p.setMessage("Please wait..");
//                    p.setCancelable(false);
                    p.show();
                    if (data.get(getAdapterPosition()).isIsLike() == false) {

                        WebServiceRequests.getInstance().getlike(homeFragment_context, data.get(getAdapterPosition()).getFeedId(), 1, new Callback<LikeResponse>() {
                            @Override
                            public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {


                                if (response.isSuccessful() && response.body().getSaveLikeDislikeResult().isResult()) {
                                    p.hide();
                                    data.get(getAdapterPosition()).setIsLike(true);
                                    imageLike.setImageResource(R.drawable.ic_like_pink_24dp);
                                    data.get(getAdapterPosition()).setNumberOfLike(data.get(getAdapterPosition()).getNumberOfLike()+1);
                                    noOfLikesTextview.setText(""+(data.get(getAdapterPosition()).getNumberOfLike()));
                                }
                            }

                            @Override
                            public void onFailure(Call<LikeResponse> call, Throwable t) {
                                p.hide();
                                Toast.makeText(homeFragment_context, "Failed to connect network, please try again", Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {


                        WebServiceRequests.getInstance().getlike(homeFragment_context,
                                data.get(getAdapterPosition()).getFeedId(), 0, new Callback<LikeResponse>() {
                            @Override
                            public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {

                                if (response.isSuccessful() && response.body().getSaveLikeDislikeResult().isResult()) {
                                    p.hide();
                                    data.get(getAdapterPosition()).setIsLike(false);
                                    imageLike.setImageResource(R.drawable.ic_favorite_black_24dp);
                                    data.get(getAdapterPosition()).setNumberOfLike(data.get(getAdapterPosition()).getNumberOfLike()-1);
                                    noOfLikesTextview.setText(""+(data.get(getAdapterPosition()).getNumberOfLike()));
                                }else {
                                    p.hide();
                                    Toast.makeText(homeFragment_context, "someting went wrong!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<LikeResponse> call, Throwable t) {
                                p.hide();
                                Toast.makeText(homeFragment_context, "Failed to connect network, please try again", Toast.LENGTH_SHORT).show();

                            }
                        });

                    }
                }
            });
        }
    }
}
