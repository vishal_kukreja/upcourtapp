package com.bizbrolly.com.uplawapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.WebServices.ProfileUpdateResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.AttorneyProfileActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile_lasted extends AppCompatActivity {
    private CircleImageView profileImage;
    private TextView patientNameId;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    String userChoosenTask;
    private String imageBase64;
    EditText FirstNameView, LastNameView, EmailView, PhoneView;
    Button submit;
    TextView header_text;
    EditText Attorneys_Biography_ID,
            SpecializationID,
            LocationID,
            YearExperienceId,
            BusinessHours;

    RelativeLayout Attorneys_BiographyLayout,
            SpecializationLayout,
            LocationLayout,
            YearExperienceLayout,
            BusinessHoursLayout;
    private Toolbar toolbar;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_lasted);
//        setHasOptionsMenu(true);
        initView();
        header_text.setText("Profile");
//        setSupportActionBar(toolbar);
        enableEdit();
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (Preferences.getInstance(this).getRole().equals("Attorney")) {
            Attorneys_BiographyLayout.setVisibility(View.VISIBLE);
            SpecializationLayout.setVisibility(View.VISIBLE);
            LocationLayout.setVisibility(View.VISIBLE);
            YearExperienceLayout.setVisibility(View.VISIBLE);
            BusinessHoursLayout.setVisibility(View.VISIBLE);
        } else {
            Attorneys_BiographyLayout.setVisibility(View.GONE);
            SpecializationLayout.setVisibility(View.GONE);
            LocationLayout.setVisibility(View.GONE);
            YearExperienceLayout.setVisibility(View.GONE);
            BusinessHoursLayout.setVisibility(View.GONE);

        }
        setData();
        setUpActions();
    }

    private void setData() {
        if (Preferences.getInstance(this).getImage() != null && !Preferences.getInstance(this).getImage().equals("")) {
            Picasso.with(this).load(Preferences.getInstance(this).getImage()).resize(200, 200).centerCrop().into(profileImage);

        }
        String s = Preferences.getInstance(this).getName();
        if (s.trim().contains(" ")) {
            String[] arr = s.split(" ");
            FirstNameView.setText(arr[0]);
            LastNameView.setText(s.substring(s.indexOf(" ") + 1));
        } else {
            FirstNameView.setText(s);
        }
        PhoneView.setText(Preferences.getInstance(this).getPhone());
        if (Preferences.getInstance(this).getRole().equals("Attorney")) {
            Attorneys_Biography_ID.setText(Preferences.getInstance(this).getAttorneys_Biography_ID());
            SpecializationID.setText(Preferences.getInstance(this).getSpecializationID());
            LocationID.setText(Preferences.getInstance(this).getLocationID());
            YearExperienceId.setText(Preferences.getInstance(this).getYearExperienceId());
            BusinessHours.setText(Preferences.getInstance(this).getBusinessHoursDD());
        }
    }

    View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectImage();
        }
    };


    private boolean validate() {
        boolean isValid = true;
        if (FirstNameView.getText().toString().trim().length() == 0) {
            FirstNameView.setError("First Name should not be empty");
            isValid = false;
        } else if (LastNameView.getText().toString().trim().length() == 0) {
            LastNameView.setError("Last Name should not be empty");
            isValid = false;
        } else if (PhoneView.getText().toString().trim().length() == 0) {
            PhoneView.setError("Phone Number should not be empty");
            isValid = false;
        }
        if (PhoneView.getText().toString().trim().length() < 10) {
            PhoneView.setError("Phone Number should be of 10 digits");
            isValid = false;
        }
        return isValid;
    }

    private boolean validate1() {
        boolean isValid = true;
        if (FirstNameView.getText().toString().trim().length() == 0) {
            FirstNameView.setError("First Name should not be empty");
            isValid = false;
        } else if (LastNameView.getText().toString().trim().length() == 0) {
            LastNameView.setError("Last Name should not be empty");
            isValid = false;
        } else if (PhoneView.getText().toString().trim().length() == 0) {
            PhoneView.setError("Phone Number should not be empty");
            isValid = false;
        } else if (Attorneys_Biography_ID.getText().toString().trim().length() == 0) {
            Attorneys_Biography_ID.setError("Attorneys Biography should not be empty");
            isValid = false;
        } else if (SpecializationID.getText().toString().trim().length() == 0) {
            SpecializationID.setError("Specialization should not be empty");
            isValid = false;
        } else if (LocationID.getText().toString().trim().length() == 0) {
            LocationID.setError("Location should not be empty");
            isValid = false;
        } else if (YearExperienceId.getText().toString().trim().length() == 0) {
            YearExperienceId.setError("Year Experience should not be empty");
            isValid = false;
        } else if (BusinessHours.getText().toString().trim().length() == 0) {
            BusinessHours.setError("Business Hours should not be empty");
            isValid = false;
        }

        if (PhoneView.getText().toString().trim().length() < 10) {
            PhoneView.setError("Phone Number should be of 10 digits");
            isValid = false;
        }
        return isValid;
    }

    private void setUpActions() {
        patientNameId.setText(Preferences.getInstance(this).getName());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Preferences.getInstance(Profile_lasted.this).getRole().equals("Attorney")) {
                    if (validate1()) {
                        updateProfile("attorney");
                    }
                } else if (validate())
                    updateProfile("user");
            }
        });
    }

    private void updateProfile(String a) {
        final ProgressDialog p = new ProgressDialog(this);
        p.setTitle("Profile Update");
        p.setMessage("Updating...");
        p.show();
        final String image, name, phone,
                Attorneys_Biography_ID,
                SpecializationID,
                LocationID,
                YearExperienceId,
                BusinessHours,
                role;

        if (a.equals("user")) {
            image = Calendar.getInstance().getTime().getTime() + ".jpg";
            name = FirstNameView.getText().toString() + " " + LastNameView.getText().toString();
            phone = PhoneView.getText().toString();
            role = "User";
            Attorneys_Biography_ID = "";
            SpecializationID = "";
            LocationID = "";
            YearExperienceId = "";
            BusinessHours = "";

        } else {
            image = Calendar.getInstance().getTime().getTime() + ".jpg";
            name = FirstNameView.getText().toString() + " " + LastNameView.getText().toString();
            phone = PhoneView.getText().toString();
            Attorneys_Biography_ID = this.Attorneys_Biography_ID.getText().toString();
            SpecializationID = this.SpecializationID.getText().toString();
            LocationID = this.LocationID.getText().toString();
            YearExperienceId = this.YearExperienceId.getText().toString();
            BusinessHours = this.BusinessHours.getText().toString();
            role = "Attorney";
        }
        WebServiceRequests.getInstance().profileUpdate(
                this,
                Preferences.getInstance(this).getUserName(),
                name,
                phone,
                image,
                imageBase64,

                role,
                Attorneys_Biography_ID,
                SpecializationID,
                LocationID,
                YearExperienceId,
                BusinessHours,

                new Callback<ProfileUpdateResponse>() {
                    @Override
                    public void onResponse(Call<ProfileUpdateResponse> call, Response<ProfileUpdateResponse> response) {
                        p.hide();
                        if (response.isSuccessful() && response.body() != null && response.body().getUpdateUserAndAttorneyDetailsResult().isResult()) {
//                            Preferences.getInstance(getContext()).setImage(response.body().getUpdateProfileResult().getData().getUserImage());
//                            Preferences.getInstance(getContext()).setName(response.body().getUpdateProfileResult().getData().getName());
//                            Preferences.getInstance(getContext()).setPhone(response.body().getUpdateProfileResult().getData().getPhone());
                            //Preferences.getInstance(getContext()).setImage("http://bizbrollydev.cloudapp.net/UpCourt/Images/"+image.toString().trim());


                            Preferences.getInstance(Profile_lasted.this).setName(name);
                            Preferences.getInstance(Profile_lasted.this).setPhone(phone);
                            if (imageBase64 != null) {
                                Preferences.getInstance(Profile_lasted.this).setImage(response.body().getUpdateUserAndAttorneyDetailsResult().getData().getUserImage());
                            }
                            if (response.body().getUpdateUserAndAttorneyDetailsResult().getData().getRole().equals("User")) {

                            } else {

                                Preferences.getInstance(Profile_lasted.this).setAttorneys_Biography_ID(Attorneys_Biography_ID);
                                Preferences.getInstance(Profile_lasted.this).setSpecializationID(SpecializationID);
                                Preferences.getInstance(Profile_lasted.this).setLocationID(LocationID);
                                Preferences.getInstance(Profile_lasted.this).setYearExperienceId(YearExperienceId);
                                Preferences.getInstance(Profile_lasted.this).setBusinessHoursDD(BusinessHours);
                            }
                            setData();
                            refreshNameProfile();

                            Toast.makeText(Profile_lasted.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Profile_lasted.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileUpdateResponse> call, Throwable t) {
                        p.hide();
                        call.request();
                        Toast.makeText(Profile_lasted.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void refreshNameProfile() {
        patientNameId.setText(Preferences.getInstance(this).getName());

    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Gallery";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri uri = data.getData();
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        profileImage.setImageBitmap(thumbnail);
        imageBase64 = encode_Image(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri uri = data.getData();
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        profileImage.setImageBitmap(bm);
        imageBase64 = encode_Image(bm);
    }

    public static String encode_Image(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        bitmap = Bitmap.createScaledBitmap(
                bitmap,
                bitmap.getWidth() >= 200 ? 200 : bitmap.getWidth(),
                bitmap.getHeight() >= 200 ? 200 : bitmap.getHeight(),
                true
        );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private void initView() {
        header_text = (TextView) findViewById(R.id.header_text);

        profileImage = (CircleImageView) findViewById(R.id.profileImage);
        patientNameId = (TextView) findViewById(R.id.name_profileView);
        FirstNameView = (EditText) findViewById(R.id.nameprofile);
        LastNameView = (EditText) findViewById(R.id.LastNameUpdateID);
        PhoneView = (EditText) findViewById(R.id.Phone_idUpdate);
        submit = (Button) findViewById(R.id.Submit_ButtonView);

        Attorneys_Biography_ID = (EditText) findViewById(R.id.Attorneys_Biography_ID);
        final int MAX_WORDS = 150;
        Attorneys_Biography_ID.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Nothing
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String[] words = s.toString().split(" "); // Get all words
                if (words.length > MAX_WORDS) {
                    // Trim words to length MAX_WORDS
                    // Join words into a String
                    Attorneys_Biography_ID.setError("You cannot add more than 150 words.");
//                    Attorneys_Biography_ID.setText(wordString);
                }
            }
        });
        SpecializationID = (EditText) findViewById(R.id.SpecializationID);
        LocationID = (EditText) findViewById(R.id.LocationID);
        BusinessHours = (EditText) findViewById(R.id.BusinessHoursIDD);
        YearExperienceId = (EditText) findViewById(R.id.YearExperienceId);

        Attorneys_BiographyLayout = (RelativeLayout) findViewById(R.id.Attorneys_BiographyLayout);
        SpecializationLayout = (RelativeLayout) findViewById(R.id.SpecializationLayout);
        LocationLayout = (RelativeLayout) findViewById(R.id.LocationLayout);
        YearExperienceLayout = (RelativeLayout) findViewById(R.id.BusinessHoursLayout);
        BusinessHoursLayout = (RelativeLayout) findViewById(R.id.YearExperienceLayout);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Profile_lasted.this, AttorneyProfileActivity.class);
                i.putExtra("year", Preferences.getInstance(Profile_lasted.this).getYearExperienceId());
                i.putExtra("name", Preferences.getInstance(Profile_lasted.this).getName());
                i.putExtra("Specialization", Preferences.getInstance(Profile_lasted.this).getSpecializationID());
                i.putExtra("Location", Preferences.getInstance(Profile_lasted.this).getLocationID());
                i.putExtra("Business_Hours", Preferences.getInstance(Profile_lasted.this).getBusinessHoursDD());
                i.putExtra("Attorney_Bio", Preferences.getInstance(Profile_lasted.
                        this).getAttorneys_Biography_ID());
                i.putExtra("Email", Preferences.getInstance(Profile_lasted.this).getUserName());
                i.putExtra("Image", (String) Preferences.getInstance(Profile_lasted.this).getImage());
                i.putExtra("phone", Preferences.getInstance(Profile_lasted.this).getPhone());
//                i.putExtra("AttorneyId", (int)Preferences.getInstance(MainActivity.this).getAttorneyId());
                startActivity(i);
            }
        });
    }

//    MenuItem editItem;

/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_menu, menu);
        editItem = menu.getItem(0);
        disableEdit();
        super.onCreateOptionsMenu(menu, inflater);
    }
*/

    boolean isEditMode = false;

    private void enableEdit() {
        FirstNameView.setEnabled(true);
        LastNameView.setEnabled(true);
        PhoneView.setEnabled(true);
        Attorneys_Biography_ID.setEnabled(true);
        SpecializationID.setEnabled(true);
        LocationID.setEnabled(true);
        YearExperienceId.setEnabled(true);
        BusinessHours.setEnabled(true);
//        editItem.setIcon(R.drawable.ic_close_black_24dp);
        isEditMode = true;
        submit.setVisibility(View.VISIBLE);
        profileImage.setOnClickListener(imageClickListener);
    }

    private void disableEdit() {
        FirstNameView.setEnabled(false);
        LastNameView.setEnabled(false);
        PhoneView.setEnabled(false);
//        editItem.setIcon(R.drawable.ic_edit_black_24dp);
        Attorneys_Biography_ID.setEnabled(false);
        SpecializationID.setEnabled(false);
        LocationID.setEnabled(false);
        YearExperienceId.setEnabled(false);
        BusinessHours.setEnabled(false);
        isEditMode = false;
        submit.setVisibility(View.GONE);
        profileImage.setOnClickListener(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(this, AttorneyProfileActivity.class);
        i.putExtra("year", Preferences.getInstance(this).getYearExperienceId());
        i.putExtra("name", Preferences.getInstance(this).getName());
        i.putExtra("Specialization", Preferences.getInstance(this).getSpecializationID());
        i.putExtra("Location", Preferences.getInstance(this).getLocationID());
        i.putExtra("Business_Hours", Preferences.getInstance(this).getBusinessHoursDD());
        i.putExtra("Attorney_Bio", Preferences.getInstance(this).getAttorneys_Biography_ID());
        i.putExtra("Email", Preferences.getInstance(this).getUserName());
        i.putExtra("Image", (String) Preferences.getInstance(this).getImage());
        i.putExtra("phone", Preferences.getInstance(this).getPhone());
//                i.putExtra("AttorneyId", (int)Preferences.getInstance(MainActivity.this).getAttorneyId());
        startActivity(i);
        super.onBackPressed();
    }
}
