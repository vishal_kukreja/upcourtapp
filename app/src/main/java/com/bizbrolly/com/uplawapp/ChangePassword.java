package com.bizbrolly.com.uplawapp;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.WebServices.changePassword;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePassword extends Fragment {
EditText oldPasswordView,newPasswordView,re_password;
    Dialog dialog;
Button Save_ButtonViewBTN;
    public ChangePassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity)getActivity()).setHeadingText("Change Password");

        findViews(v);
        setlistener();
        createDialog();
        // Inflate the layout for this fragment
        return v;
    }

    private void setlistener() {
    Save_ButtonViewBTN.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(oldPasswordView.getText().toString().isEmpty()){
                oldPasswordView.setError("password should not be empty");
            }
            else if(newPasswordView.getText().toString().isEmpty()){
                newPasswordView.setError("password should not be empty");
            }
            else {
                submxitbutton();
            }
        }
    });
    }

    private void submxitbutton() {
if(!newPasswordView.getText().toString().equals(re_password.getText().toString())){
    Toast.makeText(getActivity(), "Please enter  correct old password first", Toast.LENGTH_SHORT).show();
}else
if(!re_password.getText().toString().equals(newPasswordView.getText().toString())){
    Toast.makeText(getActivity(), "Password mismatch", Toast.LENGTH_SHORT).show();
}else {
    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
    progressDialog.setTitle("Login");
    progressDialog.setMessage("Please wait...");
    progressDialog.show();
        WebServiceRequests.getInstance().changePasswordd(
                getActivity(),
                Preferences.getInstance(getActivity()).getUserName(),
                oldPasswordView.getText().toString(),
                newPasswordView.getText().toString(),
                new Callback<changePassword>() {
                    @Override
                    public void onResponse(Call<changePassword> call, Response<changePassword> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().getChangePasswordResult().isResult()) {
                            progressDialog.hide();
                            if (response.body().getChangePasswordResult().isData()) {
                                dialog.show();

                            } else {
                                Toast.makeText(getActivity(), "Enter correct old password", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            progressDialog.hide();
                            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<changePassword> call, Throwable t) {
                        progressDialog.hide();
                        Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();


                    }
                }


        );}
    }
    private void createDialog(){
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.changepasswordbox);

    }

    private void findViews(View v) {
        Save_ButtonViewBTN= (Button) v.findViewById(R.id.Save_ButtonView);
        oldPasswordView= (EditText) v.findViewById(R.id.oldPasswordID);
        newPasswordView= (EditText) v.findViewById(R.id.newPasswordID);
        re_password= (EditText) v.findViewById(R.id.re_newPasswordID);
    }

}
