package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by VishalK on 10/13/2017.
 */

public class ContactResponse {

    /**
     * ContactToAttorneyResult : {"Data":104,"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private ContactToAttorneyResultBean ContactToAttorneyResult;

    public ContactToAttorneyResultBean getContactToAttorneyResult() {
        return ContactToAttorneyResult;
    }

    public void setContactToAttorneyResult(ContactToAttorneyResultBean ContactToAttorneyResult) {
        this.ContactToAttorneyResult = ContactToAttorneyResult;
    }

    public static class ContactToAttorneyResultBean {
        /**
         * Data : 104
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private int Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public int getData() {
            return Data;
        }

        public void setData(int Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
