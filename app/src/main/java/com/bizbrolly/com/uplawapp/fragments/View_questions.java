package com.bizbrolly.com.uplawapp.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.View_questions_attorneyResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.adapter.view_questionAdapter;
import com.bizbrolly.com.uplawapp.adapter.view_questionAdapterUser;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class View_questions extends Fragment {

    view_questionAdapterUser view_questionAdapterUser;
    ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean> list;
    private TextView NoSEARTEXtVIEW;
    private RecyclerView viewQuestionsList;

    public View_questions() {


    }
@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_questions, container, false);
        initView(v);
        ((MainActivity) getActivity()).setHeadingText("Questions");

        ApiHit();


        return v;
    }

    private void ApiHit() {
        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setMessage("Please wait...");
        p.setCancelable(false);
        p.show();
        WebServiceRequests.getInstance().viewQuestionsAttorney(getActivity(),
                Preferences.getInstance(getActivity()).getUserName(),Preferences.getInstance(getActivity()).getAuthToken(),
                0, 0, new Callback<View_questions_attorneyResponse>() {
                    @Override
                    public void onResponse(Call<View_questions_attorneyResponse> call, Response<View_questions_attorneyResponse> response) {
                        if (response.isSuccessful() && response.body().getGetQuestionResult().isResult()) {
p.hide();
                            if(Preferences.getInstance(getActivity()).getRole().equals("Attorney")) {

                                list = (ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean>) response.body().getGetQuestionResult().getData();
                                view_questionAdapter adapter = new view_questionAdapter((MainActivity) getActivity(), list);
                                viewQuestionsList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                viewQuestionsList.setAdapter(adapter);
                            }else {
                                list = (ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean>) response.body().getGetQuestionResult().getData();

                                view_questionAdapterUser = new view_questionAdapterUser((MainActivity) getActivity(), list);
                                viewQuestionsList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                viewQuestionsList.setAdapter(view_questionAdapterUser);
                            }

                        } else {
                            p.hide();
                            Toast.makeText(getActivity(), "" + response.body().getGetQuestionResult().getErrorDetail().getErrorDetails().toString().trim(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<View_questions_attorneyResponse> call, Throwable t) {
                        p.hide();

                        Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

                    }
                });
//        Intent i = new Intent((MainActivity) getActivity(), advertisementActivity.class);
//        startActivity(i);
    }


    private void initView(View v) {
        NoSEARTEXtVIEW = (TextView) v.findViewById(R.id.NoSEAR_TEXtVIEW);
        viewQuestionsList = (RecyclerView) v.findViewById(R.id.view_questions_list);
    }

    @Override
    public void onStart() {
        super.onStart();
        ApiHit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_CANCELED) {

                view_questionAdapterUser.onActivityResult(requestCode, resultCode, data);

                //Write your code if there's no result
            }
        }
    }
}
