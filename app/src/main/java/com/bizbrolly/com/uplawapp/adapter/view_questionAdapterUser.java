package com.bizbrolly.com.uplawapp.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.View_questions_attorneyResponse;
import com.bizbrolly.com.uplawapp.activities.AnswerQ_Activity;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.advertisementActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class view_questionAdapterUser extends RecyclerView.Adapter<view_questionAdapterUser.ViewHolder> {
    ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean> list_new, list1, list_old, list2;
    MainActivity view_questions_context;
    String profile_image_temp;


    public view_questionAdapterUser(MainActivity context, ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean> list) {
        list_new = list;
        /*this.list_old = list;

        list1 = new ArrayList<>();
        list2=new ArrayList<>();
        list_new = new ArrayList<>();
//        list_old = new ArrayList<>();

        for (int i = 0; i < list_old.size(); i++) {
            if (list_old.get(i).isIsAnswer()) {
                this.list1.add(list_old.get(i));// answered
            } else {
                list2.add(list_old.get(i));//Not Answered
            }
        }

        for (int i = 0; i < list1.size(); i++) {
            list_new.add(list1.get(i));
        }
        for (int i = 0; i < list2.size(); i++) {
            list_new.add(list2.get(i));
        }*/
        view_questions_context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_view_questions, parent, false);
//        view_questionAdapter.ViewHolder v = new view_questionAdapter.ViewHolder(view);
        ViewHolder v = new ViewHolder(view);
        return v;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData();

        holder.tv_Detail2.setText((CharSequence) list_new.get(position).getQuestion());
        profile_image_temp = list_new.get(position).getUserImage();
        holder.textViewTime.setText((CharSequence) list_new.get(position).getQuestionPostedDate());
//       holder.textViewTimenowans.setText(list_new.get(position).getAnsweredAgo());
        if (list_new.get(position).isIsAnswer()) {
//            holder.textViewTimenowans.setVisibility(View.VISIBLE);
            holder.textViewTimeAnswer.setVisibility(View.VISIBLE);
            holder.textViewTimeposted.setVisibility(View.GONE);
            holder.tv_username.setText((CharSequence) list_new.get(position).getAttorneyDetail().getName());

            if (!list_new.get(position).getAttorneyDetail().getUserImage().isEmpty()) {
                Picasso.with(view_questions_context).load(list_new.get(position).getAttorneyDetail().getUserImage())
                        .into(holder.profile_image);
            } else {
                Picasso.with(view_questions_context).load(R.drawable.placeholder)
                        .into(holder.profile_image);
            }


        } else {
            holder.tv_username.setText((CharSequence) list_new.get(position).getFirstName());
            holder.tv_username.setText(Preferences.getInstance(view_questions_context).getName());
            if(!Preferences.getInstance(view_questions_context).getImage().isEmpty())
            {Picasso.with(view_questions_context).load(Preferences.getInstance(view_questions_context).getImage())
                    .into(holder.profile_image);}
//            holder.textViewTimenowans.setVisibility(View.GONE);
            holder.textViewTimeposted.setVisibility(View.VISIBLE);
            holder.textViewTimeAnswer.setVisibility(View.GONE);

            if (!list_new.get(position).getUserImage().isEmpty()) {
                Picasso.with(view_questions_context).load(list_new.get(position).getUserImage())
                        .into(holder.profile_image);
            } else {
                Picasso.with(view_questions_context).load(R.drawable.placeholder)
                        .into(holder.profile_image);
            }

        }



        if(position%2==0){
            holder.tv_Detail2.setTextColor(Color.BLACK);
        }
// ((view_questionAdapter.ViewHolder) holder).tv_Head.setText((CharSequence) list.get(position).getName());
//        ((view_questionAdapter.ViewHolder) holder).tv_Detail1.setText((CharSequence) list.get(position).getAttorneysBio());
//        ((view_questionAdapter.ViewHolder) holder).tv_Detail2.setText((CharSequence) list.get(position).getLocation());
//        Picasso.with(view_questions).load((String) list.get(position).getUserImage()).into(((MarketPlaceAdapter.ViewHolder) holder).profile_image);
//        //((ViewHolder)holder).profile_image.setImageResource((Integer) list.get(position).getUserImage());
    }


    @Override
    public int getItemCount() {
        return list_new.size();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int position = data.getExtras().getInt("position");

        int id = list_new.get(position).getQuestionId();
        String question = list_new.get(position).getQuestion();
        Intent i = new Intent(view_questions_context, AnswerQ_Activity.class);
        i.putExtra("questionid", id);
        i.putExtra("question", question);
        i.putExtra("profile_image", list_new.get(position).getUserImage());
        i.putExtra("username", (CharSequence) list_new.get(position).getFirstName());
        i.putExtra("time", (CharSequence) list_new.get(position).getAnsweredAgo());
        i.putExtra("ans", (CharSequence) list_new.get(position).getAnswer());
        i.putExtra("timeAttorney", (CharSequence) list_new.get(position).getQuestionPostedDate());
        i.putExtra("NotAnswered",list_new.get(position).isIsAnswer());

        i.putExtra("AttorneysBio", list_new.get(position).getAttorneyDetail().getAttorneysBio());
        i.putExtra("BusinessHours", list_new.get(position).getAttorneyDetail().getBusinessHours());
        i.putExtra("EmailId", list_new.get(position).getAttorneyDetail().getEmailId());
        i.putExtra("Name", list_new.get(position).getAttorneyDetail().getName());
        i.putExtra("Phone", list_new.get(position).getAttorneyDetail().getPhone());
        i.putExtra("Specialization", list_new.get(position).getAttorneyDetail().getSpecialization());
        i.putExtra("UserImage",list_new.get(position).getAttorneyDetail().getUserImage());
        i.putExtra("YearExperience",list_new.get(position).getAttorneyDetail().getYearExperience());
        i.putExtra("Location",list_new.get(position).getAttorneyDetail().getLocation());


        (view_questions_context).startActivity(i);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_username;
        TextView tv_Detail2;
        ImageView profile_image;
        TextView textViewTime;
        TextView textViewTimeAnswer;
        private TextView textViewTimenowans;
        private TextView textViewTimeposted;
        public ViewHolder(final View itemView) {
            super(itemView);
            tv_username = (TextView) itemView.findViewById(R.id.textView_ItemRecyclerView);
            tv_Detail2 = (TextView) itemView.findViewById(R.id.textView_2Detail_ItemRecyclerView);
            profile_image = (ImageView) itemView.findViewById(R.id.profile_image_id);
            textViewTime = (TextView) itemView.findViewById(R.id.textView_time);
            textViewTimeAnswer = (TextView) itemView.findViewById(R.id.textView_timeAnswer);
//            textViewTimenowans = (TextView) itemView.findViewById(R.id.textView_timenowans);

            textViewTimeposted = (TextView) itemView.findViewById(R.id.textView_timeposted);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        Intent i = new Intent(itemView.getContext(), advertisementActivity.class);
                        i.putExtra("ansAdvert", list_new.get(getAdapterPosition()).getAdvertismentURL() == null ? "" : list_new.get(getAdapterPosition()).getAdvertismentURL().toString());
                        i.putExtra("position", getAdapterPosition());

                        view_questions_context.startActivityForResult(i, 5);


                }
            });
        }

        void setData() {
            if (getAdapterPosition() % 2 == 0){
//                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.drawer_item_bg_color));
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.mycolr1));

            } else {
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.mycolr2));

                //                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.drawer_bg_colot));
            }
        }
    }


}

