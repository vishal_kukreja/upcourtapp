package com.bizbrolly.com.uplawapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.AskResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AskAttorneyActivity extends AppCompatActivity {
    Button submit;
    EditText questionEditText;
    Dialog dialog;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_attorney);
        findViews();
        setListner();
        //createDialog();
    }

    private void setListner() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (questionEditText.getText().toString().trim().length() > 0) {
                    submitResponse();
                } else {
                    questionEditText.setError("Field should not be empty");
                }
            }
        });
    }

    private void submitResponse() {
        final ProgressDialog p = new ProgressDialog(AskAttorneyActivity.this);
        p.setMessage("Response submitting...");
        p.show();

        String question_String;
        question_String = questionEditText.getText().toString();
        WebServiceRequests.getInstance().submitIt(Preferences.getInstance(AskAttorneyActivity.this).getUserName(),
                Preferences.getInstance(AskAttorneyActivity.this).getAuthToken(),
                question_String,
                new Callback<AskResponse>() {
                    @Override
                    public void onResponse(Call<AskResponse> call, Response<AskResponse> response) {

                        if (response.isSuccessful() && response.body().getAskQuestionResult().isResult()) {
                            p.hide();

                            createDialog();
                            showThankYouMessage();
                            //   Preferences.getInstance(AskAttorneyActivity.this).setData(response.body().getAskQuestionResult().getData());
                        } else {
                            Toast.makeText(AskAttorneyActivity.this, response.body().getAskQuestionResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AskResponse> call, Throwable t) {
                        Toast.makeText(AskAttorneyActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }


    private void findViews() {
        submit = (Button) findViewById(R.id.submit);
        questionEditText = (EditText) findViewById(R.id.questionID);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void createDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_thank_you, null);
//       TextView textView_to_alter= (TextView) view.findViewById(R.id.textview_to_alter_attorney);
//       textView_to_alter.setText("ndbsvdvsvba");
        Button okButton = (Button) view.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        //dialog.setCancelable(false);
        dialog.setContentView(view);

    }

    public void showThankYouMessage() {
        questionEditText.setText("");
        dialog.show();
    }
}
