package com.bizbrolly.com.uplawapp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.View_questions_attorneyResponse;
import com.bizbrolly.com.uplawapp.activities.AnswerQ_Activity;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class view_questionAdapter extends RecyclerView.Adapter<view_questionAdapter.ViewHolder> {
    ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean> list, list_new;
    MainActivity view_questions_context;
    String profile_image_temp;

    private TextView textViewTimeAnswer;

    public view_questionAdapter(MainActivity context, ArrayList<View_questions_attorneyResponse.GetQuestionResultBean.DataBean> list) {
//        if (!list.get(getA).isIsAnswer()) {
        this.list = list;
        list_new = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).isIsAnswer()) {
                list_new.add(list.get(i));
            }
        }
        view_questions_context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_view_questions, parent, false);
//        view_questionAdapter.ViewHolder v = new view_questionAdapter.ViewHolder(view);
        ViewHolder v = new ViewHolder(view);
        return v;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData();

        holder.tv_username.setText((CharSequence) list_new.get(position).getFirstName());
        holder.tv_Detail2.setText((CharSequence) list_new.get(position).getQuestion());
        profile_image_temp = list_new.get(position).getUserImage();
       holder.textViewTime.setText((CharSequence) list_new.get(position).getQuestionPostedDate());
        if (!list_new.get(position).getUserImage().isEmpty()) {
            Picasso.with(view_questions_context).load(list_new.get(position).getUserImage())
                    .into(holder.profile_image);
        } else {
            Picasso.with(view_questions_context).load(R.drawable.placeholder)
                    .into(holder.profile_image);
        }
        textViewTimeAnswer.setVisibility(View.GONE);

// ((view_questionAdapter.ViewHolder) holder).tv_Head.setText((CharSequence) list.get(position).getName());
//        ((view_questionAdapter.ViewHolder) holder).tv_Detail1.setText((CharSequence) list.get(position).getAttorneysBio());
//        ((view_questionAdapter.ViewHolder) holder).tv_Detail2.setText((CharSequence) list.get(position).getLocation());
//        Picasso.with(view_questions).load((String) list.get(position).getUserImage()).into(((MarketPlaceAdapter.ViewHolder) holder).profile_image);
//        //((ViewHolder)holder).profile_image.setImageResource((Integer) list.get(position).getUserImage());
    }


    @Override
    public int getItemCount() {
        return list_new.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_username;
        TextView tv_Detail2;
        ImageView profile_image;
        private TextView textViewTime;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_username = (TextView) itemView.findViewById(R.id.textView_ItemRecyclerView);
            tv_Detail2 = (TextView) itemView.findViewById(R.id.textView_2Detail_ItemRecyclerView);
            profile_image = (ImageView) itemView.findViewById(R.id.profile_image_id);
            textViewTime = (TextView) itemView.findViewById(R.id.textView_time);
            textViewTimeAnswer = (TextView) itemView.findViewById(R.id.textView_timeAnswer);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int id = list_new.get(getAdapterPosition()).getQuestionId();
                    String question = list_new.get(getAdapterPosition()).getQuestion();
                    long userId=list_new.get(getAdapterPosition()).getUserId();
                    Intent i = new Intent(view_questions_context, AnswerQ_Activity.class);
                    i.putExtra("userId",userId);
                    i.putExtra("questionid", id);
                    i.putExtra("question", question);
                    i.putExtra("timeAttorney", (CharSequence) list_new.get(getAdapterPosition()).getQuestionPostedDate());
                    i.putExtra("profile_image", list_new.get(getAdapterPosition()).getUserImage());
                    i.putExtra("username", (CharSequence) list_new.get(getAdapterPosition()).getFirstName());
                    (view_questions_context).startActivity(i);
                }
            });
        }

        void setData() {
            if (getAdapterPosition() % 2 == 0) {
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.mycolr1));
            } else {
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.mycolr2));
            }
        }
    }
}
