package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by VishalK on 10/12/2017.
 */

public class LikeResponse {
    /**
     * SaveLikeDislikeResult : {"Data":101,"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private SaveLikeDislikeResultBean SaveLikeDislikeResult;

    public SaveLikeDislikeResultBean getSaveLikeDislikeResult() {
        return SaveLikeDislikeResult;
    }

    public void setSaveLikeDislikeResult(SaveLikeDislikeResultBean SaveLikeDislikeResult) {
        this.SaveLikeDislikeResult = SaveLikeDislikeResult;
    }

    public static class SaveLikeDislikeResultBean {
        /**
         * Data : 101
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private int Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public int getData() {
            return Data;
        }

        public void setData(int Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
