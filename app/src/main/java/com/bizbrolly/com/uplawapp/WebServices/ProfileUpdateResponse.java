package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by VishalK on 9/25/2017.
 */

public class ProfileUpdateResponse {


    /**
     * UpdateUserAndAttorneyDetailsResult : {"Data":{"__type":"clsUserUpdatedDetails:#UpCourt.Model","AttorneysBio":null,"BusinessHours":null,"EmailId":"vishal@gmail.com","IsAgree":null,"Location":null,"Name":null,"Phone":"4865555666","Role":"User","Specialization":null,"UserId":272,"UserImage":"http://bizbrollydev.cloudapp.net/UpCourt/Images/img2.jpg","YearExperience":null},"ErrorDetail":{"ErrorDetails":"Your details has been updated successfully.","ErrorMessage":"Success"},"Result":true}
     */

    private UpdateUserAndAttorneyDetailsResultBean UpdateUserAndAttorneyDetailsResult;

    public UpdateUserAndAttorneyDetailsResultBean getUpdateUserAndAttorneyDetailsResult() {
        return UpdateUserAndAttorneyDetailsResult;
    }

    public void setUpdateUserAndAttorneyDetailsResult(UpdateUserAndAttorneyDetailsResultBean UpdateUserAndAttorneyDetailsResult) {
        this.UpdateUserAndAttorneyDetailsResult = UpdateUserAndAttorneyDetailsResult;
    }

    public static class UpdateUserAndAttorneyDetailsResultBean {
        /**
         * Data : {"__type":"clsUserUpdatedDetails:#UpCourt.Model","AttorneysBio":null,"BusinessHours":null,"EmailId":"vishal@gmail.com","IsAgree":null,"Location":null,"Name":null,"Phone":"4865555666","Role":"User","Specialization":null,"UserId":272,"UserImage":"http://bizbrollydev.cloudapp.net/UpCourt/Images/img2.jpg","YearExperience":null}
         * ErrorDetail : {"ErrorDetails":"Your details has been updated successfully.","ErrorMessage":"Success"}
         * Result : true
         */

        private DataBean Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public DataBean getData() {
            return Data;
        }

        public void setData(DataBean Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class DataBean {
            /**
             * __type : clsUserUpdatedDetails:#UpCourt.Model
             * AttorneysBio : null
             * BusinessHours : null
             * EmailId : vishal@gmail.com
             * IsAgree : null
             * Location : null
             * Name : null
             * Phone : 4865555666
             * Role : User
             * Specialization : null
             * UserId : 272
             * UserImage : http://bizbrollydev.cloudapp.net/UpCourt/Images/img2.jpg
             * YearExperience : null
             */

            private String __type;
            private Object AttorneysBio;
            private Object BusinessHours;
            private String EmailId;
            private Object IsAgree;
            private Object Location;
            private Object Name;
            private String Phone;
            private String Role;
            private Object Specialization;
            private int UserId;
            private String UserImage;
            private Object YearExperience;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public Object getAttorneysBio() {
                return AttorneysBio;
            }

            public void setAttorneysBio(Object AttorneysBio) {
                this.AttorneysBio = AttorneysBio;
            }

            public Object getBusinessHours() {
                return BusinessHours;
            }

            public void setBusinessHours(Object BusinessHours) {
                this.BusinessHours = BusinessHours;
            }

            public String getEmailId() {
                return EmailId;
            }

            public void setEmailId(String EmailId) {
                this.EmailId = EmailId;
            }

            public Object getIsAgree() {
                return IsAgree;
            }

            public void setIsAgree(Object IsAgree) {
                this.IsAgree = IsAgree;
            }

            public Object getLocation() {
                return Location;
            }

            public void setLocation(Object Location) {
                this.Location = Location;
            }

            public Object getName() {
                return Name;
            }

            public void setName(Object Name) {
                this.Name = Name;
            }

            public String getPhone() {
                return Phone;
            }

            public void setPhone(String Phone) {
                this.Phone = Phone;
            }

            public String getRole() {
                return Role;
            }

            public void setRole(String Role) {
                this.Role = Role;
            }

            public Object getSpecialization() {
                return Specialization;
            }

            public void setSpecialization(Object Specialization) {
                this.Specialization = Specialization;
            }

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int UserId) {
                this.UserId = UserId;
            }

            public String getUserImage() {
                return UserImage;
            }

            public void setUserImage(String UserImage) {
                this.UserImage = UserImage;
            }

            public Object getYearExperience() {
                return YearExperience;
            }

            public void setYearExperience(Object YearExperience) {
                this.YearExperience = YearExperience;
            }
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails : Your details has been updated successfully.
             * ErrorMessage : Success
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
