package com.bizbrolly.com.uplawapp.WebServices;


import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bizbrolly on 2/13/17.
 */

public interface ApiInterface {

    @POST(Constants.PARTIALS.SIGN_IN)
    Call<SignInResponse> signin(
            @Body HashMap<String, Object> body
    );



    @POST(Constants.PARTIALS.SIGN_UP)
    Call<SignUpResponse> signUp(
            @Body HashMap<String, Object> body
    );
@POST(Constants.PARTIALS.GET_QUESTIONS_ATTORNEY)
Call<View_questions_attorneyResponse> viewQuestionAttorney(
        @Body HashMap<String, Object> body
);

    @POST(Constants.PARTIALS.SIGN_OUT)
    Call<SignOut> signout(
            @Body HashMap<String, Object> body
    );

    @POST(Constants.PARTIALS.GET_ATTORNEY_LIST)
    Call<SearchAttorneyList> searchAttorney(
            @Body HashMap<String,Object> body
    );

    @POST(Constants.PARTIALS.AskAttorney)
    Call<AskResponse> askResponse(
            @Body HashMap<String,Object> body
    );

    @POST(Constants.PARTIALS.FORGET_PASSWORD)
    Call<ForgetPassword> forgetResponse(
            @Body HashMap<String,Object> body
    );
    @POST(Constants.PARTIALS.Answer)
    Call<AnswerQResponse> AnswerResponse(
            @Body HashMap<String,Object> body
    );

    @POST(Constants.PARTIALS.CHANGE_PASSWORD)
    Call<changePassword> changeResponse(
            @Body HashMap<String,Object> body
    );
    @POST(Constants.PARTIALS.UPDATE_PROFILE)
    Call<ProfileUpdateResponse> profileResponse(
            @Body HashMap<String,Object> body
    );

    @POST(Constants.PARTIALS.FEED)
    Call<feedResponse> getfeedRespnse(
           @Body HashMap<String,Object> body
    );
    @POST(Constants.PARTIALS.LIKE)
    Call<LikeResponse> getLikeRespnse(
            @Body HashMap<String,Object> body
    );
    @POST(Constants.PARTIALS.CONTACT_BTN)
    Call<ContactResponse> getContactRespnse(
            @Body HashMap<String,Object> body
    );


    @POST(Constants.PARTIALS.Adds)
    Call<AdvertisementResponse> getAddsRespnse(
            @Body HashMap<String,Object> body
    );

//    @POST(Constants.PARTIALS.Adds)
//    Call<>




//    @POST(Constants.PARTIALS.GET_USER_DETAILS)
//    Call<GetUserDetailsResponse> getUserDetails(
//            @Body HashMap<String, Object> body
//    );
//
//    @POST(Constants.PARTIALS.GET_MASTER_DATA)
//    Call<GetMasterDataResponse> getMasterData(
//            @Body HashMap<String, Object> body
//    );
//
//    @POST(Constants.PARTIALS.GET_DOCTOR_BY_SPECIALIZATION_DATA)
//    Call<GetDoctorBySpecializationResponse> getDoctorBySpecialization(
//            @Body HashMap<String, Object> body
//    );
//
//    @POST(Constants.PARTIALS.SAVE_APPOINTMENT)
//    Call<SaveAppointMentResponse> saveAppointMent(
//            @Body HashMap<String, Object> body
//    );
//
//    @POST(Constants.PARTIALS.GET_APPOINTMENT)
//    Call<GetAppointMentsResponse> getUserAppointments(
//            @Body HashMap<String, Object> body
//    );
//
//    @POST(Constants.PARTIALS.GET_TEXT)
//    Call<GetTextResponse> getDisclamerText(
//            @Body HashMap<String, Object> body
//    );
//    @POST(Constants.PARTIALS.FORGET_PASSWORD)
//    Call<ForgotPasswordResponse> forgotPassword(
//            @Body HashMap<String, Object> body
//    );


}
