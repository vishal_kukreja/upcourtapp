package com.bizbrolly.com.uplawapp.adapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.SearchAttorneyList;
import com.bizbrolly.com.uplawapp.activities.AttorneyProfileActivity;
import com.bizbrolly.com.uplawapp.activities.MarketPlaceAttorneyActivity;
import com.bizbrolly.com.uplawapp.advertisementActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bizbrolly on 3/16/17.
 */

public class MarketPlaceAdapter extends RecyclerView.Adapter<MarketPlaceAdapter.ViewHolder> {
    int count;

    ArrayList<SearchAttorneyList.GetAttorneyListResultBean.DataBean> list,list_new;
    MarketPlaceAttorneyActivity c;
    ProgressDialog p;

    public MarketPlaceAdapter() {

    }

    public MarketPlaceAdapter(MarketPlaceAttorneyActivity marketPlaceAttorneyActivity, List<SearchAttorneyList.GetAttorneyListResultBean.DataBean> list) {
        this.list = (ArrayList<SearchAttorneyList.GetAttorneyListResultBean.DataBean>) list;
        c = marketPlaceAttorneyActivity;

        list_new=new ArrayList<>() ;
        for (int i=0;i<list.size();i++){
            if(list.get(i).isIsApproved()){
                list_new.add(list.get(i)) ;
            }
        }
    }

    @Override
    public MarketPlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_market_place, parent, false);
        ViewHolder v = new ViewHolder(view);

//        RecyclerView.ViewHolder viewHolder=new MarketPlaceAdapter.ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(final MarketPlaceAdapter.ViewHolder holder, int position) {
        holder.setData();

            ((ViewHolder) holder).tv_Head.setText((CharSequence) list_new.get(position).getName());
            ((ViewHolder) holder).tv_Detail1.setText((CharSequence) list_new.get(position).getAttorneysBio());
            ((ViewHolder) holder).tv_Detail2.setText((CharSequence) list_new.get(position).getLocation());
            Picasso.with(c).load((String) list_new.get(position).getUserImage()).into(((ViewHolder) holder).profile_image);
            //((ViewHolder)holder).profile_image.setImageResource((Integer) list.get(position).getUserImage());
        }

    @Override
    public int getItemCount() {
        return list_new.size();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        {
                int position = data.getExtras().getInt("position");
                Intent i = new Intent(c, AttorneyProfileActivity.class);
                i.putExtra("year", list_new.get(position).getYearExperience());
                i.putExtra("name", list_new.get(position).getName());
                i.putExtra("Specialization", list_new.get(position).getSpecialization());
                i.putExtra("Location", list_new.get(position).getLocation());
                i.putExtra("Business_Hours", list_new.get(position).getBusinessHours());
                i.putExtra("Attorney_Bio", list_new.get(position).getAttorneysBio());
                i.putExtra("Email", list_new.get(position).getEmailId());
                i.putExtra("Image", (String) list_new.get(position).getUserImage());
                i.putExtra("phone", list_new.get(position).getPhone());
                i.putExtra("AttorneyId", (int)list_new.get(position).getAttorneyId());

                c.startActivity(i);

        }



    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_Head;
        TextView tv_Detail1;
        TextView tv_Detail2;
        ImageView profile_image;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_Head = (TextView) itemView.findViewById(R.id.textView_ItemRecyclerView);
            tv_Detail1 = (TextView) itemView.findViewById(R.id.textView_Detail1_ItemRecyclerView);
            tv_Detail2 = (TextView) itemView.findViewById(R.id.textView_2Detail_ItemRecyclerView);
            profile_image = (ImageView) itemView.findViewById(R.id.profile_image_id);
        }

        void setData() {
            if (getAdapterPosition() % 2 == 0) {
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.drawer_item_bg_color));
            } else {
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.drawer_bg_colot));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    WebServiceRequests.getInstance().getaddss(c, new Callback<AdvertisementResponse>() {
//                        @Override
//                        public void onResponse(Call<AdvertisementResponse> call, Response<AdvertisementResponse> response) {
//                            if (response.isSuccessful() && response.body().getGetAdvertismentResult().isResult()) {
//                                response.code();
//                                p.dismiss();
//                                Intent i = new Intent(itemView.getContext(), advertisementActivity.class);
////                    i.putExtra("advertisement",list.get(getAdapterPosition()).getLstAdvertisment());
//
//                                i.putExtra("advertisement", "response.body().getGetAdvertismentResult().getData().getImagePath()");
//                                i.putExtra("position", getAdapterPosition());
//                                c.startActivityForResult(i, 1);
//
//
//                            } else {
//                                p.dismiss();
//                                Toast.makeText(c, "Something went wrong", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<AdvertisementResponse> call, Throwable t) {
//                            p.dismiss();
//                            Toast.makeText(c, "Failed to connect,please connect again", Toast.LENGTH_SHORT).show();
//                        }
//                    });
                    Intent i = new Intent(itemView.getContext(), advertisementActivity.class);
                    i.putExtra("adds", list_new.get(getAdapterPosition()).getAdvertismentURL()==null?"":list_new.get(getAdapterPosition()).getAdvertismentURL().toString());
                     i.putExtra("position", getAdapterPosition());

                    c.startActivityForResult(i, 1);

//                    Intent i = new Intent(itemView.getContext(), AttorneyProfileActivity.class);
//                    i.putExtra("year", list.get(getAdapterPosition()).getYearExperience());
//                    i.putExtra("name", list.get(getAdapterPosition()).getName());
//                    i.putExtra("Specialization", list.get(getAdapterPosition()).getSpecialization());
//                    i.putExtra("Location", list.get(getAdapterPosition()).getLocation());
//                    i.putExtra("Business_Hours", list.get(getAdapterPosition()).getBusinessHours());
//                    i.putExtra("Attorney_Bio", list.get(getAdapterPosition()).getAttorneysBio());
//                    i.putExtra("Email", list.get(getAdapterPosition()).getEmailId());
//                    i.putExtra("Image", (String) list.get(getAdapterPosition()).getUserImage());
//                    i.putExtra("phone",list.get(getAdapterPosition()).getPhone());
//
////                     i.putExtra("myList" , new Gson().toJson(list));
//                    itemView.getContext().startActivity(i);
//                    //fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new AttorneyProfileActivity()).commit();
                }
            });
        }
    }

}
