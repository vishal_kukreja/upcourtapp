package com.bizbrolly.com.uplawapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bizbrolly.com.uplawapp.activities.MainActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class privacy_policy extends Fragment {


    public privacy_policy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setHeadingText("Privacy policy");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }

}
