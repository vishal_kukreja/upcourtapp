package com.bizbrolly.com.uplawapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bizbrolly.com.uplawapp.Profile_Update;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.activities.MarketPlaceAttorneyActivity;
import com.bizbrolly.com.uplawapp.advertisementActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;

/**
 * Created by hp on 9/12/2017.
 */

public class HomeFragment extends Fragment {


    private Button editProfileBtn;

    //    RecyclerView recyclerviewId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home, container, false);
        initView(v);

        ((MainActivity) getActivity()).setHeadingText("Home");
        findViews(v);

        editProfileBtn.setVisibility(View.GONE);
        if (Preferences.getInstance(getActivity()).getRole().equals("Attorney")) {
            view_questions.setVisibility(View.VISIBLE);
            editProfileBtn.setVisibility(View.VISIBLE);
            ask_attorneys.setVisibility(View.GONE);
            search_attorneys.setVisibility(View.GONE);
        }
//        setrecycler_on_home();
        setListnet();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
//        setrecycler_on_home();
    }

    /*public void setrecycler_on_home() {
        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setMessage("please wait...");
        p.setCancelable(false);
        p.show();
        WebServiceRequests.getInstance().getFeed(getActivity(), new Callback<feedResponse>() {
            @Override
            public void onResponse(Call<feedResponse> call, Response<feedResponse> response) {
                if (response.isSuccessful() && response.body().getGetFeedResult().isResult()) {
                    p.hide();


                    recyclerviewId.setLayoutManager(new LinearLayoutManager(getContext()));
//                    recyclerView.setHasFixedSize(true);
                    recyclerviewId.setAdapter(new feed_recycler_adapter((MainActivity) getActivity(),response.body().getGetFeedResult().getData()));
                } else {
                    p.hide();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<feedResponse> call, Throwable t) {
                p.hide();
                Toast.makeText(getActivity(), "Failed to connect network,please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }
*/

    Button ask_attorneys, search_attorneys, view_questions;


    private void setListnet() {
        final String sadd = "advertisement";

        ask_attorneys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent((MainActivity) getActivity(), advertisementActivity.class);
                i.putExtra("advert", sadd);
                startActivityForResult(i, 3);


            }
        });
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new Profile_Update()).commit();

            }
        });

        view_questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new View_questions()).commit();
            }
        });
        search_attorneys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent((MainActivity) getActivity(), advertisementActivity.class);
                i.putExtra("advert", sadd);
                startActivityForResult(i, 2);

            }
        });
    }


    private void add() {
        Intent i = new Intent(getActivity(), advertisementActivity.class);
        startActivity(i);


    }

    private void findViews(View v) {
//           recyclerviewId = (RecyclerView) v.findViewById(R.id.recyclerview_id);

        ask_attorneys = (Button) v.findViewById(R.id.ask_attorneys);
        search_attorneys = (Button) v.findViewById(R.id.search_attorneys);
        view_questions = (Button) v.findViewById(R.id.view_questions);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_CANCELED) {
                Intent i = new Intent(getActivity(), MarketPlaceAttorneyActivity.class);
                startActivity(i);

            }

        } else if (requestCode == 3) {
            if (resultCode == Activity.RESULT_CANCELED) {
                ((MainActivity) getActivity()).setHeadingText("Ask an Attorney");
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new AskAttorney()).addToBackStack("Home").commit();

            }
        }
    }


    private void initView(View v) {
        editProfileBtn = (Button) v.findViewById(R.id.edit_Profile_btn);
    }
}

