package com.bizbrolly.com.uplawapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.akkipedia.skeleton.utils.ScreenUtils;
import com.bizbrolly.com.uplawapp.WebServices.AdvertisementResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Response;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class advertisementActivity extends AppCompatActivity {
    Bundle extras;
    int position;
    private ImageView mContentView;
    private ImageView cancelImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_advertisement);

        extras = getIntent().getExtras();
        initView();
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        adds();

        cancelAdd();
    }



    private void adds() {

        String ansAdvert=extras.getString("ansAdvert");


        String advert = extras.getString("advert");
        final String adds = extras.getString("adds");
                            position = extras.getInt("position");
      /*  final ProgressDialog p = new ProgressDialog(this);
        p.setCancelable(false);
        p.setMessage("please wait...");
        p.show();*/
        if (advert != null) {
            if (advert.equals("advertisement")) {
                WebServiceRequests.getInstance().getaddss(this, new retrofit2.Callback<AdvertisementResponse>() {
                            @Override
                            public void onResponse(Call<AdvertisementResponse> call, Response<AdvertisementResponse> response) {
                                if (response.isSuccessful() && response.body().getGetAdvertismentResult().isResult()) {
                                    response.code();
//                                    p.hide();
                                    final AtomicBoolean loaded = new AtomicBoolean();
//                            String s = extras.getString("advertisement");
                                    String s = response.body().getGetAdvertismentResult().getData().getImagePath();



                                    Picasso.with(advertisementActivity.this).load(s).centerCrop().resize((int) ScreenUtils.getScreenWidth(), (int) ScreenUtils.getScreenHeight()).into((ImageView) mContentView, new Callback.EmptyCallback() {

                                        @Override
                                        public void onSuccess() {
//                                            p.hide();
                                            loaded.set(true);
                                        }
                                    });


                                } else {
//                                    p.dismiss();
                                    Toast.makeText(advertisementActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AdvertisementResponse> call, Throwable t) {
//                                p.dismiss();
                                Toast.makeText(advertisementActivity.this, "Failed to connect,please connect again", Toast.LENGTH_SHORT).show();
                            }
                        }
                );

            }
                 }
                 if(adds!=null) {
                     if (!adds.equalsIgnoreCase("")) {
                         int i = extras.getInt("position");
                     /*   final ProgressDialog p1=new ProgressDialog(this);
                         p1.setMessage("please wait...");




                         p1.show();*/
                         Picasso.with(advertisementActivity.this).load(adds).centerCrop().placeholder(R.drawable.placeholder).resize((int) ScreenUtils.getScreenWidth(), (int) ScreenUtils.getScreenHeight()).into(new Target() {
                             @Override
                             public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                 bitmap.compress(Bitmap.CompressFormat.PNG,70,new ByteArrayOutputStream());
                                 mContentView.setImageBitmap(bitmap);
//                                 p1.hide();
                             }

                             @Override
                             public void onBitmapFailed(Drawable errorDrawable) {

                             }

                             @Override
                             public void onPrepareLoad(Drawable placeHolderDrawable) {

                             }
                         });

                     } else {
                         Intent i = new Intent();
                         i.putExtra("position", position);
                         setResult(Activity.RESULT_CANCELED, i);
                         finish();
                     }

                 }
        if(ansAdvert!=null) {
            if (!ansAdvert.equalsIgnoreCase("")) {
                int i = extras.getInt("position");
//                final ProgressDialog p1=new ProgressDialog(this);
//                p1.setMessage("please wait...");
//
//
//
//
//                p1.show();
                Picasso.with(advertisementActivity.this).load(ansAdvert).centerCrop().placeholder(R.drawable.placeholder).resize((int) ScreenUtils.getScreenWidth(), (int) ScreenUtils.getScreenHeight()).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        bitmap.compress(Bitmap.CompressFormat.PNG,70,new ByteArrayOutputStream());
                        mContentView.setImageBitmap(bitmap);
//                        p1.hide();
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

            } else {
                Intent i = new Intent();
                i.putExtra("position", position);
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }

        }
//        p.hide();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

    }

    private void cancelAdd() {
        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("position", position);
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }
        });

    }

    private void initView() {
        mContentView = (ImageView) findViewById(R.id.fullscreen_content);
        cancelImageView = (ImageView) findViewById(R.id.cancel_imageView);
    }
//
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent();
        i.putExtra("position", position);
        setResult(Activity.RESULT_FIRST_USER, i);
        finish();
//        i.putExtra("position", position);
//        setResult(Activity.RESULT_CANCELED, i);
//        finish();

    }
}
