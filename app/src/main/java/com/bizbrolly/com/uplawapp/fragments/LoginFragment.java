package com.bizbrolly.com.uplawapp.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputType;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkipedia.skeleton.activities.BaseSkeletonActivity;
import com.akkipedia.skeleton.utils.ScreenUtils;
import com.bizbrolly.com.uplawapp.WebServices.ForgetPassword;
import com.bizbrolly.com.uplawapp.WebServices.SignInResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.activities.Signup_skin_chooser;
import com.bizbrolly.com.uplawapp.activities.WalkThroughActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.urbanairship.UAirship;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hp on 9/12/2017.
 */

public class LoginFragment extends Fragment {
    Button sign_in, sign_up;
    TextView next_screen, Dont_HAVE_ACC_View, id_login_forgotView;
    EditText username, password;
    RelativeLayout tohidekeyBoard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        findviews(v);
        setData();
        return v;
    }

//    private boolean isValid() {
//        boolean valid = true;
//        if (username.getText().toString().trim().length() == 0) {
//            username.setError("Enter Email Address");
//            valid = false;
//        } else if (Patterns.EMAIL_ADDRESS.matcher(username.getText().toString()).matches()) {
//            username.setError("Invalid Email Address");
//            valid = false;
//        }else if (password.getText().toString().length()==0){
//            password.setError("Enter Password");
//        }
//        return valid;
//    }


    private void setData() {
        id_login_forgotView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPasswordDialog();
            }
        });

        Dont_HAVE_ACC_View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sign_up.performLongClick();
            }
        });
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean email_check = false;
                boolean text_check = false;
                email_check = isEmailValid(username.getText().toString());
                text_check = validate_Text();
                if (username.getText().toString().length() == 0) {
                    username.requestFocus();
                    username.setError(Html.fromHtml("<font size='10sp'>Please Enter Email ID</font>"));
                } else if (!email_check) {
                    password.setText("");
                    username.setText("");
                    username.requestFocus();
                    username.setError(Html.fromHtml("<font size='10sp'>Please Enter valid Email ID</font>"));
                }

                if (email_check && text_check) {
                    signedIn();

                }
            }
        });

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), Signup_skin_chooser.class);
                startActivity(i);
            }
        });

        next_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((WalkThroughActivity) getActivity()).setPagerState(1);

//
//                tohidekeyBoard.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//
//
//                    }
//                });

            }
        });
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validate_Text() {
        boolean flag = true;
        if (password.getText().toString().length() == 0) {

            password.requestFocus();
            password.setError(Html.fromHtml("<font size='10sp'>Password should not be empty</font>"));

            flag = false;
        }
//        } else if (password.getText().toString().length() < 6) {
//            password.setError("Password is too short");
//            password.setFocusable(true);
//        }
//        else if (password.getText().toString().length() >= 6) {
//            if (!password.getText().toString().matches("[A-Za-z0-9]+")) {
//                password.setError("Enter alphaNumeric password");
//                password.setFocusable(true);
//            }
//        }
//        if (password.getText().toString().length() >= 6) {
//            if (password.getText().toString().matches("[A-Za-z0-9]+")) {
//                password.setFocusable(true);
//                flag = true;
//            }
//        }
        return flag;
    }


    private void signedIn() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        WebServiceRequests.getInstance().signIn(getActivity(),
                username.getText().toString(),
                password.getText().toString()
                , new Callback<SignInResponse>() {
                    @Override
                    public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                        progressDialog.hide();
                        response.code();
                        Request request = call.request();
                        if (response.isSuccessful() && response.body().getSignInResult().isResult()) {
                            int userid=response.body().
                                    getSignInResult().getData().getUserDetails().getUserId();

                            if (response.body().getSignInResult().getData().getUserDetails().getRoleDetails()
                                    .getRoleId() == 102) {



                                UAirship.shared().getPushManager().editTags()
                                        .addTag(String.valueOf(userid))
                                        .apply();
                                Preferences.getInstance(getActivity()).setLoginType("102");

                                Preferences.getInstance(getActivity()).setUserId(response.body().
                                        getSignInResult().getData().getUserDetails().getUserId());

                                Preferences.getInstance(getActivity()).setAuthToken(response.body().
                                        getSignInResult().getData().getAuthToken());

                                Preferences.getInstance(getActivity()).setDOB((String) response
                                        .body().getSignInResult().getData().getUserDetails().getDOB());

                                Preferences.getInstance(getActivity()).setUserName(response.body().
                                        getSignInResult().getData().getUsername());
                                //                                Preferences.getInstance(LoginActivity.this).setDOB(response.body().getSignInResult()
                                //                                .getData().dob());
                                Preferences.getInstance(getActivity()).setName(response.body().
                                        getSignInResult().getData().getUserDetails().getFirstName());
                                Preferences.getInstance(getActivity()).setLoggedIn(true);
                                Preferences.getInstance(getActivity()).setPhone((String) response.body().
                                        getSignInResult().getData().getUserDetails().getPhone());
                                Preferences.getInstance(getActivity()).setfbLogin(true);

                                Preferences.getInstance(getActivity()).setRole(response.body().getSignInResult().getData().getUserDetails().getRoleDetails().getRole());

                                Preferences.getInstance(getActivity()).setImage(response.body().getSignInResult().getData().getUserDetails().getUserImage());
                                moveTo();
                            } else if (response.body().getSignInResult().getData().getUserDetails().getRoleDetails()
                                    .getRoleId() == 103) {

                                UAirship.shared().getPushManager().editTags()
                                        .addTag(String.valueOf(userid))
                                        .apply();
                               /* UAirship.shared().getPushManager().editTags()
                                        .addTag(username.getText().toString().trim())
                                        .apply();*/
                                UAirship.shared().getPushManager().editTags()
                                        .addTag("Attorney")
                                        .apply();

                                Preferences.getInstance(getActivity()).setUserId(response.body().
                                        getSignInResult().getData().getUserDetails().getUserId());

                                Preferences.getInstance(getActivity()).setAuthToken(response.body().
                                        getSignInResult().getData().getAuthToken());

                                Preferences.getInstance(getActivity()).setLoggedIn(true);

                                Preferences.getInstance(getActivity()).setUserName((String) response.body().
                                        getSignInResult().getData().getUsername());

                                Preferences.getInstance(getActivity()).setName((String) response.body().
                                        getSignInResult().getData().getUserDetails().getFirstName());

                                Preferences.getInstance(getActivity()).setPhone((String) response.body().
                                        getSignInResult().getData().getUserDetails().getPhone());



                                Preferences.getInstance(getActivity()).setAttorneys_Biography_ID((String) response.body().
                                        getSignInResult().getData().getAttorneyDetails().getAttorneysBio());

                                Preferences.getInstance(getActivity()).setSpecializationID((String) response.body().
                                        getSignInResult().getData().getAttorneyDetails().getSpecialization());

                                Preferences.getInstance(getActivity()).setLocationID((String) response.body().
                                        getSignInResult().getData().getAttorneyDetails().getLocation());

                                Preferences.getInstance(getActivity()).setYearExperienceId((String) response.body().
                                        getSignInResult().getData().getAttorneyDetails().getYearExperience());

                                Preferences.getInstance(getActivity()).setBusinessHoursDD((String) response.body().
                                        getSignInResult().getData().getAttorneyDetails().getBusinessHours());




                                Preferences.getInstance(getActivity()).setImage((String) response.body().getSignInResult().getData().getUserDetails().getUserImage());

                                Preferences.getInstance(getActivity()).setApproved(response.body().getSignInResult().getData().isIsApproved());

                                Preferences.getInstance(getActivity()).setRole(response.body().getSignInResult().getData().getUserDetails().getRoleDetails().getRole());
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finishAffinity();
                              /*  if (response.body().getSignInResult().getData().isIsApproved()) {

                                } else {
                                    Intent i = new Intent(getActivity(), MainActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    getActivity().finishAffinity();
                                }*/
                            }

                        } else {
                            Toast.makeText(getActivity(), response.body().getSignInResult().getErrorDetail().getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignInResponse> call, Throwable t) {
                        progressDialog.hide();
                        Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }

    private void moveTo() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        getActivity().finish();


    }


    private void findviews(View v) {
        username = (EditText) v.findViewById(R.id.username);
        password = (EditText) v.findViewById(R.id.password);
        sign_in = (Button) v.findViewById(R.id.sign_in);
        sign_up = (Button) v.findViewById(R.id.sign_up);
        next_screen = (TextView) v.findViewById(R.id.next_screen);
        tohidekeyBoard = (RelativeLayout) v.findViewById(R.id.relative_layout);
        Dont_HAVE_ACC_View = (TextView) v.findViewById(R.id.Dont_HAVE_ACC);
        id_login_forgotView = (TextView) v.findViewById(R.id.id_login_forgot);
    }

    AlertDialog alertDialog;

    private void forgotPasswordDialog() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        final EditText emailEditText = new EditText(getContext());
        linearLayout.addView(emailEditText);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) emailEditText.getLayoutParams();
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.setMargins(ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10), ScreenUtils.dpToPx(10));
        emailEditText.setHint("Enter Email Address");
        emailEditText.setSingleLine();
        //emailEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        emailEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        emailEditText.setLayoutParams(layoutParams);
        alertDialog = new AlertDialog.Builder(getContext()).setView(linearLayout)
                .setTitle("Forgot Password?")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Done", null).create();


        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (emailEditText.getText().toString().trim().isEmpty()) {
                            emailEditText.setError("Field should not be empty");
                        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                            emailEditText.setError("Enter Valid Email ID");
                        } else {
                            continue_func(emailEditText.getText().toString().trim());
                        }
                    }
                });
            }
        });
        alertDialog.show();
    }

    private void continue_func(final String email_EditView) {
        ((BaseSkeletonActivity) getActivity()).showProgressDialog();
        WebServiceRequests.getInstance().forgetPassword(email_EditView, new Callback<ForgetPassword>() {
            @Override
            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                if (response.isSuccessful() && response.body().getForgetPasswordResult().isResult()) {
                    Preferences.getInstance(getContext()).setEmailForgot(email_EditView);
                    Toast.makeText(getContext(), "" + response.body().getForgetPasswordResult().getData(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "" + response.body().getForgetPasswordResult().getErrorDetail().getErrorDetails(), Toast.LENGTH_SHORT).show();
                }
                ((BaseSkeletonActivity) getActivity()).hideProgressDialog();
                alertDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                ((BaseSkeletonActivity) getActivity()).hideProgressDialog();
                Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
