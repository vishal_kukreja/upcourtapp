package com.bizbrolly.com.uplawapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bizbrolly.com.uplawapp.fragments.LearnVideoFragment;
import com.bizbrolly.com.uplawapp.fragments.LoginFragment;

/**
 * Created by gaurav on 06/04/17.
 */

public class WalkThroughAdapter extends FragmentStatePagerAdapter {
    public LoginFragment loginFragment;
    public LearnVideoFragment learnVideoFragment;

    public WalkThroughAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (loginFragment == null)
                loginFragment = new LoginFragment();
            return loginFragment;
        } else {
            if (learnVideoFragment == null)
                learnVideoFragment = new LearnVideoFragment();
            return learnVideoFragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return 2;
    }
}
