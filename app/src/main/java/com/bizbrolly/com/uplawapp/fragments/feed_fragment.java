package com.bizbrolly.com.uplawapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.feedResponse;
import com.bizbrolly.com.uplawapp.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class feed_fragment extends Fragment {


    private Toolbar toolbarr;
    private TextView titleFeedTextView;
    private TextView messageFeedTextView;
    private ImageView imageFeed;
    private TextView noOfLikesTextview;
    private TextView likeTextview;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String data=getArguments().getString("databundle");
        Bundle bundle = getArguments();
        feedResponse.GetFeedResultBean.DataBean data= (feedResponse.GetFeedResultBean.DataBean) bundle.getSerializable("databundle");
        View v = inflater.inflate(R.layout.fragment_feed_fragment, container, false);
        initView(v);
        setData(data);
        setActionTool((MainActivity)getActivity());
        // Inflate the layout for this fragment
        return v;

    }

    private void setData(feedResponse.GetFeedResultBean.DataBean data) {
        titleFeedTextView.setText(data.getTitle());
        messageFeedTextView.setText(data.getDescription());
    }

    private void setActionTool(MainActivity activity) {
        activity.setSupportActionBar(toolbarr);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
       activity.getSupportActionBar().setTitle("Feed");
    }

    private void initView(View v) {
        toolbarr = (Toolbar) v.findViewById(R.id.toolbarr);
        titleFeedTextView = (TextView) v.findViewById(R.id.title_feed_textView);
        messageFeedTextView = (TextView) v.findViewById(R.id.message_feed_textView);
        imageFeed = (ImageView) v.findViewById(R.id.image_feed);
        noOfLikesTextview = (TextView) v.findViewById(R.id.no_of_likes_textview);
        likeTextview = (TextView) v.findViewById(R.id.like_textview);
    }
}
