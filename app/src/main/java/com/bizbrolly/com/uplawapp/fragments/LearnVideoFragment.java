package com.bizbrolly.com.uplawapp.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.activities.WalkThroughActivity;

/**
 * Created by hp on 9/12/2017.
 */

public class LearnVideoFragment extends Fragment {
    VideoView video_view;
    ImageView play_video,pause_video;
    TextView previous_screen;
    ProgressBar progress_bar;
    RelativeLayout video_relative_view;
    View vb;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.sample_video_screen,container,false);
        findViews(v);
        setListner();
        return v;

    }


    public static LearnVideoFragment getInstance(){

        return   new LearnVideoFragment();

    }

    public void pauseVideo(){
        if(video_view.isPlaying()){
            video_view.pause();
            pause_video.setVisibility(View.GONE);
            play_video.setVisibility(View.VISIBLE);
        }
    }

    private void setListner() {
        play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(video_view.isPlaying()){
                    video_view.resume();
                }else {
                    progress_bar.setVisibility(View.VISIBLE);
                    //Uri uri = Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
                    Uri uri=Uri.parse("http://bizbrollydev.cloudapp.net/UpCourt/Video/video.mp4");
                    video_view.setVideoURI(uri);
                    video_view.start();
                    play_video.setVisibility(View.GONE);
                }
            }
        });

        pause_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(video_view.isPlaying()){
                    video_view.pause();
                    pause_video.setVisibility(View.GONE);
                    play_video.setVisibility(View.VISIBLE);
                }
            }
        });

        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                progress_bar.setVisibility(View.GONE);
            }
        });

        video_relative_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(video_view.isPlaying()){
                    pause_video.setVisibility(View.VISIBLE);
                    //play_video.setVisibility(View.VISIBLE);
                }
            }
        });

        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_video.setVisibility(View.VISIBLE);
            }
        });

        previous_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                video_view.pause();
                pause_video.setVisibility(View.GONE);
                play_video.setVisibility(View.VISIBLE);
                ((WalkThroughActivity)getActivity()).setPagerState(0);
            }
        });
    }
    public View find(){
        return vb;
    }
    private void findViews(View v) {
        vb=v;
        video_view=(VideoView)v.findViewById(R.id.video_view);
        play_video=(ImageView)v.findViewById(R.id.play_video);
        pause_video=(ImageView)v.findViewById(R.id.pause_video);
        video_relative_view=(RelativeLayout)v.findViewById(R.id.video_relative_view);
        progress_bar=(ProgressBar)v.findViewById(R.id.progress_bar);
        previous_screen=(TextView)v.findViewById(R.id.previous_screen);
    }
}