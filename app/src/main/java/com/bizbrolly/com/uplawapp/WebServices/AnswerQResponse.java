package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by vishal on 20-11-2017.
 */

public class AnswerQResponse {

    /**
     * AnswerResult : {"Data":105,"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private AnswerResultBean AnswerResult;

    public AnswerResultBean getAnswerResult() {
        return AnswerResult;
    }

    public void setAnswerResult(AnswerResultBean AnswerResult) {
        this.AnswerResult = AnswerResult;
    }

    public static class AnswerResultBean {
        /**
         * Data : 105
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private int Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public int getData() {
            return Data;
        }

        public void setData(int Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
