package com.bizbrolly.com.uplawapp.WebServices;

import android.content.Context;

import com.akkipedia.skeleton.utils.GeneralUtils;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by bizbrolly on 2/13/17.
 */

public class WebServiceRequests {
    private static WebServiceRequests instance;
    private ApiInterface apiInterface;


    private WebServiceRequests() {
//        Retrofit apiClient = ApiClient.getClient();
//        apiInterface = apiClient.create(ApiInterfaceTest.class);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public static WebServiceRequests getInstance() {
        if (instance == null)
            instance = new WebServiceRequests();
        return instance;
    }


    public void signIn(Context context,
                       String userName,
                       String password,
                       Callback<SignInResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, userName);
//                                                                            params.put(Constants.KEYS.FacebookId, "");
        params.put(Constants.KEYS.Password, GeneralUtils.SHA1(password));
        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
        Call<SignInResponse> call = apiInterface.signin(params);
        call.enqueue(callback);
    }

    public void signUp(Context context,
                       String firstName,
                       String email,
                       String password,
                       String phone,
                       Callback<SignUpResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.FirstName, firstName);
        params.put(Constants.KEYS.Username, email);
//        params.put("Username","abc@abc.abc");
        params.put(Constants.KEYS.Password, GeneralUtils.SHA1(password));
        params.put(Constants.KEYS.Phone, phone);
        params.put(Constants.KEYS.Email, email);
        params.put(Constants.KEYS.UserImage, "");
        params.put(Constants.KEYS.Imagebase64Content, "");

        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
        params.put(Constants.KEYS.Role, "User");
        params.put(Constants.KEYS.IsAgree, true);
        Call<SignUpResponse> call = apiInterface.signUp(params);
        call.enqueue(callback);
    }

    public void viewQuestionsAttorney(Context context,

                                      String email,
                                      String authtoken,
                                      int QuestionId,
                                      int MyQuestion,
                                      Callback<View_questions_attorneyResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();

        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.AuthToken, authtoken);
        params.put(Constants.KEYS.QuestionId, QuestionId);
        params.put(Constants.KEYS.MyQuestion, MyQuestion);


        //        Preferences.getInstance(getActivity()).getAuthToken()
        Call<View_questions_attorneyResponse> call = apiInterface.viewQuestionAttorney(params);
        call.enqueue(callback);
    }

    public void signUpAttorney(Context context,
                               String firstName,
                               String email,
                               String password,
                               String phone,
                               String AttorneysBio,
                               String Specialization,
                               String Location,
                               String YearExperience,
                               String BusinessHours,
                               String imageBase64,
                               Callback<SignUpResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.FirstName, firstName);
        params.put(Constants.KEYS.Username, email);

        params.put(Constants.KEYS.Password, GeneralUtils.SHA1(password));
        params.put(Constants.KEYS.Phone, phone);
        params.put(Constants.KEYS.Email, email);
        params.put(Constants.KEYS.UserImage, Calendar.getInstance().getTime().getTime() + ".jpg");
//        params.put(Constants.KEYS.Imagebase64Content, "");

        params.put(Constants.KEYS.AttorneysBio, AttorneysBio);
        params.put(Constants.KEYS.Specialization, Specialization);
        params.put(Constants.KEYS.Location, Location);
        params.put(Constants.KEYS.YearExperience, YearExperience);
        params.put(Constants.KEYS.BusinessHours, BusinessHours);
        params.put(Constants.KEYS.Imagebase64Content, imageBase64);
        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
        params.put(Constants.KEYS.Role, "Attorney");
        params.put(Constants.KEYS.IsAgree, true);
        Call<SignUpResponse> call = apiInterface.signUp(params);
        call.enqueue(callback);
    }

    public void signOut(Context context,
                        String username,
                        Callback<SignOut> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, username);
        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
        Call<SignOut> call = apiInterface.signout(params);
        call.enqueue(callback);

    }

    public void submitIt(String username,
                         String AUTHTOKEN,
                         String Question,
                         Callback<AskResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, username);
        params.put(Constants.KEYS.AuthToken, AUTHTOKEN);
        params.put(Constants.KEYS.Questions, Question);
        Call<AskResponse> call = apiInterface.askResponse(params);
        call.enqueue(callback);
    }

    public void getAttorney(Context context,
                            String email,
                            String AUTHTOKEN,
                            Callback<SearchAttorneyList> callback) {
        HashMap<String, Object> params = new HashMap<>();

        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.AuthToken, AUTHTOKEN);
        Call<SearchAttorneyList> call = apiInterface.searchAttorney(params);
        call.enqueue(callback);
    }

    public void forgetPassword(String email,

                               Callback<ForgetPassword> callback) {
        HashMap<String, Object> params = new HashMap<>();

        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
        Call<ForgetPassword> call = apiInterface.forgetResponse(params);
        call.enqueue(callback);
    }

    public void answerQuestion(String email,
                               int QuestionId,
                               Context context,
                               String AUTHTOKEN,
                               String Answer,
                               long userId, Callback<AnswerQResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.AuthToken, AUTHTOKEN);
        params.put(Constants.KEYS.QuestionId, QuestionId);
        params.put(Constants.KEYS.Answer, Answer);
        params.put("QuestionUserId",userId);
        Call<AnswerQResponse> call=apiInterface.AnswerResponse(params);
call.enqueue(callback);
    }

    public void changePasswordd(
            Context context,
            String email,
            String oldpassword,
            String newPassword,
            Callback<changePassword> callback) {
        HashMap<String, Object> params = new HashMap<>();

        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
        params.put(Constants.KEYS.Old_password, GeneralUtils.SHA1(oldpassword));
        params.put(Constants.KEYS.NEw_password, GeneralUtils.SHA1(newPassword));
        Call<changePassword> call = apiInterface.changeResponse(params);
        call.enqueue(callback);

    }

    public void profileUpdate(
            Context context,
            String email,
            String new_name,
            String newPhone,
            String userImage,
            String ImageBase64,

            String role,
            String attorneyBio,
            String Specialization,
            String Location,
            String YearExperience,
            String BusinessHours,

            Callback<ProfileUpdateResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();

        params.put(Constants.KEYS.Username, email);
        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
        params.put("FirstName", new_name);
        params.put(Constants.KEYS.Newphone, newPhone);
        params.put(Constants.KEYS.UserImage, userImage);
        params.put(Constants.KEYS.Imagebase64Content, ImageBase64);

        params.put(Constants.KEYS.Role,role);
        //Attorney..................................
        params.put(Constants.KEYS.AttorneysBio,attorneyBio);
        params.put(Constants.KEYS.Specialization,Specialization);
        params.put(Constants.KEYS.Location,Location);
        params.put(Constants.KEYS.YearExperience,YearExperience);
        params.put(Constants.KEYS.BusinessHours,BusinessHours);

        Call<ProfileUpdateResponse> call = apiInterface.profileResponse(params);
        call.enqueue(callback);

    }


    public void getFeed(Context context,
                        Callback<feedResponse> callback) {
        HashMap<String, Object> param = new HashMap<>();
        param.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
        param.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
        Call<feedResponse> call = apiInterface.getfeedRespnse(param);
        call.enqueue(callback);
    }

    //
    public void getlike(Context context,
                        int feedid,
                        int isLike,
                        Callback<LikeResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
        params.put("FeedId", feedid);
        params.put("isLike", isLike);

        Call<LikeResponse> call = apiInterface.getLikeRespnse(params);
        call.enqueue(callback);
    }

    public void getaddss(Context context,
                         Callback<AdvertisementResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());

        Call<AdvertisementResponse> call = apiInterface.getAddsRespnse(params);
        call.enqueue(callback);
    }


    public void getContact(int AttorneyId,
                           Context context,
                           Callback<ContactResponse> callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
        params.put("AttorneyId", AttorneyId);
        Call<ContactResponse> call = apiInterface.getContactRespnse(params);
        call.enqueue(callback);
    }


//    public void facebookSignIn(
//            Context context,
//            String userName,
//            String fbId,
//            Callback<SignInResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, userName);
//        params.put(Constants.KEYS.Password, "");
//        params.put(Constants.KEYS.FacebookId, fbId);
//        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
//        params.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
//        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        Call<SignInResponse> call = apiInterface.signin(params);
//        call.enqueue(callback);
//    }
//
//    public void userSignUp(Context context,
//                           String password,
//                           String name,
//                           String emailId,
//                           String userNameId,
//                           String phoneNumber,
//                           Date dob,
//                           Callback<SignUpResponse> callback) {
//
//        HashMap<String, Object> param = new HashMap<>();
//        param.put(Constants.KEYS.Username, emailId);
//        param.put(Constants.KEYS.Password, GeneralUtils.SHA1(password));
//        param.put(Constants.KEYS.FacebookId, "");
//        param.put(Constants.KEYS.Name, name);
//        param.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        param.put(Constants.KEYS.EmailId, userNameId);
//        param.put(Constants.KEYS.Phone, phoneNumber);
//        param.put(Constants.KEYS.Role, "User");
//        param.put(Constants.KEYS.DOB, GeneralUtils.dateToJSONString(dob));
//        param.put(Constants.KEYS.DoctorFee, "");
//        param.put(Constants.KEYS.DoctorBio, "");
//       /* if (imageBase64 != null && imageBase64.length() > 0) {
//            param.put(Constants.KEYS.Imagebase64Content, imageBase64);
//        } else {
//            param.put(Constants.KEYS.Imagebase64Content, "");
//        }
//        param.put(Constants.KEYS.Imagebase64Content, "");*/
//        param.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
//        param.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
//        Call<SignUpResponse> call = apiInterface.signUp(param);
//        call.enqueue(callback);
//
//    }
//
//
//    public void doctorSignUp(Context context,
//                             String password,
//                             String name,
//                             String emailId,
//                             String userNameId,
//                             String phoneNumber,
//                             String doctorFee,
//                             String doctorbio,
//                             Callback<SignUpResponse> callback) {
//
//        HashMap<String, Object> param = new HashMap<>();
//        param.put(Constants.KEYS.Username, emailId);
//        param.put(Constants.KEYS.Password, GeneralUtils.SHA1(password));
//        param.put(Constants.KEYS.FacebookId, "");
//        param.put(Constants.KEYS.Name, name);
//        param.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        param.put(Constants.KEYS.EmailId, userNameId);
//        param.put(Constants.KEYS.Phone, phoneNumber);
//        param.put(Constants.KEYS.Role, "Doctor");
//        param.put(Constants.KEYS.DoctorFee, doctorFee);
//        param.put(Constants.KEYS.DoctorBio, doctorbio);
//       /* if (imageBase64 != null && imageBase64.length() > 0) {
//            param.put(Constants.KEYS.Imagebase64Content, imageBase64);
//        } else {
//            param.put(Constants.KEYS.Imagebase64Content, "");
//        }
//        param.put(Constants.KEYS.Imagebase64Content, "");*/
//        param.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
//        param.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
//        Call<SignUpResponse> call = apiInterface.signUp(param);
//        call.enqueue(callback);
//
//    }
//
//    public void faceBookSignUp(Context context,
//                               String emailId,
//                               String name,
//                               String phoneNumber,
//                               String facebookId,
//                               String roleType,
//                               Callback<SignUpResponse> callback) {
//
//        HashMap<String, Object> param = new HashMap<>();
//        param.put(Constants.KEYS.Username, emailId);
//        param.put(Constants.KEYS.Password, "");
//        param.put(Constants.KEYS.FacebookId, facebookId);
//        param.put(Constants.KEYS.Name, name);
//        param.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        param.put(Constants.KEYS.EmailId, emailId);
//        param.put(Constants.KEYS.Phone, phoneNumber);
//        param.put(Constants.KEYS.Role, roleType);
//        param.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
//        param.put(Constants.KEYS.ClientMachineName, GeneralUtils.getUniqueIdentifier(context));
//
//        Call<SignUpResponse> call = apiInterface.signUp(param);
//        call.enqueue(callback);
//
//    }
//
//    public void logOut(
//            Context context,
//            Callback<LogOutResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.ClientIPAddress, GeneralUtils.getUniqueIdentifier(context));
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
//        Call<LogOutResponse> call = apiInterface.logOut(params);
//        call.enqueue(callback);
//    }
//
//
//    public void getUserDetails(
//            Context context,
//            Callback<GetUserDetailsResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
//        Call<GetUserDetailsResponse> call = apiInterface.getUserDetails(params);
//        call.enqueue(callback);
//    }
//
//    public void getMedicalProfession(
//            Context context,
//            Callback<GetMasterDataResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.MasterDataType, Constants.MedicalProfession);
//        params.put(Constants.KEYS.MasterDataId, 0);
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        Call<GetMasterDataResponse> call = apiInterface.getMasterData(params);
//        call.enqueue(callback);
//    }
//
//
//    public void getDoctorBySpecialization(
//            Context context,
//            int specializationId,
//            Callback<GetDoctorBySpecializationResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
//        params.put(Constants.KEYS.SpecializationId,specializationId );
//        Call<GetDoctorBySpecializationResponse> call = apiInterface.getDoctorBySpecialization(params);
//        call.enqueue(callback);
//    }
//
//    public void saveAppointMent(
//            Context context,
//            int doctorId,
//            Date appointmentDate,
//            String appointmentCode,
//            Callback<SaveAppointMentResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
//        params.put(Constants.KEYS.DoctorId,doctorId );
//        params.put(Constants.KEYS.AppointmentDate, GeneralUtils.dateToJSONString(appointmentDate) );
//        params.put(Constants.KEYS.AppointmentCode,appointmentCode );
//        Call<SaveAppointMentResponse> call = apiInterface.saveAppointMent(params);
//        call.enqueue(callback);
//    }
//
//
// public void getUserAppointments(
//            Context context,
//            int appointmentof,
//            Callback<GetAppointMentsResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.AuthToken, Preferences.getInstance(context).getAuthToken());
//        params.put(Constants.KEYS.PatientId,0 );
//        params.put(Constants.KEYS.DoctorId,0);
//        params.put(Constants.KEYS.Appointmentof,appointmentof);
//        Call<GetAppointMentsResponse> call = apiInterface.getUserAppointments(params);
//        call.enqueue(callback);
//    }
//
//    public void getDisclamerText(
//            Context context,
//            Callback<GetTextResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, Preferences.getInstance(context).getUserName());
//        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        params.put(Constants.KEYS.TextId,0 );
//        params.put(Constants.KEYS.TextType,"Disclaimer");
//        Call<GetTextResponse> call = apiInterface.getDisclamerText(params);
//        call.enqueue(callback);
//    }
//    public void forgotPassword(
//            Context context,
//            String email,
//            Callback<ForgotPasswordResponse> callback) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(Constants.KEYS.Username, email);
//        params.put(Constants.KEYS.APIKey, Constants.API_KEY);
//        Call<ForgotPasswordResponse> call = apiInterface.forgotPassword(params);
//        call.enqueue(callback);
//    }
//


}
