package com.bizbrolly.com.uplawapp.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.SignOut;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.WalkThroughActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Not_Approved extends Fragment {


    private ImageView logo;
    private Button logout;
    String username = Preferences.getInstance(getActivity()).getUserName();
    private TextView hiTextView;

    public Not_Approved() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_not__approved, container, false);

        initView(v);
        setData();
        listener();
        return v;

    }

    private void setData() {
        hiTextView.setText("Hi "+Preferences.getInstance(getActivity()).getName().trim());
    }

    private void listener() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder ab = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                ab.setMessage("Are you sure you want to Logout?");
                ab.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        signOutHere();
                    }
                });
                ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                ab.setCancelable(false);
                ab.show();

            }
        });
    }

    private void signOutHere() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Preferences.getInstance(getActivity()).destroy_SharedPreference();
        WebServiceRequests.getInstance().signOut(getActivity(), username, new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, Response<SignOut> response) {
                if (response.isSuccessful() && response.body().getSignOutResult().isResult()) {
                    progressDialog.hide();
                    startActivity(new Intent(getActivity(), WalkThroughActivity.class));
                    getActivity().finish();
                }else {
                    progressDialog.hide();
                    Toast.makeText(getActivity(), ""+response.body().getSignOutResult().getErrorDetail().getErrorDetails(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                progressDialog.hide();
                Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initView(View v) {
        logo = (ImageView) v.findViewById(R.id.logo);
        logout = (Button) v.findViewById(R.id.logout);
        hiTextView = (TextView) v.findViewById(R.id.hi_textView);
    }
}
