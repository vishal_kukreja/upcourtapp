package com.bizbrolly.com.uplawapp.WebServices;

/**
 * Created by vishal on 25-09-2017.
 */

public class changePassword {
    /**
     * ChangePasswordResult : {"Data":true,"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private ChangePasswordResultBean ChangePasswordResult;

    public ChangePasswordResultBean getChangePasswordResult() {
        return ChangePasswordResult;
    }

    public void setChangePasswordResult(ChangePasswordResultBean ChangePasswordResult) {
        this.ChangePasswordResult = ChangePasswordResult;
    }

    public static class ChangePasswordResultBean {
        /**
         * Data : true
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private boolean Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public boolean isData() {
            return Data;
        }

        public void setData(boolean Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
