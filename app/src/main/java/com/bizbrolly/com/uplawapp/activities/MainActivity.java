package com.bizbrolly.com.uplawapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.ChangePassword;
import com.bizbrolly.com.uplawapp.Profile_Update;
import com.bizbrolly.com.uplawapp.R;
import com.bizbrolly.com.uplawapp.WebServices.SignOut;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.advertisementActivity;
import com.bizbrolly.com.uplawapp.fragments.AskAttorney;
import com.bizbrolly.com.uplawapp.fragments.HomeFragment;
import com.bizbrolly.com.uplawapp.fragments.Not_Approved;
import com.bizbrolly.com.uplawapp.fragments.View_questions;
import com.bizbrolly.com.uplawapp.privacy_policy;
import com.bizbrolly.com.uplawapp.terms_and_conditions;
import com.bizbrolly.com.uplawapp.utills.ConstantUtill;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.squareup.picasso.Picasso;
import com.urbanairship.UAirship;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String appID = "c4f6b63e-a4dd-41c4-8730-cf415a09076f";
    String AppKey = "fbf98fb9-8f06-4d23-9629-bee50a41dfc4";

    DrawerLayout drawerLayout;
    ImageView profile_imageView;
    Toolbar toolbar;
    TextView header_text, profileNameTextView;
    LinearLayout home_view, logout, ask_attoneys, search_attoneys, change_Password_navigationDrawerLayoutID, terms_conditions, viewQuestionLayt, viewProfileLayt, privacy_policy, Update_Profile_viewID;
    String username = Preferences.getInstance(MainActivity.this).getUserName();
    private TextView chatWithSupportID;
    private LinearLayout logoutAttorney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();


        if (Preferences.getInstance(this).getRole().equals("Attorney")) {

            if (Preferences.getInstance(this).getApproved()) {
                setData();
                logout.setVisibility(View.GONE);
                Update_Profile_viewID.setVisibility(View.GONE);

                chatWithSupportID.setVisibility(View.GONE);
                search_attoneys.setVisibility(View.GONE);
                ask_attoneys.setVisibility(View.GONE);
                viewProfileLayt.setVisibility(View.VISIBLE);
                setupNavDrawer();
            } else {
                set_not_Approved_data();
            }

        } else {
            chatWithSupportID.setVisibility(View.VISIBLE);
            logoutAttorney.setVisibility(View.GONE);
            viewProfileLayt.setVisibility(View.GONE);

            setData();
            setupNavDrawer();
        }
        setListner();
        initialize();
        createUser();
        Bundle bundleData = getIntent().getExtras();
        if (bundleData != null) {
            if (bundleData.getString("KEY").equalsIgnoreCase(ConstantUtill.ATTORNEYS_PROFILE)) {
                ask_attoneys.performClick();
            }
        }


    }

    @Override
    protected void onStart() {
        refreshDrawerProfile();
        super.onStart();
    }

    private void set_not_Approved_data() {
        header_text.setText("Home");
//        if (Preferences.getInstance(this).getImage().length() > 0)
//            Picasso.with(this).load(Preferences.getInstance(this).getImage()).resize(200, 200).centerCrop().into(profile_imageView);
//        profileNameTextView.setText(Preferences.getInstance(this).getName());
        getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new Not_Approved()).commit();


    }

    private void initialize() {
        FreshchatConfig freshchatConfig = new FreshchatConfig(appID, AppKey);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);
    }

    private void sendMesage() {
//        String tag = "premium";
//        String msgText = "User has trouble with order #1234";
//        FreshchatMessage = new FreshchatMessage().setTag(tag).setMessage(msgText);
//        Freshchat.sendMessage(this, Fres                                          hchatMessage);
//        Toast.makeText(this, "message sent :\n"+ msgText, Toast.LENGTH_SHORT).show();
        getMessageResult();
    }

    private void createUser() {
        //Get the user object for the current installation
        FreshchatUser freshUser = Freshchat.getInstance(getApplicationContext()).getUser();

        freshUser.setFirstName(Preferences.getInstance(this).getName());
        //freshUser.setLastName("Chauhan");
        freshUser.setEmail(Preferences.getInstance(this).getUserName());
        //freshUser.setExternalId("anup.doe");
        freshUser.setPhone("+91", Preferences.getInstance(this).getPhone());

        //Call setUser so that the user information is synced with Freshchat's servers
        Freshchat.getInstance(getApplicationContext()).setUser(freshUser);
    }

    private void getMessageResult() {


        Freshchat.showConversations(this);
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void setHeadingText(String headText) {
        header_text.setText(headText);
    }

    private void setListner() {

        chatWithSupportID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(MainActivity.this, Chat_with_support.class) ;
//                startActivity(i);
                sendMesage();
            }
        });


        Update_Profile_viewID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new Profile_Update()).commit();

            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                Freshchat.resetUser(getApplicationContext());
                signOutHere();
            }
        });
        logoutAttorney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                signOutHere();
            }
        });

        viewQuestionLayt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new View_questions()).commit();
            }
        });

        home_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                header_text.setText("Home");
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
            }
        });
        final String sadd = "advertisement";
        ask_attoneys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                Intent i = new Intent(MainActivity.this, advertisementActivity.class);
                i.putExtra("advert", sadd);

                startActivityForResult(i, 3);


            }
        });

        search_attoneys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();

                Intent i = new Intent(MainActivity.this, advertisementActivity.class);
                i.putExtra("advert", sadd);
                startActivityForResult(i, 2);
            }
        });
        change_Password_navigationDrawerLayoutID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new ChangePassword()).commit();
            }
        });
        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new privacy_policy()).commit();
            }
        });
        terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new terms_and_conditions()).commit();
            }
        });

        viewProfileLayt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AttorneyProfileActivity.class);
                i.putExtra("year", Preferences.getInstance(MainActivity.this).getYearExperienceId());
                i.putExtra("name", Preferences.getInstance(MainActivity.this).getName());
                i.putExtra("Specialization", Preferences.getInstance(MainActivity.this).getSpecializationID());
                i.putExtra("Location", Preferences.getInstance(MainActivity.this).getLocationID());
                i.putExtra("Business_Hours", Preferences.getInstance(MainActivity.this).getBusinessHoursDD());
                i.putExtra("Attorney_Bio", Preferences.getInstance(MainActivity.this).getAttorneys_Biography_ID());
                i.putExtra("Email", Preferences.getInstance(MainActivity.this).getUserName());
                i.putExtra("Image", (String) Preferences.getInstance(MainActivity.this).getImage());
                i.putExtra("phone", Preferences.getInstance(MainActivity.this).getPhone());
//                i.putExtra("AttorneyId", (int)Preferences.getInstance(MainActivity.this).getAttorneyId());
                startActivity(i);

            }
        });

    }

    private void signOutHere() {
        final ProgressDialog p = new ProgressDialog(this);
        p.setMessage("Please wait...");
        p.show();
        Preferences.getInstance(MainActivity.this).destroy_SharedPreference();
        WebServiceRequests.getInstance().signOut(MainActivity.this, username, new Callback<SignOut>() {
            @Override
            public void onResponse(Call<SignOut> call, Response<SignOut> response) {
                if (response.isSuccessful() && response.body().getSignOutResult().isResult()) {
                    p.hide();
                    UAirship.shared().getPushManager().editTags()
                            .removeTag(String.valueOf(Preferences.getInstance(MainActivity.this).getUserId()))
                            .apply();
                    UAirship.shared().getPushManager().editTags()
                            .removeTag(String.valueOf(Preferences.getInstance(MainActivity.this).getUserName()))
                            .apply();
                    UAirship.shared().getPushManager().editTags()
                            .removeTag("Attorney")
                            .apply();

                    startActivity(new Intent(MainActivity.this, WalkThroughActivity.class));
                    finish();
                } else {
                    p.hide();
                    Toast.makeText(MainActivity.this, "" + response.body().getSignOutResult().getErrorDetail().getErrorDetails().trim(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignOut> call, Throwable t) {
                p.hide();
                Toast.makeText(MainActivity.this, "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void refreshDrawerProfile() {

        if (Preferences.getInstance(this).getImage().length() > 0)
            Picasso.with(this).load(Preferences.getInstance(this).getImage()).resize(200, 200).centerCrop().into(profile_imageView);
        profileNameTextView.setText(Preferences.getInstance(this).getName());
    }

    private void setData() {
        header_text.setText("Home");
        if (Preferences.getInstance(this).getImage().length() > 0)
            Picasso.with(this).load(Preferences.getInstance(this).getImage()).resize(200, 200).centerCrop().into(profile_imageView);
        profileNameTextView.setText(Preferences.getInstance(this).getName());
        getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
    }

    private void findViews() {

        logoutAttorney = (LinearLayout) findViewById(R.id.logout_attorney);

        chatWithSupportID = (TextView) findViewById(R.id.chat_with_support_ID);

        profile_imageView = (ImageView) findViewById(R.id.profile_image_id);
        Update_Profile_viewID = (LinearLayout) findViewById(R.id.Update_Profile_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        logout = (LinearLayout) findViewById(R.id.logout);
        ask_attoneys = (LinearLayout) findViewById(R.id.ask_attoneys);
        search_attoneys = (LinearLayout) findViewById(R.id.search_attoneys);
        home_view = (LinearLayout) findViewById(R.id.home_view);
        change_Password_navigationDrawerLayoutID = (LinearLayout) findViewById(R.id.change_Password_navigationDrawerLayout);
        privacy_policy = (LinearLayout) findViewById(R.id.privacy_navigationDrawerLayout);
        terms_conditions = (LinearLayout) findViewById(R.id.terms_navigationDrawerLayout);
        viewProfileLayt = (LinearLayout) findViewById(R.id.viewProfileLayt);
        viewQuestionLayt = (LinearLayout) findViewById(R.id.viewQuestionLayt);
        header_text = (TextView) findViewById(R.id.header_text);

        profileNameTextView = (TextView) findViewById(R.id.profile_image_id_TEXT_VIEW);

    }

    private void setupNavDrawer() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                0,
                0
        );
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof HomeFragment) {
            final AlertDialog.Builder ab = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog);
            ab.setMessage("Are you sure you want to exit?");
            ab.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finish();
                }
            });
            ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            ab.setCancelable(false);
            ab.show();
        } else if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof Not_Approved) {
            final AlertDialog.Builder ab = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog);
            ab.setMessage("Are you sure you want to exit?");
            ab.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finish();
                }
            });
            ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            ab.setCancelable(false);
            ab.show();

        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof View_questions) {
            ((View_questions) getSupportFragmentManager().findFragmentById(R.id.container)).onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_CANCELED) {
                Intent i = new Intent(MainActivity.this, MarketPlaceAttorneyActivity.class);
                startActivity(i);

            }

        } else if (requestCode == 3) {
            if (resultCode == Activity.RESULT_CANCELED) {
                header_text.setText("Ask Attorneys");
                getSupportFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new AskAttorney()).commit();

            }
        }
    }



}
