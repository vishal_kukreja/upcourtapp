package com.bizbrolly.com.uplawapp.WebServices;

import java.util.List;

/**
 * Created by VishalK on 9/19/2017.
 */

public class SignInResponse {


    /**
     * SignInResult : {"Data":{"__type":"clsSignIn:#UpCourt.Model","AttorneyDetails":{"AdvertismentURL":null,"AttorneyId":0,"AttorneysBio":null,"BusinessHours":null,"DOB":null,"EmailId":null,"FirstName":null,"IsAgree":null,"IsApproved":false,"LastName":null,"Location":null,"Name":null,"Password":null,"Phone":null,"Specialization":null,"UserImage":null,"Username":null,"YearExperience":null,"lstAdvertisment":[]},"AuthToken":"A0C65AFC-E43B-4923-A026-1CD5EE539A55","IsAdmin":false,"IsApproved":true,"NewPassword":null,"UserDetails":{"ActivationCode":null,"ClientIPAddress":"101.102.103.178111","ClientMachineName":"BizBrolly","DOB":null,"EmailId":"x@x.x","FirstName":"gyhj hbnmkl,","Gender":null,"IsActive":false,"IsAgree":true,"IsApproved":false,"LastLoginTime":"/Date(-62135596800000+0000)/","LastName":null,"Latitude":0,"LicenceNumber":null,"Longitude":0,"MiddleName":null,"Password":null,"PasswordModificationDate":"/Date(1510665934460+0000)/","Phone":"3737384948","RoleDetails":{"Role":"Attorney","RoleDescription":"Attorney","RoleId":103},"UserId":235,"UserImage":"","UserName":"x@x.x","id":0},"Username":"x@x.x","password":"7c4a8d09ca3762af61e59520943dc26494f8941b"},"ErrorDetail":{"ErrorDetails":"","ErrorMessage":""},"Result":true}
     */

    private SignInResultBean SignInResult;

    public SignInResultBean getSignInResult() {
        return SignInResult;
    }

    public void setSignInResult(SignInResultBean SignInResult) {
        this.SignInResult = SignInResult;
    }

    public static class SignInResultBean {
        /**
         * Data : {"__type":"clsSignIn:#UpCourt.Model","AttorneyDetails":{"AdvertismentURL":null,"AttorneyId":0,"AttorneysBio":null,"BusinessHours":null,"DOB":null,"EmailId":null,"FirstName":null,"IsAgree":null,"IsApproved":false,"LastName":null,"Location":null,"Name":null,"Password":null,"Phone":null,"Specialization":null,"UserImage":null,"Username":null,"YearExperience":null,"lstAdvertisment":[]},"AuthToken":"A0C65AFC-E43B-4923-A026-1CD5EE539A55","IsAdmin":false,"IsApproved":true,"NewPassword":null,"UserDetails":{"ActivationCode":null,"ClientIPAddress":"101.102.103.178111","ClientMachineName":"BizBrolly","DOB":null,"EmailId":"x@x.x","FirstName":"gyhj hbnmkl,","Gender":null,"IsActive":false,"IsAgree":true,"IsApproved":false,"LastLoginTime":"/Date(-62135596800000+0000)/","LastName":null,"Latitude":0,"LicenceNumber":null,"Longitude":0,"MiddleName":null,"Password":null,"PasswordModificationDate":"/Date(1510665934460+0000)/","Phone":"3737384948","RoleDetails":{"Role":"Attorney","RoleDescription":"Attorney","RoleId":103},"UserId":235,"UserImage":"","UserName":"x@x.x","id":0},"Username":"x@x.x","password":"7c4a8d09ca3762af61e59520943dc26494f8941b"}
         * ErrorDetail : {"ErrorDetails":"","ErrorMessage":""}
         * Result : true
         */

        private DataBean Data;
        private ErrorDetailBean ErrorDetail;
        private boolean Result;

        public DataBean getData() {
            return Data;
        }

        public void setData(DataBean Data) {
            this.Data = Data;
        }

        public ErrorDetailBean getErrorDetail() {
            return ErrorDetail;
        }

        public void setErrorDetail(ErrorDetailBean ErrorDetail) {
            this.ErrorDetail = ErrorDetail;
        }

        public boolean isResult() {
            return Result;
        }

        public void setResult(boolean Result) {
            this.Result = Result;
        }

        public static class DataBean {
            /**
             * __type : clsSignIn:#UpCourt.Model
             * AttorneyDetails : {"AdvertismentURL":null,"AttorneyId":0,"AttorneysBio":null,"BusinessHours":null,"DOB":null,"EmailId":null,"FirstName":null,"IsAgree":null,"IsApproved":false,"LastName":null,"Location":null,"Name":null,"Password":null,"Phone":null,"Specialization":null,"UserImage":null,"Username":null,"YearExperience":null,"lstAdvertisment":[]}
             * AuthToken : A0C65AFC-E43B-4923-A026-1CD5EE539A55
             * IsAdmin : false
             * IsApproved : true
             * NewPassword : null
             * UserDetails : {"ActivationCode":null,"ClientIPAddress":"101.102.103.178111","ClientMachineName":"BizBrolly","DOB":null,"EmailId":"x@x.x","FirstName":"gyhj hbnmkl,","Gender":null,"IsActive":false,"IsAgree":true,"IsApproved":false,"LastLoginTime":"/Date(-62135596800000+0000)/","LastName":null,"Latitude":0,"LicenceNumber":null,"Longitude":0,"MiddleName":null,"Password":null,"PasswordModificationDate":"/Date(1510665934460+0000)/","Phone":"3737384948","RoleDetails":{"Role":"Attorney","RoleDescription":"Attorney","RoleId":103},"UserId":235,"UserImage":"","UserName":"x@x.x","id":0}
             * Username : x@x.x
             * password : 7c4a8d09ca3762af61e59520943dc26494f8941b
             */

            private String __type;
            private AttorneyDetailsBean AttorneyDetails;
            private String AuthToken;
            private boolean IsAdmin;
            private boolean IsApproved;
            private Object NewPassword;
            private UserDetailsBean UserDetails;
            private String Username;
            private String password;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public AttorneyDetailsBean getAttorneyDetails() {
                return AttorneyDetails;
            }

            public void setAttorneyDetails(AttorneyDetailsBean AttorneyDetails) {
                this.AttorneyDetails = AttorneyDetails;
            }

            public String getAuthToken() {
                return AuthToken;
            }

            public void setAuthToken(String AuthToken) {
                this.AuthToken = AuthToken;
            }

            public boolean isIsAdmin() {
                return IsAdmin;
            }

            public void setIsAdmin(boolean IsAdmin) {
                this.IsAdmin = IsAdmin;
            }

            public boolean isIsApproved() {
                return IsApproved;
            }

            public void setIsApproved(boolean IsApproved) {
                this.IsApproved = IsApproved;
            }

            public Object getNewPassword() {
                return NewPassword;
            }

            public void setNewPassword(Object NewPassword) {
                this.NewPassword = NewPassword;
            }

            public UserDetailsBean getUserDetails() {
                return UserDetails;
            }

            public void setUserDetails(UserDetailsBean UserDetails) {
                this.UserDetails = UserDetails;
            }

            public String getUsername() {
                return Username;
            }

            public void setUsername(String Username) {
                this.Username = Username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public static class AttorneyDetailsBean {
                /**
                 * AdvertismentURL : null
                 * AttorneyId : 0
                 * AttorneysBio : null
                 * BusinessHours : null
                 * DOB : null
                 * EmailId : null
                 * FirstName : null
                 * IsAgree : null
                 * IsApproved : false
                 * LastName : null
                 * Location : null
                 * Name : null
                 * Password : null
                 * Phone : null
                 * Specialization : null
                 * UserImage : null
                 * Username : null
                 * YearExperience : null
                 * lstAdvertisment : []
                 */

                private Object AdvertismentURL;
                private int AttorneyId;
                private Object AttorneysBio;
                private Object BusinessHours;
                private Object DOB;
                private Object EmailId;
                private Object FirstName;
                private Object IsAgree;
                private boolean IsApproved;
                private Object LastName;
                private Object Location;
                private Object Name;
                private Object Password;
                private Object Phone;
                private Object Specialization;
                private Object UserImage;
                private Object Username;
                private Object YearExperience;
                private List<?> lstAdvertisment;

                public Object getAdvertismentURL() {
                    return AdvertismentURL;
                }

                public void setAdvertismentURL(Object AdvertismentURL) {
                    this.AdvertismentURL = AdvertismentURL;
                }

                public int getAttorneyId() {
                    return AttorneyId;
                }

                public void setAttorneyId(int AttorneyId) {
                    this.AttorneyId = AttorneyId;
                }

                public Object getAttorneysBio() {
                    return AttorneysBio;
                }

                public void setAttorneysBio(Object AttorneysBio) {
                    this.AttorneysBio = AttorneysBio;
                }

                public Object getBusinessHours() {
                    return BusinessHours;
                }

                public void setBusinessHours(Object BusinessHours) {
                    this.BusinessHours = BusinessHours;
                }

                public Object getDOB() {
                    return DOB;
                }

                public void setDOB(Object DOB) {
                    this.DOB = DOB;
                }

                public Object getEmailId() {
                    return EmailId;
                }

                public void setEmailId(Object EmailId) {
                    this.EmailId = EmailId;
                }

                public Object getFirstName() {
                    return FirstName;
                }

                public void setFirstName(Object FirstName) {
                    this.FirstName = FirstName;
                }

                public Object getIsAgree() {
                    return IsAgree;
                }

                public void setIsAgree(Object IsAgree) {
                    this.IsAgree = IsAgree;
                }

                public boolean isIsApproved() {
                    return IsApproved;
                }

                public void setIsApproved(boolean IsApproved) {
                    this.IsApproved = IsApproved;
                }

                public Object getLastName() {
                    return LastName;
                }

                public void setLastName(Object LastName) {
                    this.LastName = LastName;
                }

                public Object getLocation() {
                    return Location;
                }

                public void setLocation(Object Location) {
                    this.Location = Location;
                }

                public Object getName() {
                    return Name;
                }

                public void setName(Object Name) {
                    this.Name = Name;
                }

                public Object getPassword() {
                    return Password;
                }

                public void setPassword(Object Password) {
                    this.Password = Password;
                }

                public Object getPhone() {
                    return Phone;
                }

                public void setPhone(Object Phone) {
                    this.Phone = Phone;
                }

                public Object getSpecialization() {
                    return Specialization;
                }

                public void setSpecialization(Object Specialization) {
                    this.Specialization = Specialization;
                }

                public Object getUserImage() {
                    return UserImage;
                }

                public void setUserImage(Object UserImage) {
                    this.UserImage = UserImage;
                }

                public Object getUsername() {
                    return Username;
                }

                public void setUsername(Object Username) {
                    this.Username = Username;
                }

                public Object getYearExperience() {
                    return YearExperience;
                }

                public void setYearExperience(Object YearExperience) {
                    this.YearExperience = YearExperience;
                }

                public List<?> getLstAdvertisment() {
                    return lstAdvertisment;
                }

                public void setLstAdvertisment(List<?> lstAdvertisment) {
                    this.lstAdvertisment = lstAdvertisment;
                }
            }

            public static class UserDetailsBean {
                /**
                 * ActivationCode : null
                 * ClientIPAddress : 101.102.103.178111
                 * ClientMachineName : BizBrolly
                 * DOB : null
                 * EmailId : x@x.x
                 * FirstName : gyhj hbnmkl,
                 * Gender : null
                 * IsActive : false
                 * IsAgree : true
                 * IsApproved : false
                 * LastLoginTime : /Date(-62135596800000+0000)/
                 * LastName : null
                 * Latitude : 0
                 * LicenceNumber : null
                 * Longitude : 0
                 * MiddleName : null
                 * Password : null
                 * PasswordModificationDate : /Date(1510665934460+0000)/
                 * Phone : 3737384948
                 * RoleDetails : {"Role":"Attorney","RoleDescription":"Attorney","RoleId":103}
                 * UserId : 235
                 * UserImage :
                 * UserName : x@x.x
                 * id : 0
                 */

                private Object ActivationCode;
                private String ClientIPAddress;
                private String ClientMachineName;
                private Object DOB;
                private String EmailId;
                private String FirstName;
                private Object Gender;
                private boolean IsActive;
                private boolean IsAgree;
                private boolean IsApproved;
                private String LastLoginTime;
                private Object LastName;
                private int Latitude;
                private Object LicenceNumber;
                private int Longitude;
                private Object MiddleName;
                private Object Password;
                private String PasswordModificationDate;
                private String Phone;
                private RoleDetailsBean RoleDetails;
                private int UserId;
                private String UserImage;
                private String UserName;
                private int id;

                public Object getActivationCode() {
                    return ActivationCode;
                }

                public void setActivationCode(Object ActivationCode) {
                    this.ActivationCode = ActivationCode;
                }

                public String getClientIPAddress() {
                    return ClientIPAddress;
                }

                public void setClientIPAddress(String ClientIPAddress) {
                    this.ClientIPAddress = ClientIPAddress;
                }

                public String getClientMachineName() {
                    return ClientMachineName;
                }

                public void setClientMachineName(String ClientMachineName) {
                    this.ClientMachineName = ClientMachineName;
                }

                public Object getDOB() {
                    return DOB;
                }

                public void setDOB(Object DOB) {
                    this.DOB = DOB;
                }

                public String getEmailId() {
                    return EmailId;
                }

                public void setEmailId(String EmailId) {
                    this.EmailId = EmailId;
                }

                public String getFirstName() {
                    return FirstName;
                }

                public void setFirstName(String FirstName) {
                    this.FirstName = FirstName;
                }

                public Object getGender() {
                    return Gender;
                }

                public void setGender(Object Gender) {
                    this.Gender = Gender;
                }

                public boolean isIsActive() {
                    return IsActive;
                }

                public void setIsActive(boolean IsActive) {
                    this.IsActive = IsActive;
                }

                public boolean isIsAgree() {
                    return IsAgree;
                }

                public void setIsAgree(boolean IsAgree) {
                    this.IsAgree = IsAgree;
                }

                public boolean isIsApproved() {
                    return IsApproved;
                }

                public void setIsApproved(boolean IsApproved) {
                    this.IsApproved = IsApproved;
                }

                public String getLastLoginTime() {
                    return LastLoginTime;
                }

                public void setLastLoginTime(String LastLoginTime) {
                    this.LastLoginTime = LastLoginTime;
                }

                public Object getLastName() {
                    return LastName;
                }

                public void setLastName(Object LastName) {
                    this.LastName = LastName;
                }

                public int getLatitude() {
                    return Latitude;
                }

                public void setLatitude(int Latitude) {
                    this.Latitude = Latitude;
                }

                public Object getLicenceNumber() {
                    return LicenceNumber;
                }

                public void setLicenceNumber(Object LicenceNumber) {
                    this.LicenceNumber = LicenceNumber;
                }

                public int getLongitude() {
                    return Longitude;
                }

                public void setLongitude(int Longitude) {
                    this.Longitude = Longitude;
                }

                public Object getMiddleName() {
                    return MiddleName;
                }

                public void setMiddleName(Object MiddleName) {
                    this.MiddleName = MiddleName;
                }

                public Object getPassword() {
                    return Password;
                }

                public void setPassword(Object Password) {
                    this.Password = Password;
                }

                public String getPasswordModificationDate() {
                    return PasswordModificationDate;
                }

                public void setPasswordModificationDate(String PasswordModificationDate) {
                    this.PasswordModificationDate = PasswordModificationDate;
                }

                public String getPhone() {
                    return Phone;
                }

                public void setPhone(String Phone) {
                    this.Phone = Phone;
                }

                public RoleDetailsBean getRoleDetails() {
                    return RoleDetails;
                }

                public void setRoleDetails(RoleDetailsBean RoleDetails) {
                    this.RoleDetails = RoleDetails;
                }

                public int getUserId() {
                    return UserId;
                }

                public void setUserId(int UserId) {
                    this.UserId = UserId;
                }

                public String getUserImage() {
                    return UserImage;
                }

                public void setUserImage(String UserImage) {
                    this.UserImage = UserImage;
                }

                public String getUserName() {
                    return UserName;
                }

                public void setUserName(String UserName) {
                    this.UserName = UserName;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public static class RoleDetailsBean {
                    /**
                     * Role : Attorney
                     * RoleDescription : Attorney
                     * RoleId : 103
                     */

                    private String Role;
                    private String RoleDescription;
                    private int RoleId;

                    public String getRole() {
                        return Role;
                    }

                    public void setRole(String Role) {
                        this.Role = Role;
                    }

                    public String getRoleDescription() {
                        return RoleDescription;
                    }

                    public void setRoleDescription(String RoleDescription) {
                        this.RoleDescription = RoleDescription;
                    }

                    public int getRoleId() {
                        return RoleId;
                    }

                    public void setRoleId(int RoleId) {
                        this.RoleId = RoleId;
                    }
                }
            }
        }

        public static class ErrorDetailBean {
            /**
             * ErrorDetails :
             * ErrorMessage :
             */

            private String ErrorDetails;
            private String ErrorMessage;

            public String getErrorDetails() {
                return ErrorDetails;
            }

            public void setErrorDetails(String ErrorDetails) {
                this.ErrorDetails = ErrorDetails;
            }

            public String getErrorMessage() {
                return ErrorMessage;
            }

            public void setErrorMessage(String ErrorMessage) {
                this.ErrorMessage = ErrorMessage;
            }
        }
    }
}
