package com.bizbrolly.com.uplawapp;
import android.os.StrictMode;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizbrolly.com.uplawapp.WebServices.ProfileUpdateResponse;
import com.bizbrolly.com.uplawapp.WebServices.WebServiceRequests;
import com.bizbrolly.com.uplawapp.activities.MainActivity;
import com.bizbrolly.com.uplawapp.utills.Preferences;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile_Update extends Fragment {

    private CircleImageView profileImage;
    private TextView patientNameId;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    String userChoosenTask;
    private String imageBase64;
    EditText FirstNameView, LastNameView, EmailView, PhoneView;
    Button submit;

    EditText Attorneys_Biography_ID,
            SpecializationID,
            LocationID,
            YearExperienceId,
            BusinessHours;

    RelativeLayout Attorneys_BiographyLayout,
            SpecializationLayout,
            LocationLayout,
            YearExperienceLayout,
            BusinessHoursLayout;

    public Profile_Update() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile__update, container, false);
        setHasOptionsMenu(true);
        initView(v);
/*
        imageBase64="";
                imageBase64=Preferences.getInstance(getActivity()).getImage();


*/
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        ((MainActivity) getActivity()).setHeadingText("Change Profile");


        if (Preferences.getInstance(getActivity()).getRole().equals("Attorney")) {
            Attorneys_BiographyLayout.setVisibility(View.VISIBLE);
            SpecializationLayout.setVisibility(View.VISIBLE);
            LocationLayout.setVisibility(View.VISIBLE);
            YearExperienceLayout.setVisibility(View.VISIBLE);
            BusinessHoursLayout.setVisibility(View.VISIBLE);
        } else {
            Attorneys_BiographyLayout.setVisibility(View.GONE);
            SpecializationLayout.setVisibility(View.GONE);
            LocationLayout.setVisibility(View.GONE);
            YearExperienceLayout.setVisibility(View.GONE);
            BusinessHoursLayout.setVisibility(View.GONE);

        }
        setData();
        setUpActions();

        // Inflate the layout for this fragment
        return v;
    }

    private void setData() {
        if (Preferences.getInstance(getActivity()).getImage() != null && !Preferences.getInstance(getActivity()).getImage().equals("")) {
            Picasso.with(getActivity()).load(Preferences.getInstance(getActivity()).getImage()).resize(200, 200).centerCrop().into(profileImage);
            image();
        }
        String s = Preferences.getInstance(getActivity()).getName();
        if (s.trim().contains(" ")) {
            String[] arr = s.split(" ");
            FirstNameView.setText(arr[0]);
            LastNameView.setText(s.substring(s.indexOf(" ") + 1));
        } else {
            FirstNameView.setText(s);
        }
        PhoneView.setText(Preferences.getInstance(getActivity()).getPhone());
        if (Preferences.getInstance(getActivity()).getRole().equals("Attorney")) {
            Attorneys_Biography_ID.setText(Preferences.getInstance(getActivity()).getAttorneys_Biography_ID());
            SpecializationID.setText(Preferences.getInstance(getActivity()).getSpecializationID());
            LocationID.setText(Preferences.getInstance(getActivity()).getLocationID());
            YearExperienceId.setText(Preferences.getInstance(getActivity()).getYearExperienceId());
            BusinessHours.setText(Preferences.getInstance(getActivity()).getBusinessHoursDD());
        }
    }

    View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectImage();
        }
    };


    private boolean validate() {
        boolean isValid = true;
        if (FirstNameView.getText().toString().trim().length() == 0) {
            FirstNameView.setError("First Name should not be empty");
            isValid = false;
        } else if (LastNameView.getText().toString().trim().length() == 0) {
            LastNameView.setError("Last Name should not be empty");
            isValid = false;
        } else if (PhoneView.getText().toString().trim().length() == 0) {
            PhoneView.setError("Phone Number should not be empty");
            isValid = false;
        }
        if (PhoneView.getText().toString().trim().length() < 10) {
            PhoneView.setError("Phone Number should atleast 10 digits");
            isValid = false;
        }
        return isValid;
    }

    private boolean validate1() {
        boolean isValid = true;
        if (FirstNameView.getText().toString().trim().length() == 0) {
            FirstNameView.setError("First Name should not be empty");
            isValid = false;
        } else if (LastNameView.getText().toString().trim().length() == 0) {
            LastNameView.setError("Last Name should not be empty");
            isValid = false;
        } else if (PhoneView.getText().toString().trim().length() == 0) {
            PhoneView.setError("Phone Number should not be empty");
            isValid = false;
        } else if (Attorneys_Biography_ID.getText().toString().trim().length() == 0) {
            Attorneys_Biography_ID.setError("Attorneys Biography should not be empty");
            isValid = false;
        } else if (SpecializationID.getText().toString().trim().length() == 0) {
            SpecializationID.setError("Specialization should not be empty");
            isValid = false;
        } else if (LocationID.getText().toString().trim().length() == 0) {
            LocationID.setError("Location should not be empty");
            isValid = false;
        } else if (YearExperienceId.getText().toString().trim().length() == 0) {
            YearExperienceId.setError("Year Experience should not be empty");
            isValid = false;
        } else if (BusinessHours.getText().toString().trim().length() == 0) {
            BusinessHours.setError("Business Hours should not be empty");
            isValid = false;
        }

        if (PhoneView.getText().toString().trim().length() < 10) {
            PhoneView.setError("Phone Number should be of 10 digits");
            isValid = false;
        }
        return isValid;
    }

    private void setUpActions() {
        patientNameId.setText(Preferences.getInstance(getActivity()).getName());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Preferences.getInstance(getActivity()).getRole().equals("Attorney")) {
                    if (validate1()) {
                        updateProfile("attorney");
                    }
                } else if (validate())
                    updateProfile("user");
            }
        });
    }

    private void updateProfile(String a) {
        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setTitle("Profile Update");
        p.setMessage("Updating...");
        p.show();
        final String image, name, phone,
                Attorneys_Biography_ID,
                SpecializationID,
                LocationID,
                YearExperienceId,
                BusinessHours,
                role;

        if (a.equals("user")) {
            image = Calendar.getInstance().getTime().getTime() + ".jpg";
            name = FirstNameView.getText().toString() + " " + LastNameView.getText().toString();
            phone = PhoneView.getText().toString();
            role = "User";
            Attorneys_Biography_ID = "";
            SpecializationID = "";
            LocationID = "";
            YearExperienceId = "";
            BusinessHours = "";

        } else {
            image = Calendar.getInstance().getTime().getTime() + ".jpg";
            name = FirstNameView.getText().toString() + " " + LastNameView.getText().toString();
            phone = PhoneView.getText().toString();
            Attorneys_Biography_ID = this.Attorneys_Biography_ID.getText().toString();
            SpecializationID = this.SpecializationID.getText().toString();
            LocationID = this.LocationID.getText().toString();
            YearExperienceId = this.YearExperienceId.getText().toString();
            BusinessHours = this.BusinessHours.getText().toString();
            role = "Attorney";
        }
        WebServiceRequests.getInstance().profileUpdate(
                getActivity(),
                Preferences.getInstance(getActivity()).getUserName(),
                name,
                phone,
                image,
                imageBase64,

                role,
                Attorneys_Biography_ID,
                SpecializationID,
                LocationID,
                YearExperienceId,
                BusinessHours,

                new Callback<ProfileUpdateResponse>() {
                    @Override
                    public void onResponse(Call<ProfileUpdateResponse> call, Response<ProfileUpdateResponse> response) {
                        p.hide();
                        if (response.isSuccessful() && response.body() != null && response.body().getUpdateUserAndAttorneyDetailsResult().isResult()) {
//                            Preferences.getInstance(getContext()).setImage(response.body().getUpdateProfileResult().getData().getUserImage());
//                            Preferences.getInstance(getContext()).setName(response.body().getUpdateProfileResult().getData().getName());
//                            Preferences.getInstance(getContext()).setPhone(response.body().getUpdateProfileResult().getData().getPhone());
                            //Preferences.getInstance(getContext()).setImage("http://bizbrollydev.cloudapp.net/UpCourt/Images/"+image.toString().trim());

                            Preferences.getInstance(getContext()).setName(name);
                            Preferences.getInstance(getContext()).setPhone(phone);
                            if (imageBase64 != null) {
                                Preferences.getInstance(getContext()).setImage(response.body().getUpdateUserAndAttorneyDetailsResult().getData().getUserImage());
                            }
                            if (response.body().getUpdateUserAndAttorneyDetailsResult().getData().getRole().equals("User")) {

                            } else {

                                Preferences.getInstance(getContext()).setAttorneys_Biography_ID(Attorneys_Biography_ID);
                                Preferences.getInstance(getContext()).setSpecializationID(SpecializationID);
                                Preferences.getInstance(getContext()).setLocationID(LocationID);
                                Preferences.getInstance(getContext()).setYearExperienceId(YearExperienceId);
                                Preferences.getInstance(getContext()).setBusinessHoursDD(BusinessHours);
                            }
                            setData();
                            ((MainActivity) getActivity()).refreshDrawerProfile();
                            refreshNameProfile();
                            disableEdit();
                            Toast.makeText(getActivity(), "Profile Updated", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileUpdateResponse> call, Throwable t) {
                        p.hide();
                        call.request();
                        Toast.makeText(getActivity(), "failed to connect network,please connect", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void refreshNameProfile() {
        patientNameId.setText(Preferences.getInstance(getActivity()).getName());

    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Gallery";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri uri = data.getData();
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        profileImage.setImageBitmap(thumbnail);
        imageBase64 = encode_Image(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri uri = data.getData();
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        profileImage.setImageBitmap(bm);
        imageBase64 = encode_Image(bm);
    }

    public static String encode_Image(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        bitmap = Bitmap.createScaledBitmap(
                bitmap,
                bitmap.getWidth() >= 200 ? 200 : bitmap.getWidth(),
                bitmap.getHeight() >= 200 ? 200 : bitmap.getHeight(),
                true
        );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private void initView(View view) {
        profileImage = (CircleImageView) view.findViewById(R.id.profileImage);
        patientNameId = (TextView) view.findViewById(R.id.name_profileView);
        FirstNameView = (EditText) view.findViewById(R.id.nameprofile);
        LastNameView = (EditText) view.findViewById(R.id.LastNameUpdateID);
        PhoneView = (EditText) view.findViewById(R.id.Phone_idUpdate);
        submit = (Button) view.findViewById(R.id.Submit_ButtonView);

        Attorneys_Biography_ID = (EditText) view.findViewById(R.id.Attorneys_Biography_ID);
        final int MAX_WORDS = 150;
        Attorneys_Biography_ID.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Nothing
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String[] words = s.toString().split(" "); // Get all words
                if (words.length > MAX_WORDS) {
                    // Trim words to length MAX_WORDS
                    // Join words into a String
                    Attorneys_Biography_ID.setError("You cannot add more than 150 words.");
//                    Attorneys_Biography_ID.setText(wordString);
                }
            }
        });


        SpecializationID = (EditText) view.findViewById(R.id.SpecializationID);
        LocationID = (EditText) view.findViewById(R.id.LocationID);
        BusinessHours = (EditText) view.findViewById(R.id.BusinessHoursIDD);
        YearExperienceId = (EditText) view.findViewById(R.id.YearExperienceId);

        Attorneys_BiographyLayout = (RelativeLayout) view.findViewById(R.id.Attorneys_BiographyLayout);
        SpecializationLayout = (RelativeLayout) view.findViewById(R.id.SpecializationLayout);
        LocationLayout = (RelativeLayout) view.findViewById(R.id.LocationLayout);
        YearExperienceLayout = (RelativeLayout) view.findViewById(R.id.BusinessHoursLayout);
        BusinessHoursLayout = (RelativeLayout) view.findViewById(R.id.YearExperienceLayout);


    }

    MenuItem editItem;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_menu, menu);
        editItem = menu.getItem(0);
        disableEdit();
        super.onCreateOptionsMenu(menu, inflater);
    }

    boolean isEditMode = false;

    private void enableEdit() {
        FirstNameView.setEnabled(true);
        LastNameView.setEnabled(true);
        PhoneView.setEnabled(true);
        Attorneys_Biography_ID.setEnabled(true);
        SpecializationID.setEnabled(true);
        LocationID.setEnabled(true);
        YearExperienceId.setEnabled(true);
        BusinessHours.setEnabled(true);
        editItem.setIcon(R.drawable.ic_close_black_24dp);
        isEditMode = true;
        submit.setVisibility(View.VISIBLE);
        profileImage.setOnClickListener(imageClickListener);
    }

    private void disableEdit() {
        FirstNameView.setEnabled(false);
        LastNameView.setEnabled(false);
        PhoneView.setEnabled(false);
        editItem.setIcon(R.drawable.ic_edit_black_24dp);
        Attorneys_Biography_ID.setEnabled(false);
        SpecializationID.setEnabled(false);
        LocationID.setEnabled(false);
        YearExperienceId.setEnabled(false);
        BusinessHours.setEnabled(false);
        isEditMode = false;
        submit.setVisibility(View.GONE);
        profileImage.setOnClickListener(null);
    }
/*public void image(){
    Bitmap bm = BitmapFactory.decodeFile("/path/to/image.jpg");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
    byte[] byteArrayImage = baos.toByteArray();
    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
}*/

    public void image() {
        try {
            URL url = new URL(Preferences.getInstance(getActivity()).getImage());
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            imageBase64 = encode_Image(image);

        } catch (IOException e) {
            System.out.println(e);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit) {
            if (isEditMode)
                disableEdit();
            else
                enableEdit();
        }
        return super.onOptionsItemSelected(item);
    }
}
